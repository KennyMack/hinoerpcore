using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Gerais.Interfaces.Repositories
{
    public interface IGEEmpresaRepository : IBaseRepository<GEEmpresa>
    {
    }
}
