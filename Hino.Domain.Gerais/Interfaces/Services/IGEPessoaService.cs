using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Gerais.Interfaces.Services
{
    public interface IGEPessoaService : IBaseService<GEPessoa>
    {
    }
}
