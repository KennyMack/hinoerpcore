using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Gerais.Interfaces.Services
{
    public interface IGEFuncionariosService : IBaseService<GEFuncionarios>
    {
        GEFuncionarios GetByIdentficador(int pCodEstab, string pIdentificacao);
    }
}
