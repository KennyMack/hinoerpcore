using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Gerais.Services
{
    public class GEEmpresaService : BaseService<GEEmpresa>, IGEEmpresaService
    {
        private readonly IGEEmpresaRepository _IGEEmpresaRepository;

        public GEEmpresaService(IGEEmpresaRepository pIGEEmpresaRepository) : 
             base(pIGEEmpresaRepository)
        {
            _IGEEmpresaRepository = pIGEEmpresaRepository;
        }
    }
}
