using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Gerais.Services
{
    public class GEUsuariosService : BaseService<GEUsuarios>, IGEUsuariosService
    {
        private readonly IGEUsuariosRepository _IGEUsuariosRepository;

        public GEUsuariosService(IGEUsuariosRepository pIGEUsuariosRepository) : 
             base(pIGEUsuariosRepository)
        {
            _IGEUsuariosRepository = pIGEUsuariosRepository;
        }
    }
}
