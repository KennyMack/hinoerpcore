using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Gerais.Services
{
    public class GEEstabService : BaseService<GEEstab>, IGEEstabService
    {
        private readonly IGEEstabRepository _IGEEstabRepository;

        public GEEstabService(IGEEstabRepository pIGEEstabRepository) : 
             base(pIGEEstabRepository)
        {
            _IGEEstabRepository = pIGEEstabRepository;
        }
    }
}
