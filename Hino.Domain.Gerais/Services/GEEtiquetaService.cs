using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Gerais.Services
{
    public class GEEtiquetaService : BaseService<GEEtiqueta>, IGEEtiquetaService
    {
        private readonly IGEEtiquetaRepository _IGEEtiquetaRepository;

        public GEEtiquetaService(IGEEtiquetaRepository pIGEEtiquetaRepository) : 
             base(pIGEEtiquetaRepository)
        {
            _IGEEtiquetaRepository = pIGEEtiquetaRepository;
        }
    }
}
