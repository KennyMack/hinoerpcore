using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Gerais.Services
{
    public class GEFuncionariosService : BaseService<GEFuncionarios>, IGEFuncionariosService
    {
        private readonly IGEFuncionariosRepository _IGEFuncionariosRepository;

        public GEFuncionariosService(IGEFuncionariosRepository pIGEFuncionariosRepository) : 
             base(pIGEFuncionariosRepository)
        {
            _IGEFuncionariosRepository = pIGEFuncionariosRepository;
        }

        public GEFuncionarios GetByIdentficador(int pCodEstab, string pIdentificacao) =>
            _IGEFuncionariosRepository.GetByIdentficador(pCodEstab, pIdentificacao);
    }
}
