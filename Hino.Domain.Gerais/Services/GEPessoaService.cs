using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Gerais.Services
{
    public class GEPessoaService : BaseService<GEPessoa>, IGEPessoaService
    {
        private readonly IGEPessoaRepository _IGEPessoaRepository;

        public GEPessoaService(IGEPessoaRepository pIGEPessoaRepository) : 
             base(pIGEPessoaRepository)
        {
            _IGEPessoaRepository = pIGEPessoaRepository;
        }
    }
}
