﻿
using Hino.Application;
using Hino.Application.AutoMapper;
using Hino.Application.Interfaces.Gerais;
using Hino.Application.ViewModels.Producao.Ordem;
using Hino.Domain.Estoque.Interfaces.Repositories;
using Hino.Domain.Estoque.Interfaces.Repositories.Lote;
using Hino.Domain.Fiscal.Interfaces.Repositories.Estoque;
using Hino.Domain.Fiscal.Interfaces.Repositories.Produto;
using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Domain.Producao.Interfaces.Repositories;
using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Domain.Producao.Interfaces.Repositories.Ordem;
using Hino.Domain.Producao.Interfaces.Repositories.Refugos;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.Cross.IoC;
using Hino.Infra.DataBase.Context;
using Hino.Infra.DataBase.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Apontamento.Terminal
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            // serviceCollection.AddAutoMapper(mapperConfig => mapperConfig.AddProfiles(GetType().Assembly))
            DInjection.Initialize(serviceCollection);

            Globals.CodEstab = 1;
            if (args.Length > 0)
                Globals.CodEstab = Convert.ToInt32(args[0]);

            if (args.Length == 2)
                Globals.NLinhas = Convert.ToInt32(args[1]);
            else
                Globals.NLinhas = 4;
            
            Mapper.Initialize();

            var pdordemprod = new PDOrdemProd();
            pdordemprod.codordprod = 1;

            var op = Mapper.Map<PDOrdemProd, PDOrdemProdVM>(pdordemprod);
            var op2 = Mapper.Map<PDOrdemProdVM>(pdordemprod);

            var conApp = new ConsAppTerm();
            await conApp.Iniciar();

            /*
                       var Repo = DInjection.GetIntance<IGEFuncionariosAS>();
                       while (true)
                       {
                           Console.WriteLine("Informe a pagina");
                           var Page = Console.ReadLine();
                           Console.WriteLine("Informe o numero de itens");
                           var Items = Console.ReadLine();

                           var resultsPage = Task.Run(async () => await Repo.GetAllPagedAsync(Convert.ToInt32(Page), Convert.ToInt32(Items))).Result;

                           Console.WriteLine($"Num. Itens: {resultsPage.RowCount} Pagina: {resultsPage.CurrentPage}/{resultsPage.PageCount}");
                           foreach (var item in resultsPage.Results)
                           {
                               Console.WriteLine($"{item.codpessoa}-{item.codfuncionario}");
                           }
                           Console.ReadKey();
                           Console.Clear();
                       }

                       Repo.Dispose();



                       using (var context = new AppDbContext())
                       using (var repo = new BaseRepository<GEEstab>(context))
                       using (var repoEmpresa = new BaseRepository<GEEmpresa>(context))
                       {
                           while (true)
                           {
                               Console.WriteLine("Informe a pagina");
                               var Page = Console.ReadLine();
                               Console.WriteLine("Informe o numero de itens");
                               var Items = Console.ReadLine();

                               var resultsPage = Task.Run(async () => await repoEmpresa.GetAllPagedAsync(Convert.ToInt32(Page), Convert.ToInt32(Items))).Result;

                               Console.WriteLine($"Num. Itens: {resultsPage.RowCount} Pagina: {resultsPage.CurrentPage}/{resultsPage.PageCount}");
                               foreach (var item in resultsPage.Results)
                               {
                                   Console.WriteLine($"{item.codempresa}-{item.razaosocial}");
                               }
                               Console.ReadKey();
                               Console.Clear();
                           }



                           var seq = Convert.ToInt32(repo.NextSequence());

                           var Estab2 = new GEEstab
                           {
                               codestab = seq,
                               nomefantasia = "ESTAB NOVO 2",
                               razaosocial = "ESTAB NOVO 2",
                               codmoeda = 1,
                               ativo = 1,
                               cdquantidade = 2,
                               cdvlunitario = 2,
                               cdvltotal = 2,
                               cdpercentual = 2,
                               cdtamanhocubico = 2,
                               gerarcontaempresa = 1,
                               caminhoimagens = "",
                               consestoque = 0,
                               usaunnegocio = 0
                           };

                           repo.Add(Estab2);
                           repo.SaveChanges();

                           Estab2.razaosocial += " EITA PORRA";

                           repo.Update(Estab2);
                           repo.SaveChanges();

                           var filtered = Task.Run(async () => await repo.QueryAsync(r => r.codestab != 0)).Result;
                           foreach (var item in filtered)
                           {
                               Console.WriteLine(item.razaosocial);
                           }

                           repo.RemoveByWhere(r => r.codestab == Estab2.codestab);
                           repo.SaveChanges();

                           filtered = Task.Run(async () => await repo.QueryAsync(r => r.codestab != 0)).Result;
                           foreach (var item in filtered)
                           {
                               Console.WriteLine(item.razaosocial);
                           }
                       }


                           Console.WriteLine("Hello World!");*/

            Console.WriteLine("Pressione ENTER para encerrar.");
            Console.ReadKey();
        }
    }
}
