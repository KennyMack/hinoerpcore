﻿using Hino.Application;
using Hino.Application.ConsoleUtils;
using Hino.Application.Interfaces.Gerais;
using Hino.Application.Interfaces.Producao;
using Hino.Application.Interfaces.Producao.Apontamento;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Infra.Cross.IoC;
using Hino.Infra.Cross.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Apontamento.Terminal
{
    public class ConsAppTerm
    {
        private IGEFuncionariosAS _geFuncionarios;
        private IPDParamEstabAS _pdParamEstab;
        private PDParamEstab _ParametrosProducao;
        private GEFuncionarios _FuncionarioAtual;

        private ETiposApontamento.OPERACAO OperacaoAtual;

        public ConsAppTerm()
        {
            _geFuncionarios = DInjection.GetIntance<IGEFuncionariosAS>();

            _pdParamEstab = DInjection.GetIntance<IPDParamEstabAS>();
            OperacaoAtual = ETiposApontamento.OPERACAO.INICIO;
        }

        #region Valida Parametros
        private bool ValidaParametros()
        {
            var retorno = true;

            if (_ParametrosProducao == null)
            {
                Console.WriteLine("Parâmetros de produção não encontrados (Produção|Parametros)");
                retorno = false;
            }


            if (_ParametrosProducao.exigemaqapto &&
                string.IsNullOrEmpty(_ParametrosProducao.codmaqpadrao))
            {
                retorno = false;
                Console.WriteLine("Maquina padrão preenchimento obrigatório (Produção|Parametros)");
            }
            else if (_ParametrosProducao.exigeturnoapto &&
                string.IsNullOrEmpty(_ParametrosProducao.turnopadrao))
            {
                retorno = false;
                Console.WriteLine("Turno padrão preenchimento obrigatório (Produção|Parametros)");
            }
            /*else if (string.IsNullOrEmpty(_pdParamEstab.codmotrefugo.ToString()))
            {
                retorno = false;
                Console.WriteLine("Motivo padrão refugo preenchimento obrigatório (Produção|Parametros)" );
            }*/
            else if (string.IsNullOrEmpty(_ParametrosProducao.codmotparada.ToString()))
            {
                retorno = false;
                Console.WriteLine("Motivo padrão parada preenchimento obrigatório (Produção|Parametros)");
            }
            else if (string.IsNullOrEmpty(_ParametrosProducao.codmotretrab.ToString()))
            {
                retorno = false;
                Console.WriteLine("Motivo padrão retrabalho preenchimento obrigatório (Produção|Parametros)");
            }
            else if (string.IsNullOrEmpty(_ParametrosProducao.codmotsetup.ToString()))
            {
                retorno = false;
                Console.WriteLine("Motivo padrão setup preenchimento obrigatório (Produção|Parametros)");
            }
            else if (string.IsNullOrEmpty(_ParametrosProducao.codmotmanut.ToString()))
            {
                retorno = false;
                Console.WriteLine("Motivo padrão manutenção preenchimento obrigatório (Produção|Parametros)");
            }
            else if (string.IsNullOrEmpty(_ParametrosProducao.codmottrserv.ToString()))
            {
                retorno = false;
                Console.WriteLine("Motivo padrão troca de serviço preenchimento obrigatório (Produção|Parametros)");
            }
            /*else if (string.IsNullOrEmpty(_pdParamEstab.codmotrefugo.ToString()))
            {
                retorno = false;
                Console.WriteLine("Motivo padrão refugo preenchimento obrigatório (Produção|Parametros)" );
            }*/

            return retorno;
        }
        #endregion

        #region Carregar Parametros produção
        public void CarregarParamProd()
        {
            _ParametrosProducao = _pdParamEstab.GetFirstByWhere(r => r.codestab == Globals.CodEstab);
        }
        #endregion

        #region Iniciar
        public async Task Iniciar()
        {
            Console.WriteLine("Iniciando....");
            Console.WriteLine("Validando parametros....");
            CarregarParamProd();

            if (ValidaParametros())
            {
                TermConsole.LimparConsole();
                Globals.Iniciado = true;
                var OpAtual = ETiposApontamento.INICIO.INICIO;

                do
                {
                    switch (OpAtual)
                    {
                        case ETiposApontamento.INICIO.INICIO:
                            CarregarParamProd();
                            TermConsole.LimparConsole();
                            TermConsole.ImprimirCabecalho();
                            OpAtual = ETiposApontamento.INICIO.USUARIO;
                            break;
                        case ETiposApontamento.INICIO.USUARIO:
                            OpAtual = TermConsole.ExibeMensagem("Informe o Usuário: ", ETiposApontamento.INICIO.DIGUSUARIO);
                            break;
                        case ETiposApontamento.INICIO.DIGUSUARIO:
                            OpAtual = BuscaUsuario();
                            break;
                        case ETiposApontamento.INICIO.OPERACOES:
                            TermConsole.ImprimirUsuario(_FuncionarioAtual);
                            TermConsole.ImprimirOperacoes(_ParametrosProducao, _FuncionarioAtual);
                            OpAtual = ETiposApontamento.INICIO.DIGOPERACOES;
                            break;
                        case ETiposApontamento.INICIO.DIGOPERACOES:
                            Console.Write("Operacao: ");
                            OpAtual = OperacaoSelecionada();
                            break;
                        case ETiposApontamento.INICIO.TIPO:
                            break;
                        case ETiposApontamento.INICIO.DIGTIPO:
                            break;
                        case ETiposApontamento.INICIO.APONTAMENTO:
                            OpAtual = await TrataApontamento();
                            break;
                        case ETiposApontamento.INICIO.SAIR:
                            break;
                        default:
                            break;
                    }
                }
                while (OpAtual != ETiposApontamento.INICIO.SAIR);

            }
        }
        #endregion

        #region Busca Usuario
        private ETiposApontamento.INICIO BuscaUsuario()
        {
            var Usuario = Console.ReadLine();
            return CarregaUsuario(Usuario);
        }
        #endregion

        #region Carrega Usuário
        private ETiposApontamento.INICIO CarregaUsuario(string pCodUsuario)
        {
            var retorno = ETiposApontamento.INICIO.OPERACOES;

            if (!string.IsNullOrEmpty(pCodUsuario))
            {
                _FuncionarioAtual = _geFuncionarios.GetByIdentficador(Globals.CodEstab, pCodUsuario);
                if (_FuncionarioAtual == null)
                {
                    ExibeMensagem("Usuário nao encontrado");
                    retorno = ETiposApontamento.INICIO.USUARIO;
                }
            }
            else
                retorno = ETiposApontamento.INICIO.INICIO;

            return retorno;
        }
        #endregion

        #region Exibe Mensagem
        private void ExibeMensagem(string mensagem)
        {
            Console.WriteLine(mensagem);
        }
        #endregion

        #region Operacao Selecionada
        private ETiposApontamento.INICIO OperacaoSelecionada()
        {
            var retorno = ETiposApontamento.INICIO.INICIO;
            var operacao = Console.ReadLine();

            if (!string.IsNullOrEmpty(operacao))
            {
                var idApt = 0;
                if (int.TryParse(operacao, out idApt))
                {
                    /// 1 - Inicio Preparação
                    /// 2 - Fim Preparação
                    /// 3 - Inicio de Produção
                    /// 4 - Fim de Produção
                    /// 5 - Inicio Troca de Serviço
                    /// 6 - Fim Troca de Serviço
                    /// 7 - Inicio de Manutenção
                    /// 8 - Fim de Manutenção
                    /// 9 - Inicio Consulta Desenho
                    /// 10 - Fim Consulta Desenho
                    /// 11 - Início Retrabalho
                    /// 12 - Fim Retrabalho
                    /// 13 - Apontamento de sucata
                    /// 14 - Termino de turno
                    if (idApt.In(1, 2, 3, 4, 5,
                                 6, 7, 8, 9, 10,
                                 11, 12, 13, 14) &&
                        PermiteOperacao(idApt))
                    {
                        OperacaoAtual = idApt.ToOperacao();
                        retorno = ETiposApontamento.INICIO.APONTAMENTO;
                    }
                    else
                    {
                        retorno = TermConsole.ExibeMensagemLine("Operacao nao permitida", ETiposApontamento.INICIO.DIGOPERACOES);
                    }
                }
                else
                {
                    retorno = TermConsole.ExibeMensagemLine("Tipo invalido", ETiposApontamento.INICIO.DIGOPERACOES);
                }

            }
            else
                retorno = ETiposApontamento.INICIO.USUARIO;

            return retorno;
        }
        #endregion

        #region Trata terminal Apontamento
        private async Task<ETiposApontamento.INICIO> TrataApontamento()
        {
            var retorno = ETiposApontamento.INICIO.APONTAMENTO;
            var _pdApontProducao = DInjection.GetIntance<IPDApontProducaoAS>();
            _pdApontProducao.SetFuncionarioAtual(_FuncionarioAtual);
            _pdApontProducao.SetParamEstabAtual(_ParametrosProducao);

            switch (OperacaoAtual)
            {
                case ETiposApontamento.OPERACAO.INICIO:
                    break;
               // case ETiposApontamento.OPERACAO.INICIOPREPARACAO:
               //     retorno = _pdApontProducao.InicioPreparacaoTerminal();
               //     break;
               // case ETiposApontamento.OPERACAO.TERMINOPREPARACAO:
               //     retorno = _pdApontProducao.TerminoPreparacaoTerminal();
               //     break;
                case ETiposApontamento.OPERACAO.INICIOPRODUCAO:
                    retorno =  await _pdApontProducao.InicioProducaoTerminal();
                    break;
                case ETiposApontamento.OPERACAO.TERMINOPRODUCAO:
                    retorno = await _pdApontProducao.TerminoProducaoTerminal();
                    break;
                // case ETiposApontamento.OPERACAO.INICIOTROCASERVICO:
                //     retorno = _pdApontProducao.InicioTrocaServicoTerminal();
                //     break;
                // case ETiposApontamento.OPERACAO.TERMINOTROCASERVICO:
                //     retorno = _pdApontProducao.TerminoTrocaServicoTerminal();
                //     break;
                // case ETiposApontamento.OPERACAO.INICIOMANUTENCAO:
                //     retorno = _pdApontProducao.InicioManutencaoTerminal();
                //     break;
                // case ETiposApontamento.OPERACAO.TERMINOMANUTENCAO:
                //     retorno = _pdApontProducao.TerminoManutencaoTerminal();
                //     break;
                // case ETiposApontamento.OPERACAO.INICIOCONSULTADESENHO:
                //     retorno = _pdApontProducao.InicioConsultaDesenhoTerminal();
                //     break;
                // case ETiposApontamento.OPERACAO.TERMINOCONSULTADESENHO:
                //     retorno = _pdApontProducao.TerminoConsultaDesenhoTerminal();
                //     break;
                // case ETiposApontamento.OPERACAO.INICIORETRABALHO:
                //     retorno = _pdApontProducao.InicioRetrabalhoTerminal();
                //     break;
                // case ETiposApontamento.OPERACAO.TERMINORETRABALHO:
                //     retorno = _pdApontProducao.TerminoRetrabalhoTerminal();
                //     break;
                // case ETiposApontamento.OPERACAO.TERMINOTURNO:
                //     retorno = _pdApontProducao.TerminoTurnoTerminal();
                //     break;

                case ETiposApontamento.OPERACAO.CONCLUIDO:
                    TermConsole.LimparConsole();
                    retorno = ETiposApontamento.INICIO.INICIO;
                    break;
                default:
                    break;
            }

            return retorno;
        }
        #endregion

        #region Permite Operação
        private bool PermiteOperacao(int pIdApt)
        {
            switch (pIdApt.ToOperacao())
            {
                case ETiposApontamento.OPERACAO.INICIOPREPARACAO:
                case ETiposApontamento.OPERACAO.TERMINOPREPARACAO:
                    return ((_ParametrosProducao.aptsetup) && (_FuncionarioAtual.aptsetup));
                case ETiposApontamento.OPERACAO.INICIOPRODUCAO:
                case ETiposApontamento.OPERACAO.TERMINOPRODUCAO:
                case ETiposApontamento.OPERACAO.TERMINOTURNO:
                    return ((_ParametrosProducao.aptproducao) && (_FuncionarioAtual.aptproducao));
                case ETiposApontamento.OPERACAO.INICIOTROCASERVICO:
                case ETiposApontamento.OPERACAO.TERMINOTROCASERVICO:
                    return ((_ParametrosProducao.apttrocaserv) && (_FuncionarioAtual.apttrocaserv));
                case ETiposApontamento.OPERACAO.INICIOMANUTENCAO:
                case ETiposApontamento.OPERACAO.TERMINOMANUTENCAO:
                    return ((_ParametrosProducao.aptmanut) && (_FuncionarioAtual.aptmanut));
                case ETiposApontamento.OPERACAO.INICIOCONSULTADESENHO:
                case ETiposApontamento.OPERACAO.TERMINOCONSULTADESENHO:
                    return ((_ParametrosProducao.aptconsdesenho) && (_FuncionarioAtual.aptconsdesenho));
                case ETiposApontamento.OPERACAO.INICIORETRABALHO:
                case ETiposApontamento.OPERACAO.TERMINORETRABALHO:
                    return ((_ParametrosProducao.aptretrab) && (_FuncionarioAtual.aptretrab));
                default:
                    return false;
            }
        }
        #endregion
    }
}
