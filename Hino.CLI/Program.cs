﻿using Oracle.ManagedDataAccess.Client;

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Hino.CLI
{
    class Program
    {
        /*
         create or replace FUNCTION longsubstrS( 
p_TABLE in VARCHAR2,
P_COLUMN IN VARCHAR2,
p_from in number,
p_for in number )
return varchar2
Is
l_tmp long;
begin
select DATA_DEFAULT into l_tmp from COLS 
where COLS.TABLE_NAME = p_TABLE
AND COLS.COLUMN_NAME = P_COLUMN;

return substr( l_tmp , p_from, p_for );
end;
/
         
         */

        static void Main(string[] args)
        {
            try
            {
                Directory.CreateDirectory("./converted");
            }
            catch (Exception)
            {

            }
            var StrConn = new OracleConnectionStringBuilder();
            StrConn.DataSource = "XE";
            StrConn.UserID = "HINOERP_SF";
            StrConn.Password = "HINO";

            using (OracleConnection conn = new OracleConnection(StrConn.ToString()))
            {
                conn.Open();
                var ExistsConfig = File.Exists("config.txt");
                var linhaAtual = 0;
                string[] linhas = new string[1];
                if (ExistsConfig)
                    linhas = File.ReadAllLines("config.txt");

                do
                {
                    Console.Clear();
                    string Tabela = "";
                    string Classe = "";
                    string modulo = "",
                        local = "",
                        GerarRepository = "N",
                        GerarService = "N",
                        GerarAppService = "N",
                        GerarViewModel = "N";

                    if (!ExistsConfig)
                    {
                        Console.WriteLine("Tabela");
                        Tabela = Console.ReadLine();

                        Console.WriteLine("Classe");
                        Classe = Console.ReadLine();
                    }
                    else
                    {
                        if (linhas[linhaAtual] == "")
                        {
                            linhaAtual++;
                            if (linhaAtual >= linhas.Length)
                                break;
                            continue;
                        }

                        var Linha = linhas[linhaAtual].Split(' ');

                        Tabela = Linha[0];
                        Classe = Linha[1];
                        modulo = Linha[2];
                        local = Linha[3] == "-" ? "" : Linha[3];
                        GerarRepository = Linha[4];
                        GerarService = Linha[5];
                        GerarAppService = Linha[6];
                        GerarViewModel = Linha[7];
                    }

                    if (Tabela != "")
                    {

                        if (!ExistsConfig)
                        {
                            Console.WriteLine("Modulo");
                            modulo = Console.ReadLine();

                            Console.WriteLine("Local");
                            local = Console.ReadLine();

                            if (!string.IsNullOrEmpty(local))
                                local = "." + local;

                            Console.WriteLine("Gerar Repository N - Não S - Sim");
                            GerarRepository = Console.ReadLine();

                            Console.WriteLine("Gerar Service N - Não S - Sim");
                            GerarService = Console.ReadLine();

                            Console.WriteLine("Gerar App Service N - Não S - Sim");
                            GerarAppService = Console.ReadLine();

                            Console.WriteLine("Gerar ViewModel N - Não S - Sim");
                            GerarViewModel = Console.ReadLine();
                        }

                        System.Console.WriteLine($"gerando Classe: {Classe} ");

                        
                        List<string> fileReadme = new List<string>();

                        fileReadme.Add(@"modelBuilder.Entity<ENEstRotinas>(entity =>");
                        fileReadme.Add(@"   entity.HasKey(e => new {");

                        List<string> fileContent = new List<string>();
                        var sql = @$"SELECT COLS.COLUMN_NAME, COLS.DATA_TYPE, COLS.DATA_LENGTH, NVL(COLS.DATA_PRECISION, 0) DATA_PRECISION,
                                            COLS.NULLABLE, COLS.DATA_DEFAULT, TRIM(longsubstrS('{Tabela.ToUpper()}', COLS.COLUMN_NAME,1,200)) DEFVAL,
                                            NVL(TBCONSTRAINTS.POSITION, -1) COLPRIMARY
                                       FROM COLS,
                                            (SELECT COLS.TABLE_NAME,
                                                    COLS.COLUMN_NAME,
                                                    COLS.POSITION,
                                                    CONS.STATUS,
                                                    CONS.OWNER
                                                    FROM ALL_CONSTRAINTS CONS,
                                                    ALL_CONS_COLUMNS COLS
                                                    WHERE CONS.CONSTRAINT_TYPE = 'P'
                                                    AND COLS.TABLE_NAME = '{Tabela.ToUpper()}'
                                                    AND CONS.CONSTRAINT_NAME = COLS.CONSTRAINT_NAME
                                                    AND CONS.OWNER = COLS.OWNER
                                                    ORDER BY COLS.TABLE_NAME,COLS.POSITION) TBCONSTRAINTS
                                      WHERE COLS.TABLE_NAME = '{Tabela.ToUpper()}'
                                        AND TBCONSTRAINTS.TABLE_NAME  (+)= COLS.TABLE_NAME
                                        AND TBCONSTRAINTS.COLUMN_NAME (+)= COLS.COLUMN_NAME
                                      ORDER BY COLS.COLUMN_ID";
                        var arqReadme = "./converted/IoC.txt";

                        if (ExistsConfig)
                            arqReadme = $"./converted/IoC-Line-{linhaAtual}.txt";

                        if (File.Exists(arqReadme))
                        {
                            try
                            {
                                File.Delete(arqReadme);
                            }
                            catch (Exception)
                            {

                            }
                        }

                        var arqNome = $"./converted/{Classe}.cs";

                        fileContent.Add("using System;");
                        fileContent.Add("using System.ComponentModel.DataAnnotations;");
                        fileContent.Add("using System.ComponentModel.DataAnnotations.Schema;");
                        fileContent.Add("using Hino.Infra.Cross.Utils.Attributes;");
                        fileContent.Add("");
                        fileContent.Add($"namespace Hino.Salesforce.Infra.Cross.Entities.{modulo}{local}");
                        fileContent.Add("{");
                        fileContent.Add($"    public class {Classe} : BaseEntity");
                        fileContent.Add("    {");

                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            bool primaryKeyAdded = false;
                            int ColumnIndex = -1;

                            var reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                ColumnIndex = Convert.ToInt32(reader["COLPRIMARY"]);

                                if (ColumnIndex > 0)
                                {
                                    fileContent.Add(@$"        [Key]");
                                    fileContent.Add(@$"        [Column(Order = {ColumnIndex})]");
                                    fileReadme.Add(@$"       e.{reader["COLUMN_NAME"].ToString().ToLower()},");
                                }

                                if (!primaryKeyAdded && ColumnIndex > 0 && reader["COLUMN_NAME"].ToString() != "CODESTAB")
                                {
                                    fileContent.Add(@$"        [PrimaryKey]");
                                    primaryKeyAdded = true;
                                }

                                fileContent.Add(@$"        public {GetConverted(reader)} {reader["COLUMN_NAME"].ToString().ToLower()} " + "{ get; set; }");
                            }
                            fileContent.Add("    }");
                            fileContent.Add("}");

                            fileReadme.Add(@"}));");
                            fileReadme.Add(@"");
                            if (File.Exists(arqNome))
                            {
                                try
                                {
                                    File.Delete(arqNome);
                                }
                                catch (Exception)
                                {

                                }
                            }

                            File.AppendAllLines(arqNome, fileContent.ToArray());

                            System.Console.WriteLine($"Classe: {Classe} gerada com sucesso");
                        }

                        arqNome = $"./converted/{Classe}VM.cs";
                        fileContent.Clear();

                        fileContent.Add("using System;");
                        fileContent.Add("using System.ComponentModel.DataAnnotations;");
                        fileContent.Add("using System.ComponentModel.DataAnnotations.Schema;");
                        fileContent.Add("using Hino.Salesforce.Infra.Cross.Utils.Attributes;");
                        fileContent.Add("");
                        fileContent.Add($"namespace Hino.Salesforce.Application.ViewModels.{modulo}{local}");
                        fileContent.Add("{");
                        fileContent.Add($"    public class {Classe}VM : BaseVM");
                        fileContent.Add("    {");

                        using (OracleCommand cmd = new OracleCommand(sql, conn))
                        {
                            var reader = cmd.ExecuteReader();
                            while (reader.Read())
                            {
                                var ret = GetConverted(reader);

                                fileContent.Add(@"        [DisplayField]");
                                if (!ret.Contains('?'))
                                    fileContent.Add(@"        [RequiredField]");

                                fileContent.Add(@$"        public {ret} {reader["COLUMN_NAME"].ToString().ToLower()} " + "{ get; set; }");
                            }
                            fileContent.Add("    }");
                            fileContent.Add("}");
                            if (File.Exists(arqNome))
                            {
                                try
                                {
                                    File.Delete(arqNome);
                                }
                                catch (Exception)
                                {

                                }
                            }

                            File.AppendAllLines(arqNome, fileContent.ToArray());

                            System.Console.WriteLine($"Classe: {Classe}VM gerada com sucesso");
                        }

                        var Entity = Classe;
                        arqNome = "";
                        if (GerarRepository.ToUpper() == "S")
                        {
                            arqNome = $"./converted/I{Entity}Repository.cs";
                            fileContent.Clear();

                            // gerando interface do Repository
                            System.Console.WriteLine($"gerando interface do repository do arquivo: {Classe} ");

                            fileContent.Add($"using Hino.Salesforce.Infra.Cross.Entities.{modulo}{local};");
                            fileContent.Add($"using Hino.Salesforce.Domain.Base.Interfaces.Repositories;");
                            fileContent.Add("");
                            fileContent.Add($"namespace Hino.Salesforce.Domain.{modulo}.Interfaces.Repositories{local}");
                            fileContent.Add("{");
                            fileContent.Add($"    public interface I{Entity}Repository : IBaseRepository<{Entity}>");
                            fileContent.Add("    {");
                            fileContent.Add("    }");
                            fileContent.Add("}");

                            if (File.Exists(arqNome))
                            {
                                try
                                {
                                    File.Delete(arqNome);
                                }
                                catch (Exception)
                                {

                                }
                            }

                            File.AppendAllLines(arqNome, fileContent.ToArray());

                            System.Console.WriteLine($"gerado interface do repository do arquivo: {Classe} com sucesso");

                            // gerando classe do Repository
                            System.Console.WriteLine($"gerando classe repository do arquivo: {Classe} ");

                            arqNome = $"./converted/{Entity}Repository.cs";
                            fileContent.Clear();

                            fileContent.Add($"using Hino.Salesforce.Domain.{modulo}.Interfaces.Repositories{local};");
                            fileContent.Add($"using Hino.Salesforce.Infra.Cross.Entities.{modulo}{local};");
                            fileContent.Add(@"using Hino.Salesforce.Infra.DataBase.Context;");
                            fileContent.Add("");
                            fileContent.Add($"namespace Hino.Salesforce.Infra.DataBase.Repositories.{modulo}{local}");
                            fileContent.Add("{");
                            fileContent.Add($"    public class {Entity}Repository : BaseRepository<{Entity}>, I{Entity}Repository");
                            fileContent.Add("    {");
                            fileContent.Add($"        public {Entity}Repository(AppDbContext appDbContext) : base(appDbContext)");
                            fileContent.Add("        {");
                            fileContent.Add("        }");
                            fileContent.Add("    }");
                            fileContent.Add("}");


                            if (File.Exists(arqNome))
                            {
                                try
                                {
                                    File.Delete(arqNome);
                                }
                                catch (Exception)
                                {

                                }
                            }

                            File.AppendAllLines(arqNome, fileContent.ToArray());

                            fileReadme.Add($"            services.AddTransient<I{Classe}Repository, {Classe}Repository>();");

                            System.Console.WriteLine($"gerado classe do repository do arquivo: {Classe} com sucesso");
                        }

                        if (GerarService.ToUpper() == "S")
                        {
                            // gerando interface do Service
                            System.Console.WriteLine($"gerando interface do service do arquivo: {Classe} ");

                            arqNome = $"./converted/I{Entity}Service.cs";
                            fileContent.Clear();
                            fileContent.Add($"using Hino.Salesforce.Infra.Cross.Entities.{modulo}{local};");
                            fileContent.Add($"using Hino.Salesforce.Domain.Base.Interfaces.Services;");
                            fileContent.Add("");
                            fileContent.Add($"namespace Hino.Salesforce.Domain.{modulo}.Interfaces.Services{local}");
                            fileContent.Add("{");
                            fileContent.Add($"    public interface I{Entity}Service : IBaseService<{Entity}>");
                            fileContent.Add("    {");
                            fileContent.Add("    }");
                            fileContent.Add("}");

                            if (File.Exists(arqNome))
                            {
                                try
                                {
                                    File.Delete(arqNome);
                                }
                                catch (Exception)
                                {

                                }
                            }
                            File.AppendAllLines(arqNome, fileContent.ToArray());

                            System.Console.WriteLine($"gerado interface do service do arquivo: {Classe} com sucesso");


                            // gerando classe do Service
                            System.Console.WriteLine($"gerando classe service do arquivo: {Classe} ");

                            arqNome = $"./converted/{Entity}Service.cs";
                            fileContent.Clear();

                            fileContent.Add($"using Hino.Salesforce.Domain.{modulo}.Interfaces.Repositories{local};");
                            fileContent.Add($"using Hino.Salesforce.Domain.{modulo}.Interfaces.Services{local};");
                            fileContent.Add($"using Hino.Salesforce.Infra.Cross.Entities.{modulo}{local};");
                            fileContent.Add($"using Hino.Salesforce.Domain.Base.Services;");
                            fileContent.Add("");
                            fileContent.Add($"namespace Hino.Salesforce.Domain.{modulo}.Services{local}");
                            fileContent.Add("{");
                            fileContent.Add($"    public class {Entity}Service : BaseService<{Entity}>, I{Entity}Service");
                            fileContent.Add("    {");
                            fileContent.Add($"        private readonly I{Entity}Repository _I{Entity}Repository;");
                            fileContent.Add("");
                            fileContent.Add($"        public {Entity}Service(I{Entity}Repository pI{Entity}Repository) : ");
                            fileContent.Add($"             base(pI{Entity}Repository)");
                            fileContent.Add("        {");
                            fileContent.Add($"            _I{Entity}Repository = pI{Entity}Repository;");
                            fileContent.Add("        }");
                            fileContent.Add("    }");
                            fileContent.Add("}");

                            if (File.Exists(arqNome))
                            {
                                try
                                {
                                    File.Delete(arqNome);
                                }
                                catch (Exception)
                                {

                                }
                            }
                            File.AppendAllLines(arqNome, fileContent.ToArray());

                            fileReadme.Add($"            services.AddTransient<I{Classe}Service, {Classe}Service>();");

                            System.Console.WriteLine($"gerado classe do service do arquivo: {Classe} com sucesso");
                        }


                        if (GerarAppService.ToUpper() == "S")
                        {
                            // gerando interface do Application Service
                            System.Console.WriteLine($"gerando interface do application service do arquivo: {Classe} ");

                            arqNome = $"./converted/I{Entity}AS.cs";
                            fileContent.Clear();
                            fileContent.Add($"using Hino.Salesforce.Infra.Cross.Entities.{modulo}{local};");
                            fileContent.Add($"using Hino.Salesforce.Application.Interfaces;");
                            fileContent.Add("");
                            fileContent.Add($"namespace Hino.Salesforce.Aplication.Interfaces.{modulo}{local}");
                            fileContent.Add("{");
                            fileContent.Add($"    public interface I{Entity}AS : IBaseAppService<{Entity}>");
                            fileContent.Add("    {");
                            fileContent.Add("    }");
                            fileContent.Add("}");

                            if (File.Exists(arqNome))
                            {
                                try
                                {
                                    File.Delete(arqNome);
                                }
                                catch (Exception)
                                {

                                }
                            }
                            File.AppendAllLines(arqNome, fileContent.ToArray());

                            System.Console.WriteLine($"gerado interface do application service do arquivo: {Classe} com sucesso");


                            // gerando classe do Application Service
                            System.Console.WriteLine($"gerando classe application service do arquivo: {Classe} ");

                            arqNome = $"./converted/{Entity}AS.cs";
                            fileContent.Clear();

                            fileContent.Add($"using Hino.Salesforce.Aplication.Interfaces.{modulo}{local};");
                            fileContent.Add($"using Hino.Salesforce.Application.Interfaces;");
                            fileContent.Add($"using Hino.Salesforce.Domain.{modulo}.Interfaces.Services{local};");
                            fileContent.Add($"using Hino.Salesforce.Infra.Cross.Entities.{modulo}{local};");
                            fileContent.Add("");
                            fileContent.Add($"namespace Hino.Aplication.Services.{modulo}{local}");
                            fileContent.Add("{");
                            fileContent.Add($"    public class {Entity}AS : BaseAppService<{Entity}>, I{Entity}AS");
                            fileContent.Add("    {");
                            fileContent.Add($"        private readonly I{Entity}Service _I{Entity}Service;");
                            fileContent.Add("");
                            fileContent.Add($"        public {Entity}AS(I{Entity}Service pI{Entity}Service) : ");
                            fileContent.Add($"             base(pI{Entity}Service)");
                            fileContent.Add("        {");
                            fileContent.Add($"            _I{Entity}Service = pI{Entity}Service;");
                            fileContent.Add("        }");
                            fileContent.Add("    }");
                            fileContent.Add("}");

                            if (File.Exists(arqNome))
                            {
                                try
                                {
                                    File.Delete(arqNome);
                                }
                                catch (Exception)
                                {

                                }
                            }
                            File.AppendAllLines(arqNome, fileContent.ToArray());
                            fileReadme.Add($"            services.AddTransient<I{Classe}AS, {Classe}AS>();");

                            System.Console.WriteLine($"gerado classe do application service do arquivo: {Classe} com sucesso");
                        }

                        if (fileReadme.Any())
                        {
                            if (File.Exists(arqReadme))
                            {
                                try
                                {
                                    File.Delete(arqReadme);
                                }
                                catch (Exception)
                                {

                                }
                            }
                            File.AppendAllLines(arqReadme, fileReadme.ToArray());
                        }
                    }
                    else
                        break;
                    if (!ExistsConfig)
                        Console.ReadKey();
                    else
                    {
                        linhaAtual++;
                        if (linhaAtual >= linhas.Length)
                            break;
                    }
                } while (true);
                if (ExistsConfig)
                    System.Console.WriteLine($"Processo concluido com sucesso");
            }


            Console.ReadKey();
        }

        static string GetConverted(OracleDataReader pLinha)
        {
            var column = "string";
            var dataPrecision = Convert.ToInt32(pLinha["DATA_PRECISION"]);

            if (pLinha["DATA_TYPE"].ToString() == "NUMBER" && pLinha["COLUMN_NAME"].ToString().StartsWith("COD"))
            {
                if (dataPrecision == 0)
                    column = "long";
            }
            
            if (column == "string" && pLinha["DATA_TYPE"].ToString() == "NUMBER")
            {
                var defaultVal = pLinha["DEFVAL"].ToString();
                if (dataPrecision == 1 && (defaultVal.ToString() == "0" || defaultVal.ToString() == "1") &&
                     pLinha["NULLABLE"].ToString() == "N")
                    column = "bool";
                else if (dataPrecision == 5)
                    column = "int";
                else if (dataPrecision > 1 && dataPrecision < 5)
                    column = "short";
                else if (dataPrecision > 5)
                    column = "long";
                else
                    column = "decimal";
            }
            else if (column == "string" && pLinha["DATA_TYPE"].ToString() == "DATE")
                column = "DateTime";

            if (column != "string" && pLinha["NULLABLE"].ToString() != "N")
            {
                column += "?";
            }

            return column;
        }
    }
}
