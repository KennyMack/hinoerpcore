using Hino.Domain.Estoque.Interfaces.Repositories;
using Hino.Domain.Estoque.Interfaces.Services;
using Hino.Infra.Cross.Entities.Estoque;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Estoque.Services
{
    public class ESKardexService : BaseService<ESKardex>, IESKardexService
    {
        private readonly IESKardexRepository _IESKardexRepository;

        public ESKardexService(IESKardexRepository pIESKardexRepository) : 
             base(pIESKardexRepository)
        {
            _IESKardexRepository = pIESKardexRepository;
        }
    }
}
