﻿using Hino.Domain.Base.Services;
using Hino.Domain.Estoque.Interfaces.Packages;
using Hino.Domain.Estoque.Interfaces.Services;
using Hino.Infra.Cross.Entities.Estoque;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Domain.Estoque.Services
{
    public class PCKG_Estoque : BasePackage, IPCKG_Estoque
    {
        private readonly IEstoquePackage _IEstoquePackage;
        public PCKG_Estoque(IEstoquePackage pIEstoquePackage) :
            base(pIEstoquePackage)
        {
            _IEstoquePackage = pIEstoquePackage;
        }

        public async Task<DateTime> BuscaDataFechamentoAsync(int pCodEstab) =>
            await _IEstoquePackage.BuscaDataFechamentoAsync(pCodEstab);

        public async Task<bool> GeraKardexAsync(ESKardex pKardex)
        {
            try
            {
                return await _IEstoquePackage.GeraKardexAsync(
                    pKardex);
            }
            catch (Exception ex)
            {
                var message = ex.Message.ClearMessage();

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { message }
                });
            }
            return false;
        }

        public async Task<bool> MovimentaSaldoLoteAsync(ESLoteSaldo pLoteSaldo, decimal pQuantidade, string pMovimento)
        {
            try
            {
                return await _IEstoquePackage.MovimentaSaldoLoteAsync(
                    pLoteSaldo, 
                    pQuantidade, pMovimento);
            }
            catch (Exception ex)
            {
                var message = ex.Message.ClearMessage();

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { message }
                });
            }
            return false;
        }
    }
}
