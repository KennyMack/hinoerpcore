using Hino.Domain.Estoque.Interfaces.Repositories.Lote;
using Hino.Domain.Estoque.Interfaces.Services.Lote;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Estoque.Services.Lote
{
    public class ESLoteService : BaseService<ESLote>, IESLoteService
    {
        private readonly IESLoteRepository _IESLoteRepository;

        public ESLoteService(IESLoteRepository pIESLoteRepository) : 
             base(pIESLoteRepository)
        {
            _IESLoteRepository = pIESLoteRepository;
        }
    }
}
