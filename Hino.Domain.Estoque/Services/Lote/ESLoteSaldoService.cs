using Hino.Domain.Estoque.Interfaces.Repositories.Lote;
using Hino.Domain.Estoque.Interfaces.Services.Lote;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Estoque.Services.Lote
{
    public class ESLoteSaldoService : BaseService<ESLoteSaldo>, IESLoteSaldoService
    {
        private readonly IESLoteSaldoRepository _IESLoteSaldoRepository;

        public ESLoteSaldoService(IESLoteSaldoRepository pIESLoteSaldoRepository) : 
             base(pIESLoteSaldoRepository)
        {
            _IESLoteSaldoRepository = pIESLoteSaldoRepository;
        }
    }
}
