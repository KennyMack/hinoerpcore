using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Estoque.Interfaces.Services.Lote
{
    public interface IESLoteSaldoService : IBaseService<ESLoteSaldo>
    {
    }
}
