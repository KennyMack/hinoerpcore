using Hino.Infra.Cross.Entities.Estoque;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Estoque.Interfaces.Services
{
    public interface IESKardexService : IBaseService<ESKardex>
    {
    }
}
