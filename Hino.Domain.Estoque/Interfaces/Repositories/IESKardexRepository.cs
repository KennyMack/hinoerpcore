using Hino.Infra.Cross.Entities.Estoque;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Estoque.Interfaces.Repositories
{
    public interface IESKardexRepository : IBaseRepository<ESKardex>
    {
    }
}
