using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Estoque.Interfaces.Repositories.Lote
{
    public interface IESLoteRepository : IBaseRepository<ESLote>
    {
    }
}
