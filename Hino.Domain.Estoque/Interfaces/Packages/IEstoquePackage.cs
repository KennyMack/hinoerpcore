﻿using Hino.Domain.Base.Interfaces.Packages;
using Hino.Infra.Cross.Entities.Estoque;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Domain.Estoque.Interfaces.Packages
{
    public interface IEstoquePackage : IBaseDBPackage
    {
        Task<DateTime> BuscaDataFechamentoAsync(int pCodEstab);
        Task<bool> GeraKardexAsync(ESKardex pKardex);
        Task<bool> MovimentaSaldoLoteAsync(ESLoteSaldo pLoteSaldo, decimal pQuantidade, string pMovimento);
    }
}
