﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Test
{
    class Program
    {
        public class BloggingContext : DbContext
        {
            public DbSet<Blog> Blogs { get; set; }
            public DbSet<Post> Posts { get; set; }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                optionsBuilder.UseOracle(@"User Id=HINOERP_TECTOR;Password=HINO;Data Source=192.168.15.103:1521/XE");
            }
        }

        [Table("BLOG")]
        public class Blog
        {
            [Column("BLOGID")]
            public int BlogId { get; set; }
            [Column("URL")]
            public string Url { get; set; }
            //public int Rating { get; set; }
            public List<Post> Posts { get; set; }
        }
        [Table("POST")]
        public class Post
        {
            [Column("POSTID")]
            public int PostId { get; set; }
            [Column("URL")]
            public string Url { get; set; }

            [Column("BLOGID")]
            public int BlogId { get; set; }
            public Blog Blog { get; set; }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            using (var db = new BloggingContext())
            {
                var blog = new Blog { Url = "https://blogs.oracle.com" };
                blog.BlogId = 1;
                db.Blogs.Add(blog);
                db.SaveChanges();
            }

            using (var db = new BloggingContext())
            {
                var blogs = db.Blogs;
            }

            Console.ReadKey();
        }
    }
}
