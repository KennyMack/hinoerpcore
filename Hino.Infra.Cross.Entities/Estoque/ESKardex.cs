﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Estoque
{
    public class ESKardex: BaseEntity
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codkardex { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        public decimal? indiceinftem { get; set; }
        public short? seqnfitem { get; set; }
        public long? codlancamento { get; set; }
        public DateTime data { get; set; }
        public string codtipomov { get; set; }
        public string codproduto { get; set; }
        public string codunidade { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }
        public decimal valortotal { get; set; }
        public decimal precomedio { get; set; }
        public long? lote { get; set; }
        public string observacao { get; set; }
        public decimal? codconsumo { get; set; }
        public decimal? codrefugo { get; set; }
        public decimal? codajuste { get; set; }
        public decimal? codrequisicao { get; set; }
        public decimal? codromaneiosep { get; set; }
        public decimal? codsucata { get; set; }
        public string codusuario { get; set; }
        public long? codinsprecebe { get; set; }
        [NotMapped]
        public long? codtransfest { get; set; }
        public int? codsolic { get; set; }
        public long? codinv { get; set; }
        public long? codsobra { get; set; }
        public long? codromaneiosepdet { get; set; }
        public long? codreserva { get; set; }
        public long? codempenho { get; set; }
        [NotMapped]
        public long? codordprod { get; set; }
        [NotMapped]
        public string nivelordprod { get; set; }
        public long? indiceromitem { get; set; }
        public long? codromseparacao { get; set; }
        public decimal? valorunitario { get; set; }
    }
}
