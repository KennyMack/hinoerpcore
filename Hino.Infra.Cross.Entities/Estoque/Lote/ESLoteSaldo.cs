﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Estoque.Lote
{
    public class ESLoteSaldo : BaseEntity
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long lote { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        public string codestoque { get; set; }
        public decimal qtdentrada { get; set; }
        public decimal qtdsaida { get; set; }
        [NotMapped]
        public string codproduto { get; set; }
    }
}
