﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Infra.Cross.Utils.Attributes;

namespace Hino.Infra.Cross.Entities.Engenharia.Estrutura
{
    public class ENEstRotinas : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public long codestrutura { get; set; }
        [Key]
        [Column(Order = 3)]
        public short codroteiro { get; set; }
        public short codrotina { get; set; }
        public string descricao { get; set; }
        [Key]
        [Column(Order = 4)]
        public short operacao { get; set; }
        public string codmaquina { get; set; }
        public decimal prodhoraria { get; set; }
        public decimal operador { get; set; }
        public decimal setup { get; set; }
        public bool aptoprocesso { get; set; }
        public bool aptoacabado { get; set; }
        public decimal segundosporpeca { get; set; }
        public decimal minutossporpeca { get; set; }
        public string codmaqopc { get; set; }
        public decimal ciclopeca { get; set; }
        public decimal pecaciclo { get; set; }
        public string areaproduto { get; set; }
    }
}
