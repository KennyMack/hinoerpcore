﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Infra.Cross.Utils.Attributes;

namespace Hino.Infra.Cross.Entities.Engenharia.Estrutura
{
    public class ENEstComponentes : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public long codestrutura { get; set; }
        [Key]
        [Column(Order = 3)]
        public short codroteiro { get; set; }
        [Key]
        [Column(Order = 4)]
        public short operacao { get; set; }
        [Key]
        [Column(Order = 5)]
        public string componente { get; set; }
        public string estoquebaixa { get; set; }
        public decimal quantidade { get; set; }
        public bool baixaapto { get; set; }
        public int variacao { get; set; }
        public int perda { get; set; }
        public bool reaproveita { get; set; }
        public string codagrup { get; set; }
        public decimal geraconsudif { get; set; }
        public decimal sequencia { get; set; }
        public decimal camadas { get; set; }
        public decimal passadas { get; set; }
        public short nivel { get; set; }
        public int? codtecido { get; set; }
        public string codunidade { get; set; }
        public decimal qtdeeng { get; set; }
        public string espectec { get; set; }
        public bool separacao { get; set; }
    }
}
