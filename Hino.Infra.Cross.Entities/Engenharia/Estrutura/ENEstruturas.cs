﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Infra.Cross.Utils.Attributes;

namespace Hino.Infra.Cross.Entities.Engenharia.Estrutura
{
    public class ENEstruturas : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int codestab { get; set; }
        [Key]
        [PrimaryKey]
        [Column(Order = 2)]
        public long codestrutura { get; set; }
        public string codproduto { get; set; }
        public short codprocesso { get; set; }
        public string descricao { get; set; }
        public string status { get; set; }
        public decimal versao { get; set; }
        public DateTime cadastro { get; set; }
        public DateTime vigenciainicio { get; set; }
        public string cadusuario { get; set; }
        public string aprovusuario { get; set; }
        public string codestoque { get; set; }
        public string fantasma { get; set; }
        public string cronoanalise { get; set; }
        public decimal kgporbatida { get; set; }
        public long? codcor { get; set; }
        public string numdesenho { get; set; }
        public long? codorcitem { get; set; }
        public bool exigeduzentos { get; set; }
    }
}
