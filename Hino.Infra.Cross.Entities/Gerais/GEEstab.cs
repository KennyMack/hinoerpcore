﻿using Hino.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Infra.Cross.Entities.Gerais
{
    [Table("GEESTAB")]
    [Sequence("SEQ_GEESTAB")]
    public class GEEstab : BaseEntity
    {
        [Key]
        [PrimaryKey]
        public int codestab { get; set; }
        public string razaosocial { get; set; }
        public string nomefantasia { get; set; }
        public int? codendereco { get; set; }
        public short codmoeda { get; set; }
        public short ativo { get; set; }
        public short cdquantidade { get; set; }
        public short cdvlunitario { get; set; }
        public short cdvltotal { get; set; }
        public short cdpercentual { get; set; }
        public short cdtamanhocubico { get; set; }
        public int? codnatopercompra { get; set; }
        public int? codnatopervenda { get; set; }
        public short? gerarcontaempresa { get; set; }
        public string caminhoimagens { get; set; }
        public short consestoque { get; set; }
        public string certificado { get; set; }
        public int? codgrupoestab { get; set; }
        public short usaunnegocio { get; set; }
        public string tokenbuscacnpj { get; set; }
        public short obrigafotovisita { get; set; }
        public string awsaccesskey { get; set; }
        public string awsaccesssecret { get; set; }
        public string awsbucket { get; set; }
        public string awsregion { get; set; }
    }
}
