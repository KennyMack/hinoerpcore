﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Gerais
{
    public class GEUsuarios : BaseEntity
    {
        [Key]
        [PrimaryKey]
        public string codusuario { get; set; }
        public string nome { get; set; }
        public bool status { get; set; }
        public short tipusuario { get; set; }
        public string senha { get; set; }
        public string controle { get; set; }
        public string email { get; set; }
        public bool visualizasc { get; set; }
        public bool vervalprogrec { get; set; }
        public string vervalromaneio { get; set; }
        public bool admincalendario { get; set; }
        public bool alteracomissao { get; set; }
        public bool alteratblprvig { get; set; }
        public bool visvlrultcmpsc { get; set; }
        public bool visvlrreqest { get; set; }
        public bool naopaginagrid { get; set; }
        public bool viscomprarestrito { get; set; }
        public bool permiteestoqneg { get; set; }
        public bool visempresaprod { get; set; }
        public bool admorcamento { get; set; }
        public string senhaemail { get; set; }
        public bool vervalmanut { get; set; }

    }
}
