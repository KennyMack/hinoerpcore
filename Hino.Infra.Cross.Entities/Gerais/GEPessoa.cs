﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Gerais
{
    public class GEPessoa: BaseEntity
    {
        public GEPessoa()
        {
            this.GEFuncionarios = new HashSet<GEFuncionarios>();
        }

        [Key]
        [PrimaryKey]
        public int codpessoa { get; set; }
        public string nome { get; set; }
        public long? numregnasc { get; set; }
        public string livroregnasc { get; set; }
        public long? folharegnasc { get; set; }
        public string sexo { get; set; }
        public DateTime dtnascimento { get; set; }
        public string raca { get; set; }
        public short? tiposanguineo { get; set; }
        public short? cabelo { get; set; }
        public short? olhos { get; set; }
        public int naturalidade { get; set; }
        public int? codreligiao { get; set; }
        public string profissao { get; set; }
        public string localtrabalho { get; set; }
        public string numcpf { get; set; }
        public string rg { get; set; }
        public string matriculanasc { get; set; }
        public string orgaoexped { get; set; }
        public string estadocivil { get; set; }


        public virtual ICollection<GEFuncionarios> GEFuncionarios { get; set; }

    }
}
