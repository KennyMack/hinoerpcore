﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Gerais
{
    [Table("GEEMPRESA")]
    public class GEEmpresa : BaseEntity
    {
        [Key]
        [PrimaryKey]
        public int codempresa { get; set; }
        [PaginateBy]
        public string razaosocial { get; set; }
        public string nomefantasia { get; set; }
    }
}
