﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Producao
{
    public class PDMotivos : BaseEntity
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public int codmotivo { get; set; }
        public string descricao { get; set; }
        public short tipo { get; set; }
        public bool status { get; set; }
    }
}
