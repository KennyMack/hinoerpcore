﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Producao
{
    public class PDParamEstab : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int codestab { get; set; }
        public bool calculandomps { get; set; }
        public bool calculandomrp { get; set; }
        public string leadtimeop { get; set; }
        public bool exigemaqapto { get; set; }
        public bool exigeturnoapto { get; set; }
        public string aceitaprgmaior { get; set; }
        public string encerraautomatico { get; set; }
        public string geraopfilhos { get; set; }
        public bool recalcopaltcomp { get; set; }
        public bool regramaqaponta { get; set; }
        public bool contapontproc { get; set; }
        public bool consumolote { get; set; }
        public bool fifolotesconsumo { get; set; }
        public bool inputsensor { get; set; }
        public bool apontalogin { get; set; }
        public bool custoapont { get; set; }
        public bool taxascustoapt { get; set; }
        public bool materiaiscustoapt { get; set; }
        public bool contestoqref { get; set; }
        public bool validadtconsumo { get; set; }
        public bool validadtsucata { get; set; }
        public bool validadtapont { get; set; }
        public decimal minutosapont { get; set; }
        public bool transfmodo { get; set; }
        public bool tempovalapont { get; set; }
        public int? codmotsuc { get; set; }
        public bool horamanualapt { get; set; }
        public bool calcoee { get; set; }
        public bool imprimelanc { get; set; }
        public int numseqfotolito { get; set; }
        public short? codrotencomenda { get; set; }
        public int? codmotrefugo { get; set; }
        public int? codmotparada { get; set; }
        public int? codmotretrab { get; set; }
        public int? codmotsetup { get; set; }
        public int? codmotdesenho { get; set; }
        public int? codmottrserv { get; set; }
        public int? codmotmanut { get; set; }
        public string turnopadrao { get; set; }
        public string codmaqpadrao { get; set; }
        public bool procsequencial { get; set; }
        public bool situacaoop { get; set; }
        public bool aptproducao { get; set; }
        public bool aptsetup { get; set; }
        public bool apttrocaserv { get; set; }
        public bool aptmanut { get; set; }
        public bool aptconsdesenho { get; set; }
        public bool aptretrab { get; set; }
        public bool exigetransfini { get; set; }
        public bool validacompoper { get; set; }
        public bool valaptoperanterior { get; set; }
        public bool usaempenho { get; set; }
        public bool reapautomatico { get; set; }
        public bool validaopsaldo { get; set; }
        public bool usasaldoestoque { get; set; }
        public bool tipofifolote { get; set; }
        public decimal percaumento { get; set; }
        public decimal percfinalizar { get; set; }

    }
}
