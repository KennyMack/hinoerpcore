﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Producao.Refugos
{
    public class PDRefugoReap : BaseEntity
    {
        [Key]
        [PrimaryKey]
        public long codreaproveitado { get; set; }
        public long codrefugo { get; set; }
        public int codestab { get; set; }
        public long codordprod { get; set; }
        public string nivelordprod { get; set; }
        public long codestrutura { get; set; }
        public short codroteiro { get; set; }
        public short operacao { get; set; }
        public string codcomponente { get; set; }
        public decimal qtdreaproveitada { get; set; }
    }
}
