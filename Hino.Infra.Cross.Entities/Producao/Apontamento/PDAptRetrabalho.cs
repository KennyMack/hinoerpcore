﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Producao.Apontamento
{
    public class PDAptRetrabalho : BaseEntity
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codfimapt { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 3)]
        public long codordprod { get; set; }
        [Key]
        [Column(Order = 4)]
        public string nivelordprod { get; set; }
        [Key]
        [Column(Order = 5)]
        public long codestrutura { get; set; }
        [Key]
        [Column(Order = 6)]
        public long codretrab { get; set; }
    }
}
