﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Producao.Apontamento
{
    public class PDRetrabalho : BaseEntity
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codordprod { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public string nivelordprod { get; set; }
        [Key]
        [Column(Order = 2)]
        public long codestrutura { get; set; }
        [Key]
        [Column(Order = 2)]
        public long codretrab { get; set; }
        public short operacao { get; set; }
        public DateTime dtinicio { get; set; }
        public DateTime dttermino { get; set; }
        public int codfuncionario { get; set; }
        public string codestoque { get; set; }
        public long? lote { get; set; }
        public decimal quantidade { get; set; }
        public int codmotivo { get; set; }
    }
}
