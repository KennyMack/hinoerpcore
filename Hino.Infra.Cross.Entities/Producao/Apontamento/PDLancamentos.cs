﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Producao.Apontamento
{
    [Sequence("SEQ_PDLANCAMENTO")]
    public class PDLancamentos : BaseEntity
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codlancamento { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        public long codordprod { get; set; }
        public string nivelordprod { get; set; }
        public short operacao { get; set; }
        public int codfuncionario { get; set; }
        public decimal quantidade { get; set; }
        public DateTime dtinicio { get; set; }
        public DateTime dttermino { get; set; }
        public long codestrutura { get; set; }
        public short codroteiro { get; set; }
        public long? codlancpai { get; set; }
        public string turno { get; set; }
        public string codmaquina { get; set; }
        public long? codcor { get; set; }
        public string lotemanual { get; set; }
        public bool reginsp { get; set; }
        public int? codmotivo { get; set; }
        public string codccusto { get; set; }
    }
}
