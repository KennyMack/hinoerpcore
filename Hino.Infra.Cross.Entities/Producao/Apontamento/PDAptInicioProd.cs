﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Entities.Producao.Apontamento
{
    public class PDAptInicioProd : BaseEntity
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codiniapt { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        public long codordprod { get; set; }
        public string nivelordprod { get; set; }
        public long codestrutura { get; set; }
        public short codroteiro { get; set; }
        public short operacao { get; set; }
        public int codfuncionario { get; set; }
        public string codusuario { get; set; }
        public DateTime dtinicio { get; set; }
        public short tipo { get; set; }
    }
}
