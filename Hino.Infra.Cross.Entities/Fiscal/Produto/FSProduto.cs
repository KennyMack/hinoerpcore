﻿using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Hino.Infra.Cross.Entities.Fiscal.Produto
{
    public class FSProduto : BaseEntity
    {
        public FSProduto()
        {
            //this.PDOrdemProdComp = new HashSet<PDOrdemProdComp>();
            this.FSProdutoPCP = new HashSet<FSProdutoPCP>();
            this.FSProdutoParamEstab = new HashSet<FSProdutoParamEstab>();
        }
        [Key]
        [PrimaryKey]
        public string codproduto { get; set; }
        public string descricao { get; set; }
        public string detalhamento { get; set; }
        public string infadicfiscal { get; set; }

        //public virtual ICollection<PDOrdemProdComp> PDOrdemProdComp { get; set; }
        public virtual ICollection<FSProdutoPCP> FSProdutoPCP { get; set; }
        public virtual ICollection<FSProdutoParamEstab> FSProdutoParamEstab { get; set; }
    }
}
