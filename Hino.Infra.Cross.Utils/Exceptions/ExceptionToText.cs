﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hino.Infra.Cross.Utils.Exceptions
{
    public static class ExceptionToText
    {
        public static string ToMessage(this List<ModelException> Errors)
        {
            if (Errors != null && Errors.Any())
            {
                var Messages = Errors.Select(r => string.Join("\r\n", r.Messages));

                return string.Join("\r\n", Messages);
            }


            return "";
        }
    }
}
