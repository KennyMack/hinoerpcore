﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Hino.Infra.Cross.Utils.Exceptions
{
    public static class HelperOracleException
    {
        public static string ClearMessage(this string pMessage)
        {
            if (pMessage.IndexOf("20099") > -1)
            {
                Regex red = new Regex(@"((99\:)(.*))", RegexOptions.IgnoreCase);
                return red.Matches(pMessage)[0].Value.Substring(3);
            }
            else if (pMessage.IndexOf("20010") > -1)
            {
                Regex red = new Regex(@"((10\:)(.*))", RegexOptions.IgnoreCase);
                return red.Matches(pMessage)[0].Value.Substring(3);
            }
            else if (pMessage.IndexOf("20001") > -1)
            {
                Regex red = new Regex(@"((01\:)(.*))", RegexOptions.IgnoreCase);
                return red.Matches(pMessage)[0].Value.Substring(3);
            }

            return pMessage;
        }
    }
}
