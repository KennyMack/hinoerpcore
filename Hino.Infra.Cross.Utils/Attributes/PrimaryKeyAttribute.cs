﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class PrimaryKeyAttribute : DatabaseGeneratedAttribute
    {
        public bool HasSequence
        {
            get;
            private set;
        }
        public PrimaryKeyAttribute(bool pHasSequence = true) :
            base(DatabaseGeneratedOption.None)
        {
            HasSequence = pHasSequence;
        }
    }
}
