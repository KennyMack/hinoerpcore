﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Hino.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class GreatherThanZero : ValidationAttribute
    {
        public GreatherThanZero()
        {
            ErrorMessageResourceName = "GreatherThanZero";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }

        /// <summary>
        /// Valor maior que zero
        /// </summary>
        /// <param name="value">Valor</param>
        /// <returns>verdadeiro caso o valor seja maior que zero</returns>
        public override bool IsValid(object value)
        {
            // return true if value is a non-null number > 0, otherwise return false
            decimal i;
            return value != null && decimal.TryParse(value.ToString(), out i) && i > 0;
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class QuantityGreatherThanZero : ValidationAttribute
    {
        public QuantityGreatherThanZero()
        {
            ErrorMessageResourceName = "QuantityGreatherThanZero";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }

        /// <summary>
        /// Valor maior que zero
        /// </summary>
        /// <param name="value">Valor</param>
        /// <returns>verdadeiro caso o valor seja maior que zero</returns>
        public override bool IsValid(object value)
        {
            // return true if value is a non-null number > 0, otherwise return false
            decimal i;
            return value != null && decimal.TryParse(value.ToString(), out i) && i > 0;
        }
    }
}
