﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Infra.Cross.Utils.Attributes
{
    public class RequiredFieldAttribute: RequiredAttribute
    {
        public RequiredFieldAttribute()
        {
            AllowEmptyStrings = false;
            ErrorMessageResourceName = "RequiredDefault";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource); 
        }
    }
}
