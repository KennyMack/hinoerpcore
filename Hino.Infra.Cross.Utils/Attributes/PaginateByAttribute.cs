﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class PaginateByAttribute : Attribute
    {
        public bool Descending
        {
            get;
            private set;
        }
        public PaginateByAttribute(bool pDescending = true)
        {
            Descending = pDescending;
        }
    }
}
