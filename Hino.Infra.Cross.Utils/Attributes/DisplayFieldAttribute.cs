﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Hino.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DisplayFieldAttribute : DisplayNameAttribute
    {
        public DisplayFieldAttribute([CallerMemberName]string propertyName = "")
        {
            var a = new System.Resources.ResourceManager(typeof(Resources.FieldsNameResource));

            DisplayNameValue = a.GetString(propertyName);
        }
    }
}
