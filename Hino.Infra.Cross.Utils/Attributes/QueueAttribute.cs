﻿using System;

namespace Hino.Infra.Cross.Utils.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class QueueAttribute : Attribute
    {
        public QueueAttribute(string pQueue)
        {
            this.Queue = pQueue;
        }

        public string Queue
        {
            get;
            private set;
        }
    }
}
