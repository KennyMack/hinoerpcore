﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Infra.Cross.Utils.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class SequenceAttribute : Attribute
    {
        // This is a positional argument
        public SequenceAttribute(string pSequenceName)
        {
            this.SequenceName = pSequenceName;
        }

        public string SequenceName
        {
            get;
            private set;
        }
    }
}
