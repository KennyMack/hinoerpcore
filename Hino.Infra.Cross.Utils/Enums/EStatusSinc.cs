﻿using System.ComponentModel.DataAnnotations;

namespace Hino.Infra.Cross.Utils.Enums
{
    public enum EStatusSinc
    {
        [Display(Description = "Aguardando sincronização")]
        Waiting = 0,
        [Display(Description = "Sincronizado")]
        Sincronized = 1,
        [Display(Description = "Integrado")]
        Integrated = 2
    }
}
