﻿namespace Hino.Infra.Cross.Utils
{
    public enum EGender
    {
        NotDefined = 0,
        Male = 1,
        Female = 2,
        Other = 3
    }
}
