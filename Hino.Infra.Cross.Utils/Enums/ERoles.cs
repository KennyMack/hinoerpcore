﻿using System.ComponentModel.DataAnnotations;

namespace Hino.Infra.Cross.Utils.Enums
{
    public enum ERoles
    {
        [Display(Description = "Ajudante")]
        Assistant = 0,

        [Display(Description = "Vendedor")]
        Sallesman = 1,

        [Display(Description = "Representante")]
        Representant = 2,

        [Display(Description = "Diretor")]
        Director = 3,

        [Display(Description = "Administrador")]
        Master = 4
    }
}
