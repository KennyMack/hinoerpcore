﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Infra.Cross.Utils.Enums
{
    public enum EEstadoAtualApontamento
    {
        SELECIONALOTE = 0,
        SEMLOTE = 1,
        ERRO = 2
    }
}
