﻿CREATE OR REPLACE FUNCTION longsubstrS( 
p_TABLE in VARCHAR2,
P_COLUMN IN VARCHAR2,
p_from in number,
p_for in number )
return varchar2
Is
l_tmp long;
begin
select DATA_DEFAULT into l_tmp from COLS 
where COLS.TABLE_NAME = p_TABLE
AND COLS.COLUMN_NAME = P_COLUMN;

return substr( l_tmp , p_from, p_for );
end;
/