﻿using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Infra.Cross.Entities.Estoque;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Infra.Cross.Entities.Fiscal.Estoque;
using Hino.Infra.Cross.Entities.Fiscal.Produto;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.Cross.Entities.Producao.Refugos;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hino.Infra.DataBase.Context
{
    public class AppDbContext : DbContext
    {
        public virtual DbSet<GEEstab> GEEstab { get; set; }
        public virtual DbSet<GEPessoa> GEPessoa { get; set; }
        public virtual DbSet<GEEmpresa> GEEmpresa { get; set; }
        public virtual DbSet<ESKardex> ESKardex { get; set; }
        public virtual DbSet<ESLote> ESLote { get; set; }
        public virtual DbSet<ESLoteSaldo> ESLoteSaldo { get; set; }
        public virtual DbSet<FSProduto> FSProduto { get; set; }
        public virtual DbSet<FSProdutoParamEstab> FSProdutoParamEstab { get; set; }
        public virtual DbSet<FSProdutoPCP> FSProdutoPCP { get; set; }
        public virtual DbSet<FSSaldoEstoque> FSSaldoEstoque { get; set; }
        public virtual DbSet<GEEtiqueta> GEEtiqueta { get; set; }
        public virtual DbSet<GEFuncionarios> GEFuncionarios { get; set; }
        public virtual DbSet<GEUsuarios> GEUsuarios { get; set; }
        public virtual DbSet<PDAptInicioProd> PDAptInicioProd { get; set; }
        public virtual DbSet<PDAptRetrabalho> PDAptRetrabalho { get; set; }
        public virtual DbSet<PDAptTerminoProd> PDAptTerminoProd { get; set; }
        public virtual DbSet<PDLancamentos> PDLancamentos { get; set; }
        public virtual DbSet<PDRetrabalho> PDRetrabalho { get; set; }
        public virtual DbSet<PDOrdemProd> PDOrdemProd { get; set; }
        public virtual DbSet<PDOrdemProdComp> PDOrdemProdComp { get; set; }
        public virtual DbSet<PDOrdemProdRotinas> PDOrdemProdRotinas { get; set; }
        public virtual DbSet<PDRefugoMot> PDRefugoMot { get; set; }
        public virtual DbSet<PDRefugoReap> PDRefugoReap { get; set; }
        public virtual DbSet<PDRefugos> PDRefugos { get; set; }
        public virtual DbSet<PDMotivos> PDMotivos { get; set; }
        public virtual DbSet<PDParamEstab> PDParamEstab { get; set; }

        public virtual DbSet<ENEstruturas> ENEstruturas { get; set; }
        public virtual DbSet<ENEstComponentes> ENEstComponentes { get; set; }
        public virtual DbSet<ENEstRotinas> ENEstRotinas { get; set; }

        public static readonly ILoggerFactory MyLoggerFactory
            = LoggerFactory.Create(builder => { builder.AddConsole(); });
        public AppDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseLoggerFactory(MyLoggerFactory);
            // optionsBuilder.UseOracle(@"User Id=HINOERP_TECTOR;Password=HINO;Data Source=192.168.15.103:1521/XE",
            optionsBuilder.UseOracle(@"User Id=HINOERP_PLT;Password=HINOAWS;Data Source=rdsoraclehinobrsp01dev.cde1pczayzub.sa-east-1.rds.amazonaws.com:11030/ORCL",
                options => options
                    .UseOracleSQLCompatibility("11"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GEFuncionarios>(entity =>
                entity.HasKey(e => new { e.codestab, e.codfuncionario }));

            modelBuilder.Entity<ESKardex>(entity =>
                entity.HasKey(e => new { e.codestab, e.codkardex }));

            modelBuilder.Entity<ESLote>(entity =>
                entity.HasKey(e => new { e.codestab, e.lote }));

            modelBuilder.Entity<ESLoteSaldo>(entity =>
                entity.HasKey(e => new { e.codestab, e.lote, e.codestoque }));

            modelBuilder.Entity<FSProduto>(entity =>
                entity.HasKey(e => new { e.codproduto }));

            modelBuilder.Entity<FSProdutoParamEstab>(entity =>
            {
                entity
                    .HasOne(s => s.FSProduto)
                    .WithMany(c => c.FSProdutoParamEstab)
                    .HasForeignKey(s => new { s.codproduto });
                entity.HasKey(e => new { e.codestab, e.codproduto });
            });

            modelBuilder.Entity<FSProdutoPCP>(entity =>
            {
                entity
                    .HasOne(s => s.FSProduto)
                    .WithMany(c => c.FSProdutoPCP)
                    .HasForeignKey(s => new { s.codproduto });
                entity.HasKey(e => new { e.codestab, e.codproduto });
            });

            modelBuilder.Entity<FSSaldoEstoque>(entity =>
                entity.HasKey(e => new { e.codestab, e.codproduto, e.codestoque }));

            modelBuilder.Entity<PDAptInicioProd>(entity =>
                entity.HasKey(e => new { e.codestab, e.codiniapt }));

            modelBuilder.Entity<PDAptRetrabalho>(entity =>
                entity.HasKey(e => new { 
                    e.codestab, 
                    e.codfimapt,
                    e.codordprod,
                    e.nivelordprod,
                    e.codestrutura,
                    e.codretrab
                }));

            modelBuilder.Entity<PDAptTerminoProd>(entity =>
                entity.HasKey(e => new { e.codestab, e.codfimapt }));

            modelBuilder.Entity<PDLancamentos>(entity =>
                entity.HasKey(e => new { e.codestab, e.codlancamento }));

            modelBuilder.Entity<PDRetrabalho>(entity =>
                entity.HasKey(e => new {
                    e.codestab, 
                    e.codordprod, 
                    e.nivelordprod, 
                    e.codestrutura, 
                    e.codretrab 
                }));

            modelBuilder.Entity<PDOrdemProd>(entity =>
                entity.HasKey(e => new {
                    e.codestab, 
                    e.codordprod,
                    e.nivelordprod,
                    e.codestrutura
                }));

            modelBuilder.Entity<PDOrdemProdComp>(entity =>
            {
                entity
                    .HasOne(s => s.FSProdutoParamEstab)
                    .WithMany(c => c.PDOrdemProdComp)
                    .HasForeignKey(s => new { s.codestab, s.codcomponente });
                entity
                    .HasOne(s => s.FSProdutoPCP)
                    .WithMany(c => c.PDOrdemProdComp)
                    .HasForeignKey(s => new { s.codestab, s.codcomponente });
                entity.HasKey(e => new
                {
                    e.codestab,
                    e.codordprod,
                    e.nivelordprod,
                    e.codestrutura,
                    e.codroteiro,
                    e.operacao,
                    e.codcomponente
                });
            });

            modelBuilder.Entity<PDOrdemProdRotinas>(entity =>
                entity.HasKey(e => new {
                    e.codordprod,
                    e.nivelordprod,
                    e.codestab,
                    e.codestrutura,
                    e.codroteiro,
                    e.operacao
                }));

            modelBuilder.Entity<PDRefugoMot>(entity =>
                entity.HasKey(e => new { e.codmotivo, e.codrefugo }));

            modelBuilder.Entity<ENEstruturas>(entity =>
                entity.HasKey(e => new {
                    e.codestab,
                    e.codestrutura
                }));

            modelBuilder.Entity<ENEstComponentes>(entity =>
                entity.HasKey(e => new { 
                    e.codestab, 
                    e.codestrutura,
                    e.codroteiro,
                    e.operacao,
                    e.componente
                }));

            modelBuilder.Entity<ENEstRotinas>(entity =>
                entity.HasKey(e => new {
                    e.codestab,
                    e.codestrutura,
                    e.codroteiro,
                    e.operacao
                }));

            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            var tables = modelBuilder.Model.GetEntityTypes();

            foreach (var table in tables)
            {
                table.SetTableName(table.GetTableName().ToUpper());
                foreach (var col in table.GetProperties())
                    col.SetColumnName(col.GetColumnName().ToUpper());
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}
