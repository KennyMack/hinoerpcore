﻿using Hino.Domain.Base.Interfaces.Packages;
using Hino.Infra.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Infra.DataBase.Packages
{
    public class BaseDBPackage: IDisposable, IBaseDBPackage
    {
        protected AppDbContext DbConn;

        public BaseDBPackage(AppDbContext appDbContext)
        {
            DbConn = appDbContext;
        }

        public void Dispose()
        {
            DbConn.Dispose();
        }
    }
}
