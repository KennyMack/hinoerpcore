﻿using Hino.Domain.Producao.Interfaces.Packages;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.Cross.Utils;
using Hino.Infra.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Infra.DataBase.Packages.Producao
{
    public class ProducaoPackage: BaseDBPackage, IProducaoPackage
    {
        public ProducaoPackage(AppDbContext appDbContext)
             : base(appDbContext)
        {
        }

        public async Task<bool> VerificaOperacoesAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, short pOperacao, decimal pQuantidade)
        {
            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.VERIFICAOPERACOES";
                cmd.CommandType = CommandType.StoredProcedure;

                DbConn.Database.OpenConnection();

                cmd.Parameters.Add(new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pOPERACAOATUAL", OracleDbType.Varchar2, pOperacao, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQUANTIDADE", OracleDbType.Decimal, pQuantidade, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output));

                await cmd.ExecuteNonQueryAsync();
                if (((OracleDecimal)(cmd.Parameters["nRETURN"].Value)).Value == 1)
                    return false;

                // Tratando retorno da função
                var mensErro = ((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value;
                mensErro = mensErro == "*OK*" ? "" : mensErro;

                if (!mensErro.IsEmpty())
                    throw new Exception(mensErro);
            }
            return true;
        }

        #region Package Produção Procedure GETVALIDAOPAPTO
        /// <summary>
        /// Valida o apontamento de produção
        /// </summary>
        /// <param name="msErro">Mensagem de erro</param>
        /// <returns>bool</returns>
        public async Task<bool> GetValidaOpAptoAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, short pOperacao, decimal pQuantidade)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.GETVALIDAOPAPTO";
                cmd.CommandType = CommandType.StoredProcedure;

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("nRETURN", OracleDbType.Clob, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODESTRUTURA", OracleDbType.Int64, pCodEstrutura, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQUANTIDADE", OracleDbType.Decimal, pQuantidade, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pOPERACAO", OracleDbType.Varchar2, pOperacao, ParameterDirection.Input));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();

                // Tratando retorno da função
                var mensErro = ((OracleClob)(cmd.Parameters["nRETURN"].Value)).Value;
                if (mensErro != "**OK**")
                    throw new Exception(mensErro);
            }

            return retorno;
        }
        #endregion

        #region Package PRODUCAO - Function CHECKUSERPERLOCEST
        /// <summary>
        /// VERIFICA SE O USUÁRIO TEM ACESSO AO LOCAL DE ESTOQUE
        /// </summary>
        /// <returns>bool</returns>
        public async Task<bool> CheckUserPerLocEstAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, long pCodFuncionario)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.CHECKUSERPERLOCEST";
                cmd.CommandType = CommandType.StoredProcedure;

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("nRETURN", OracleDbType.Clob, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODESTRUTURA", OracleDbType.Int64, pCodEstrutura, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODFUNCIONARIO", OracleDbType.Int64, pCodFuncionario, ParameterDirection.Input));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();

                // Tratando retorno da função
                var mensErro = ((OracleClob)(cmd.Parameters["nRETURN"].Value)).Value;
                if (mensErro != "**OK**")
                    throw new Exception(mensErro);
            }

            return retorno;
        }
        #endregion

        #region Package Produção Procedure GETVALIDAOPAPTOSLDDTLANC
        /// <summary>
        /// Valida o apontamento de produção
        /// </summary>
        /// <param name="msErro">Mensagem de erro</param>
        /// <returns>bool</returns>
        public async Task<bool> GetValidaOpAptoSldDtlancAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, DateTime pDtLanc, decimal pQuantidade)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.GETVALIDAOPAPTOSLDDTLANC";
                cmd.CommandType = CommandType.StoredProcedure;

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("nRETURN", OracleDbType.Clob, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODESTRUTURA", OracleDbType.Int64, pCodEstrutura, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQUANTIDADE", OracleDbType.Decimal, pQuantidade, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pDTLANC", OracleDbType.Date, pDtLanc, ParameterDirection.Input));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();

                // Tratando retorno da função
                var mensErro = ((OracleClob)(cmd.Parameters["nRETURN"].Value)).Value;
                if (mensErro != "**OK**")
                    throw new Exception(mensErro);
            }

            return retorno;
        }
        #endregion

        #region Package Produção Procedure APONTLOTESFIFO
        /// <summary>
        /// Gera os apontamentos dos lotes via fifo
        /// </summary>
        /// <param name="pTransaction">Transação</param>
        /// <param name="pLancamento">Instancia do lancamento</param>
        /// <param name="pCodUsuario">Cód. Usuário</param>
        /// <param name="pMovimentaEstoque">Movimenta estoque</param>
        /// <returns>bool</returns>
        public async Task<bool> ApontLotesFifoAsync(PDLancamentos pLancamento, string pCodUsuario, bool pMovimentaEstoque)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.APONTLOTESFIFO";
                cmd.CommandType = CommandType.StoredProcedure;

                if (DbConn.Database.CurrentTransaction != null)
                    cmd.Transaction = DbConn.Database.CurrentTransaction.GetDbTransaction();

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pLancamento.codestab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODLANCAMENTO", OracleDbType.Int64, pLancamento.codlancamento, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Int64, pLancamento.codordprod, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pLancamento.nivelordprod, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODESTRUTURA", OracleDbType.Int64, pLancamento.codestrutura, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQTDAPONTAMENTO", OracleDbType.Decimal, pLancamento.quantidade, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pMOVIMENTAESTOQ", OracleDbType.Int32, pMovimentaEstoque, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();
                if (((OracleDecimal)(cmd.Parameters["nRETURN"].Value)).Value == 1)
                    return false;

                // Tratando retorno da função
                var mensErro = ((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value;
                mensErro = mensErro == "**OK**" ? "" : mensErro;

                if (!mensErro.IsEmpty())
                    throw new Exception(mensErro);
            }

            return retorno;
        }
        #endregion

        #region Package Produção Procedure APTOPRODUCAO
        /// <summary>
        /// Realiza o apontamento de produção
        /// </summary>
        /// <param name="pTransaction">Transação</param>
        /// <param name="pLancamento">Instância do lançamento</param>
        /// <param name="pCodUsuario">Cód. Usuário</param>
        /// <param name="pQtdrefugo">Qtd. Refugo</param>
        /// <returns>bool</returns>
        public async Task<bool> AptoProducaoAsync(PDLancamentos pLancamento, string pCodUsuario, decimal pQtdrefugo)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.APTOPRODUCAO";
                cmd.CommandType = CommandType.StoredProcedure;

                if (DbConn.Database.CurrentTransaction != null)
                    cmd.Transaction = DbConn.Database.CurrentTransaction.GetDbTransaction();

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pLancamento.codestab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODLANCAMENTO", OracleDbType.Int64, pLancamento.codlancamento, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQTDREFUGO", OracleDbType.Decimal, pQtdrefugo, ParameterDirection.Input));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();
            }

            return retorno;
        }
        #endregion

        #region Package Produção Procedure LANCCONSUMOPROCESSO
        /// <summary>
        /// Realiza o apontamento de produção
        /// </summary>
        /// <param name="pTransaction">Transação</param>
        /// <param name="pLancamento">Instância do lançamento</param>
        /// <param name="pQtdRefugo">Qtd. Refugo</param>
        /// <param name="pCodUsuario">Cód. Usuário</param>
        /// <returns>bool</returns>
        public async Task<bool> LancConsumoProcessoAsync(PDLancamentos pLancamento, decimal pQtdRefugo, string pCodUsuario)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.LANCCONSUMOPROCESSO";
                cmd.CommandType = CommandType.StoredProcedure;

                if (DbConn.Database.CurrentTransaction != null)
                    cmd.Transaction = DbConn.Database.CurrentTransaction.GetDbTransaction();

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pLancamento.codestab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Int64, pLancamento.codordprod, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pLancamento.nivelordprod, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODLANCAMENTO", OracleDbType.Int64, pLancamento.codlancamento, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQTDREFUGO", OracleDbType.Decimal, pQtdRefugo, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pDATACONSUMO", OracleDbType.Date, pLancamento.dttermino, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pOPERACAO", OracleDbType.Int32, pLancamento.operacao, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();
                if (((OracleDecimal)(cmd.Parameters["nRETURN"].Value)).Value == 1)
                    return false;

                // Tratando retorno da função
                var mensErro = ((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value;
                mensErro = mensErro == "*OK*" ? "" : mensErro;

                if (!mensErro.IsEmpty())
                    throw new Exception(mensErro);
            }

            return retorno;
        }
        #endregion

        #region Package Produção Procedure MOVREFUGOOPERACAO
        /// <summary>
        /// Movimenta os refugos nas operações
        /// </summary>
        /// <param name="pTransaction">Transação</param>
        /// <param name="pCodEstab">Cód. Estab.</param>
        /// <param name="pCodLancamento">Cód. Lançamento</param>
        /// <param name="pCodRefugo">Cód. Refugo</param>
        /// <param name="pQtdRefugo">Qtd. Refugo</param>
        /// <param name="pCodUsuario">Cód. Usuário</param>
        /// <returns>bool</returns>
        public async Task<bool> MovRefugoOperacaoAsync(int pCodEstab, long pCodLancamento, long pCodRefugo, decimal pQtdRefugo, string pCodUsuario)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.MOVREFUGOOPERACAO";
                cmd.CommandType = CommandType.StoredProcedure;

                if (DbConn.Database.CurrentTransaction != null)
                    cmd.Transaction = DbConn.Database.CurrentTransaction.GetDbTransaction();

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODLANCAMENTO", OracleDbType.Int64, pCodLancamento, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODREFUGO", OracleDbType.Int64, pCodRefugo, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQUANTIDADEREF", OracleDbType.Decimal, pQtdRefugo, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();
                if (((OracleDecimal)(cmd.Parameters["nRETURN"].Value)).Value == 1)
                    return false;

                // Tratando retorno da função
                var mensErro = ((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value;
                mensErro = mensErro == "*OK*" ? "" : mensErro;

                if (!mensErro.IsEmpty())
                    throw new Exception(mensErro);
            }

            return retorno;
        }
        #endregion

        #region Package Produção Procedure RECALCULATOTAISORDEM
        /// <summary>
        /// Recalcula totais da O.P.
        /// </summary>
        /// <param name="pTransaction">Transação</param>
        /// <param name="pCodEstab">Cód. Estab.</param>
        /// <param name="pCodOrdProd">Cód. Ord. Prod.</param>
        /// <param name="pNivelOrdProd">Nivel Ord. Prod.</param>
        /// <returns>bool</returns>
        public async Task<bool> RecalculaTotaisOrdemAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_PRODUCAO.RECALCULATOTAISORDEM";
                cmd.CommandType = CommandType.StoredProcedure;

                if (DbConn.Database.CurrentTransaction != null)
                    cmd.Transaction = DbConn.Database.CurrentTransaction.GetDbTransaction();

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Int64, pCodOrdProd, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();
            }

            return retorno;
        }
        #endregion
    }
}
