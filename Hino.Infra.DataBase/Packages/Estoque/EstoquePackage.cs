﻿using Hino.Domain.Estoque.Interfaces.Packages;
using Hino.Infra.Cross.Entities.Estoque;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Infra.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Infra.DataBase.Packages.Estoque
{
    public class EstoquePackage : BaseDBPackage, IEstoquePackage
    {
        public EstoquePackage(AppDbContext appDbContext)
             : base(appDbContext)
        {
        }

        public async Task<DateTime> BuscaDataFechamentoAsync(int pCodEstab)
        {
            var retorno = new DateTime(1900, 01, 01);
            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = @"SELECT PCKG_ESTOQUE.GETDATAFEC(:pCODESTAB) 
                                     FROM DUAL";
                cmd.CommandType = CommandType.Text;

                DbConn.Database.OpenConnection();

                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));

                try
                {
                    retorno = Convert.ToDateTime(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {

                }
            }
            return retorno;
        }

        #region Package Estoque Procedure GERAKARDEX
        /// <summary>
        /// Gera as movimentações de estoque e o registro no kardex
        /// </summary>
        /// <param name="pTransaction">Transação</param>
        /// <param name="pKardex">Classe Kardex</param>
        /// <returns>bool</returns>
        public async Task<bool> GeraKardexAsync(ESKardex pKardex)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_ESTOQUE.GERAKARDEX";
                cmd.CommandType = CommandType.StoredProcedure;

                if (DbConn.Database.CurrentTransaction != null)
                    cmd.Transaction = DbConn.Database.CurrentTransaction.GetDbTransaction();

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pKardex.codestab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pINDICEITEM", OracleDbType.Decimal, (pKardex.indiceinftem == null) ? 0 : pKardex.indiceinftem, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pSEQITEMNF", OracleDbType.Int16, (pKardex.seqnfitem == null) ? 0 : pKardex.seqnfitem, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODLANCOP", OracleDbType.Decimal, (pKardex.codlancamento == null) ? 0 : pKardex.codlancamento, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pDATAKARDEX", OracleDbType.Date, pKardex.data, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODTIPMOV", OracleDbType.Varchar2, pKardex.codtipomov, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, pKardex.codproduto, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODUNIDADE", OracleDbType.Varchar2, pKardex.codunidade, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODESTOQUE", OracleDbType.Varchar2, pKardex.codestoque, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQUANTIDADE", OracleDbType.Decimal, pKardex.quantidade, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pVALORTOTAL", OracleDbType.Decimal, pKardex.valortotal, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODTRANSF", OracleDbType.Decimal, (pKardex.codajuste == null) ? 0 : pKardex.codajuste, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODLOTE", OracleDbType.Decimal, (pKardex.lote == null) ? 0 : pKardex.lote, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODCONSUMO", OracleDbType.Decimal, (pKardex.codconsumo == null) ? 0 : pKardex.codconsumo, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODREFUGO", OracleDbType.Decimal, (pKardex.codrefugo == null) ? 0 : pKardex.codrefugo, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODREQUISICAO", OracleDbType.Decimal, (pKardex.codrequisicao == null) ? 0 : pKardex.codrequisicao, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODROMANEIOSEP", OracleDbType.Decimal, (pKardex.codromaneiosep == null) ? 0 : pKardex.codromaneiosep, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODSUCATA", OracleDbType.Decimal, (pKardex.codsucata == null) ? 0 : pKardex.codsucata, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODINSPRECEBE", OracleDbType.Int64, (pKardex.codinsprecebe == null) ? 0 : pKardex.codinsprecebe, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pUSUARIO", OracleDbType.Varchar2, pKardex.codusuario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODTRANSFEST", OracleDbType.Decimal, (pKardex.codtransfest == null) ? 0 : pKardex.codtransfest, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODSOLIC", OracleDbType.Decimal, (pKardex.codsolic == null) ? 0 : pKardex.codsolic, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODINV", OracleDbType.Decimal, (pKardex.codinv == null) ? 0 : pKardex.codinv, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODSOBRA", OracleDbType.Decimal, (pKardex.codsobra == null) ? 0 : pKardex.codsobra, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODROMANEIOSEPDET", OracleDbType.Decimal, (pKardex.codromaneiosepdet == null) ? 0 : pKardex.codromaneiosepdet, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODRESERVA", OracleDbType.Decimal, pKardex.codreserva ?? 0, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODEMPENHO", OracleDbType.Decimal, pKardex.codempenho ?? 0, ParameterDirection.Input));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();
            }

            return retorno;
        }
        #endregion

        #region  Movimenta o saldo do lote
        /// <summary>
        /// Movimenta o saldo do lote
        /// </summary>
        /// <param name="pTransaction">Transação</param>
        /// <param name="pLoteSaldo">Classe lote saldo</param>
        /// <param name="pQuantidade">Quantidade movimentada</param>
        /// <param name="pMovimento">Tipo do movimento 'S': Saida 'E': Entrada</param>
        /// <returns>bool</returns>
        public async Task<bool> MovimentaSaldoLoteAsync(ESLoteSaldo pLoteSaldo, decimal pQuantidade, string pMovimento)
        {
            bool retorno = true;

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "PCKG_ESTOQUE.MOVIMENTALOTE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (DbConn.Database.CurrentTransaction != null)
                    cmd.Transaction = DbConn.Database.CurrentTransaction.GetDbTransaction();

                DbConn.Database.OpenConnection();

                // Parâmetros
                cmd.Parameters.Add(new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pLoteSaldo.codestab, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODLOTE", OracleDbType.Int64, pLoteSaldo.lote, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODESTOQUE", OracleDbType.Varchar2, pLoteSaldo.codestoque, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pQUANTIDADE", OracleDbType.Decimal, pQuantidade, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pMOVIMENTO", OracleDbType.Varchar2, pMovimento, ParameterDirection.Input));

                // Executando o comando
                await cmd.ExecuteNonQueryAsync();
                if (((OracleDecimal)(cmd.Parameters["nRETURN"].Value)).Value != 0)
                    throw new Exception(Infra.Cross.Resources.ErrorMessagesResource.LoteCantBeMovimented);
            }

            return retorno;
        }
        #endregion
    }
}
