using Hino.Domain.Estoque.Interfaces.Repositories.Lote;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Estoque.Lote
{
    public class ESLoteSaldoRepository : BaseRepository<ESLoteSaldo>, IESLoteSaldoRepository
    {
        public ESLoteSaldoRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
