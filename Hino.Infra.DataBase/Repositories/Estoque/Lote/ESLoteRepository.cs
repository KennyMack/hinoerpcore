using Hino.Domain.Estoque.Interfaces.Repositories.Lote;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Estoque.Lote
{
    public class ESLoteRepository : BaseRepository<ESLote>, IESLoteRepository
    {
        public ESLoteRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
