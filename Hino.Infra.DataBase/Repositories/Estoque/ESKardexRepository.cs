using Hino.Infra.Cross.Entities.Estoque;
using Hino.Infra.DataBase.Context;
using Hino.Domain.Estoque.Interfaces.Repositories;

namespace Hino.Infra.DataBase.Repositories.Estoque
{
    public class ESKardexRepository : BaseRepository<ESKardex>, IESKardexRepository
    {
        public ESKardexRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
