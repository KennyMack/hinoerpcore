﻿using Hino.Domain.Base.Interfaces.Repositories;
using Hino.Infra.Cross.Entities;
using Hino.Infra.Cross.Utils.Attributes;
using Hino.Infra.Cross.Utils.Extensions;
using Hino.Infra.Cross.Utils.Paging;
using Hino.Infra.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Infra.DataBase.Repositories
{
    public class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : BaseEntity
    {
        protected AppDbContext DbConn;
        protected DbSet<T> DbEntity;

        public BaseRepository(AppDbContext appDbContext)
        {
            DbConn = appDbContext;
            DbEntity = DbConn.Set<T>();
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync() =>
            await DbConn.Database.BeginTransactionAsync();

        protected IQueryable<T> AddQueryProperties(IQueryable<T> query, params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.AsNoTracking();
        }

        protected async Task<PagedResult<T>> PaginateQueryAsync(IQueryable<T> query, int page, int pageSize)
        {
            var PropertyKey = typeof(T).GetProperties()
                .Where(r => r.GetCustomAttributes(typeof(KeyAttribute), false).Length > 0)
                .FirstOrDefault();
            var PropertyPaginate = typeof(T).GetProperties()
                .Where(r => r.GetCustomAttributes(typeof(PaginateByAttribute), false).Length > 0)
                .FirstOrDefault();
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = 1,
                PageCount = 1
            };

            if (page > -1 && pageSize > 0)
            {
                result.RowCount = query.Count();
                result.PageCount = (int)Math.Ceiling((double)result.RowCount / pageSize);

                var skip = (page - 1) * pageSize;

                var queryOrder = query;

                if (PropertyPaginate != null)
                {
                    var Descending = ((PaginateByAttribute)PropertyPaginate.GetCustomAttributes(typeof(PaginateByAttribute), false).First()).Descending;

                    queryOrder = Descending ? 
                        LinqSortEx.OrderByDescending(query, PropertyPaginate.Name) :
                        LinqSortEx.OrderBy(query, PropertyPaginate.Name);
                }
                else if (PropertyKey != null)
                    queryOrder = LinqSortEx.OrderByDescending(query, PropertyKey.Name);
                else
                    queryOrder = LinqSortEx.OrderByDescending(query, typeof(T).GetProperties().First().Name);

                result.Results = await queryOrder.Skip(skip).Take(pageSize).ToListAsync();
            }
            else
            {
                result.Results = await query.ToListAsync();
                result.RowCount = result.Results.Count();
                result.CurrentPage = 1;
                result.PageSize = result.RowCount;
            }

            return result;
        }

        public async virtual Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties).ToListAsync();

        public async virtual Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
            await PaginateQueryAsync(AddQueryProperties(DbEntity, includeProperties), page, pageSize);

        public virtual T GetFirstByWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
             AddQueryProperties(DbEntity, includeProperties)
                .AsNoTracking()
                .Where(predicate)
                .FirstOrDefault();

        public virtual T GetByWhereForUpdate(Expression<Func<T, bool>> predicate) =>
            DbEntity.Where(predicate)
            .FirstOrDefault();

        long GetNextSequence()
        {
            long NextVal = -1;
            var SeqAttribute = typeof(T).GetCustomAttributes(typeof(SequenceAttribute), false);
            var SequenceName = $"SEQ_{typeof(T).Name.ToUpper()}";
            if (SeqAttribute.Length > 0)
                SequenceName = ((SequenceAttribute)SeqAttribute[0]).SequenceName;

            var sql = $"SELECT {SequenceName}.NEXTVAL FROM DUAL";

            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;
                DbConn.Database.OpenConnection();
                NextVal = Convert.ToInt64(cmd.ExecuteScalar());
            }
            return NextVal;
        }

        public virtual long NextSequence() =>
            GetNextSequence();

        public async virtual Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, 
            params Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties)
                            .AsNoTracking()
                            .Where(predicate)
                            .ToListAsync();

        public async virtual Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, 
            params Expression<Func<T, object>>[] includeProperties) =>
            await PaginateQueryAsync(
                AddQueryProperties(DbEntity, includeProperties)
                    .AsNoTracking()
                    .Where(predicate), page, pageSize);

        public virtual T RemoveByWhere(Expression<Func<T, bool>> predicate)
        {
            var model = GetByWhereForUpdate(predicate);

            if (model != null)
                Remove(model);

            return model;
        }

        public async virtual Task<int> SaveChanges()
        {
            var ret = -1;
            try
            {
                ret = await DbConn.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                // Logging.Exception(e);
                /* TODO: REMOVER NA VERSÃO FINAL
                 * if (e.InnerException.InnerException is OracleException)
                {
                    var sqlex = e.InnerException.InnerException as OracleException;
                    throw new DBOracleCodeException(sqlex);
                }*/
                throw;
            }
            catch (Exception e)
            {
                // Logging.Exception(e);
                throw;
            }

            return ret;
        }

        void SetNextId(T model)
        {
            var PropertiesKey = model.GetType().GetProperties().Where(r => r.GetCustomAttributes(typeof(PrimaryKeyAttribute), false).Length > 0);

            if (PropertiesKey.Count() > 0)
            {
                foreach (var item in PropertiesKey)
                {
                    var Attributes = item.GetCustomAttributes(typeof(PrimaryKeyAttribute), false);

                    if (Attributes.Length > 0 &&
                        ((PrimaryKeyAttribute)Attributes[0]).HasSequence &&
                        long.TryParse(item.GetValue(model).ToString(), out long Value) &&
                        Value == 0)
                    {
                        item.SetValue(model, Convert.ChangeType(NextSequence(), Type.GetType(item.PropertyType.FullName)));

                    }
                }
            }
        }

        public virtual T Add(T model)
        {
            SetNextId(model);

            DbEntity.Add(model);
            return model;
        }

        public virtual T Update(T model)
        {
            DbConn.Entry(model).State = EntityState.Detached;
            DbConn.Entry(model).State = EntityState.Modified;

            return model;
        }

        public virtual T Remove(T model)
        {
            // DbEntity.Attach(model);
            // DbConn.Entry(model).State = EntityState.Detached;
            DbConn.Entry(model).State = EntityState.Deleted;
            DbEntity.Remove(model);

            return model;
        }

        public void Dispose()
        {
            DbConn.Dispose();
        }

    }
}
