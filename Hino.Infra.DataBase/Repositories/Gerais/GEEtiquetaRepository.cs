using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Gerais
{
    public class GEEtiquetaRepository : BaseRepository<GEEtiqueta>, IGEEtiquetaRepository
    {
        public GEEtiquetaRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
