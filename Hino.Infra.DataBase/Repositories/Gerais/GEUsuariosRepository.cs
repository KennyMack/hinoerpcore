using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Gerais
{
    public class GEUsuariosRepository : BaseRepository<GEUsuarios>, IGEUsuariosRepository
    {
        public GEUsuariosRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
