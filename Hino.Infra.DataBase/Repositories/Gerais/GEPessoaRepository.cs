using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Gerais
{
    public class GEPessoaRepository : BaseRepository<GEPessoa>, IGEPessoaRepository
    {
        public GEPessoaRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
