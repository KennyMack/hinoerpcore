using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Gerais
{
    public class GEFuncionariosRepository : BaseRepository<GEFuncionarios>, IGEFuncionariosRepository
    {
        public GEFuncionariosRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public GEFuncionarios GetByIdentficador(int pCodEstab, string pIdentificacao) =>
            this.GetFirstByWhere(r => r.codestab == pCodEstab &&
                r.identificacao == pIdentificacao &&
                r.status,
                s => s.GEPessoa
            );

    }
}
