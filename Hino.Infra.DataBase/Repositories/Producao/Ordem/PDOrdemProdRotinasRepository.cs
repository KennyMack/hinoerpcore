using Hino.Domain.Producao.Interfaces.Repositories.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.DataBase.Context;
using System.Threading.Tasks;

namespace Hino.Infra.DataBase.Repositories.Producao.Ordem
{
    public class PDOrdemProdRotinasRepository : BaseRepository<PDOrdemProdRotinas>, IPDOrdemProdRotinasRepository
    {
        public PDOrdemProdRotinasRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
