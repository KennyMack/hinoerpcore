using Hino.Domain.Producao.Interfaces.Repositories.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Producao.Ordem
{
    public class PDOrdemProdRepository : BaseRepository<PDOrdemProd>, IPDOrdemProdRepository
    {
        public PDOrdemProdRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public PDOrdemProd GetByBarras(long pSeqBarras) =>
            GetFirstByWhere(r => r.seqbarras == pSeqBarras);
    }
}
