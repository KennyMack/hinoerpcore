using Hino.Domain.Producao.Interfaces.Repositories.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Infra.DataBase.Repositories.Producao.Ordem
{
    public class PDOrdemProdCompRepository : BaseRepository<PDOrdemProdComp>, IPDOrdemProdCompRepository
    {
        public PDOrdemProdCompRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<DataTable> SelectSaldoEstoqueNec(int pCodEstab, decimal pQtdLanc, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura)
        {
            DataTable dt = new DataTable();
            using var cmd = DbConn.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = @"SELECT CODCOMPONENTE, FSPRODUTODESCRICAO, CODESTOQUE, QUANTIDADE, QTDCONSUMIDA, SLDEST, 
                                  PROGRAMADOOP, ROUND((:pQTDLANC * (QUANTIDADE / PROGRAMADOOP)), 4) SLDNEC,
                                  CODUNIDADE,  CODESTRUTURA, CODROTEIRO, OPERACAO, 
                                  ROUND((QUANTIDADE / PROGRAMADOOP), 4) FATOR,
                                  CASE 
                                      WHEN ROUND((:pQTDLANC * (QUANTIDADE / PROGRAMADOOP)), 4) > SLDEST  THEN 'SALDO INSUFICIENTE'
                                      ELSE 'SALDO SUFICIENTE'
                                  END STATUSSALDO
                             FROM (
                                   SELECT PDORDEMPRODCOMP.CODCOMPONENTE, PDORDEMPRODCOMP.CODESTOQUE, 
                                          PDORDEMPRODCOMP.QUANTIDADE, PDORDEMPRODCOMP.QTDCONSUMIDA,
                                          PCKG_PRODUCAO.GETSLDESTCOMPOP(PDORDEMPRODCOMP.CODESTAB, PDORDEMPRODCOMP.CODCOMPONENTE, PDORDEMPRODCOMP.CODESTOQUE, PDORDEMPRODCOMP.CODORDPROD) SLDEST,
                                          PCKG_PRODUCAO.GETPROGRAMADOOP(PDORDEMPRODCOMP.CODESTAB, PDORDEMPRODCOMP.CODORDPROD, PDORDEMPRODCOMP.NIVELORDPROD, :pCODESTRUTURA) PROGRAMADOOP,
                                          FSPRODUTOPARAMESTAB.CODUNIDADE, FSPRODUTO.DESCRICAO FSPRODUTODESCRICAO, PDORDEMPRODCOMP.CODESTRUTURA,
                                          PDORDEMPRODCOMP.CODROTEIRO, PDORDEMPRODCOMP.OPERACAO
                                     FROM PDORDEMPRODCOMP,
                                          FSPRODUTOPARAMESTAB,
                                          FSPRODUTOPCP,
                                          FSPRODUTO
                                    WHERE PDORDEMPRODCOMP.CODESTAB      = FSPRODUTOPARAMESTAB.CODESTAB
                                      AND PDORDEMPRODCOMP.CODCOMPONENTE = FSPRODUTOPARAMESTAB.CODPRODUTO
                                      AND PDORDEMPRODCOMP.CODESTAB      = FSPRODUTOPCP.CODESTAB
                                      AND PDORDEMPRODCOMP.CODCOMPONENTE = FSPRODUTOPCP.CODPRODUTO
                                      AND FSPRODUTO.CODPRODUTO          = FSPRODUTOPCP.CODPRODUTO
                                      AND PDORDEMPRODCOMP.CODORDPROD    = :pCODORDPROD
                                      AND PDORDEMPRODCOMP.CODESTAB      = :pCODESTAB
                                      AND PDORDEMPRODCOMP.NIVELORDPROD  = :pNIVELORDPROD
                                      AND PDORDEMPRODCOMP.CODESTRUTURA  = :pCODESTRUTURA
                                   )";
            cmd.CommandType = CommandType.Text;

            DbConn.Database.OpenConnection();

            // Parāmetros
            cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
            cmd.Parameters.Add(new OracleParameter("pQTDLANC", OracleDbType.Decimal, pQtdLanc, ParameterDirection.Input));
            cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Int64, pCodOrdProd, ParameterDirection.Input));
            cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input));
            cmd.Parameters.Add(new OracleParameter("pCODESTRUTURA", OracleDbType.Int64, pCodEstrutura, ParameterDirection.Input));

            try
            {
                dt.Load(await cmd.ExecuteReaderAsync());
            }
            catch (Exception)
            {
            }

            return dt;
        }
    }
}
