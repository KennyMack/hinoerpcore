using Hino.Domain.Producao.Interfaces.Repositories.Refugos;
using Hino.Infra.Cross.Entities.Producao.Refugos;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Producao.Refugos
{
    public class PDRefugoReapRepository : BaseRepository<PDRefugoReap>, IPDRefugoReapRepository
    {
        public PDRefugoReapRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
