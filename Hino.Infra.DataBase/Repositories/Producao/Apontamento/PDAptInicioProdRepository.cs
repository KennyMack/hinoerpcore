using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using System;

namespace Hino.Infra.DataBase.Repositories.Producao.Apontamento
{
    public class PDAptInicioProdRepository : BaseRepository<PDAptInicioProd>, IPDAptInicioProdRepository
    {
        private AppDbContext _AppDbContext;
        public PDAptInicioProdRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _AppDbContext = appDbContext;
        }

        async Task<long> GetLastIdAsync(PDAptInicioProd pAptInicio)
        {
            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = @"SELECT NVL(MAX(PDAPTINICIOPROD.CODINIAPT), -1)
                                FROM PDAPTINICIOPROD
                               WHERE PDAPTINICIOPROD.CODESTRUTURA = :pCODESTRUTURA
                                 AND PDAPTINICIOPROD.CODFUNCIONARIO = :pCODFUNCIONARIO
                                 AND PDAPTINICIOPROD.CODORDPROD = :pCODORDPROD
                                 AND PDAPTINICIOPROD.CODROTEIRO = :pCODROTEIRO
                                 AND PDAPTINICIOPROD.NIVELORDPROD = :pNIVELORDPROD
                                 AND PDAPTINICIOPROD.CODUSUARIO = :pCODUSUARIO
                                 AND PDAPTINICIOPROD.OPERACAO = :pOPERACAO
                                 AND PDAPTINICIOPROD.TIPO = :pTIPO
                                 AND PDAPTINICIOPROD.CODESTAB = :pCODESTAB
                                 AND NOT EXISTS (SELECT 1
                                                   FROM PDAPTTERMINOPROD
                                                  WHERE PDAPTTERMINOPROD.CODINIAPT = PDAPTINICIOPROD.CODINIAPT
                                                    AND PDAPTTERMINOPROD.CODESTAB  = PDAPTINICIOPROD.CODESTAB)";
                cmd.CommandType = CommandType.Text;

                DbConn.Database.OpenConnection();

                // Parāmetros
                cmd.Parameters.Add(new OracleParameter("pCODESTRUTURA", OracleDbType.Int64, pAptInicio.codestrutura, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODFUNCIONARIO", OracleDbType.Int64, pAptInicio.codfuncionario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Int64, pAptInicio.codordprod, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODROTEIRO", OracleDbType.Int16, pAptInicio.codroteiro, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pAptInicio.nivelordprod, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pAptInicio.codusuario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pOPERACAO", OracleDbType.Int16, pAptInicio.operacao, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pTIPO", OracleDbType.Int16, pAptInicio.tipo, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pAptInicio.codestab, ParameterDirection.Input));

                try
                {
                    return Convert.ToInt64(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {
                    return -1;
                }
            }
        }

        public async Task<PDAptInicioProd> GetLastAptAsync(PDAptInicioProd pAptInicio)
        {
            var Id = await GetLastIdAsync(pAptInicio);
            if (Id <= 0)
                return null;

            return GetFirstByWhere(r => r.codestab == pAptInicio.codestab && r.codiniapt == Id);
        }

        public async Task<bool> WasStartedAsync(PDAptInicioProd pAptInicio)
        {
            using (var cmd = DbConn.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = @"SELECT COUNT(*) TOTREG
                                      FROM PDAPTINICIOPROD
                                     WHERE PDAPTINICIOPROD.CODESTRUTURA   = :pCODESTRUTURA
                                       AND PDAPTINICIOPROD.CODFUNCIONARIO = :pCODFUNCIONARIO
                                       AND PDAPTINICIOPROD.CODORDPROD     = :pCODORDPROD
                                       AND PDAPTINICIOPROD.CODROTEIRO     = :pCODROTEIRO
                                       AND PDAPTINICIOPROD.NIVELORDPROD   = :pNIVELORDPROD
                                       AND PDAPTINICIOPROD.CODUSUARIO     = :pCODUSUARIO
                                       AND PDAPTINICIOPROD.OPERACAO       = :pOPERACAO
                                       AND PDAPTINICIOPROD.TIPO           = :pTIPO
                                       AND PDAPTINICIOPROD.CODESTAB       = :pCODESTAB
                                       AND NOT EXISTS(SELECT 1
                                                         FROM PDAPTTERMINOPROD
                                                        WHERE PDAPTTERMINOPROD.CODINIAPT = PDAPTINICIOPROD.CODINIAPT
                                                          AND PDAPTTERMINOPROD.CODESTAB  = PDAPTINICIOPROD.CODESTAB)";
                cmd.CommandType = CommandType.Text;

                DbConn.Database.OpenConnection();

                cmd.Parameters.Add(new OracleParameter("pCODESTRUTURA", OracleDbType.Int64, pAptInicio.codestrutura, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODFUNCIONARIO", OracleDbType.Int64, pAptInicio.codfuncionario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODORDPROD", OracleDbType.Int64, pAptInicio.codordprod, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODROTEIRO", OracleDbType.Int16, pAptInicio.codroteiro, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pAptInicio.nivelordprod, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pAptInicio.codusuario, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pOPERACAO", OracleDbType.Int16, pAptInicio.operacao, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pTIPO", OracleDbType.Int16, pAptInicio.tipo, ParameterDirection.Input));
                cmd.Parameters.Add(new OracleParameter("pCODESTAB", OracleDbType.Int32, pAptInicio.codestab, ParameterDirection.Input));

                return (Convert.ToInt32(await cmd.ExecuteScalarAsync()) > 0);

            }
        }
    }
}
