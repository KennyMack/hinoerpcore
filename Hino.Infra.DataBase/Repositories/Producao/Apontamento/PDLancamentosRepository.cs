using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Producao.Apontamento
{
    public class PDLancamentosRepository : BaseRepository<PDLancamentos>, IPDLancamentosRepository
    {
        public PDLancamentosRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
