using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.DataBase.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Infra.DataBase.Repositories.Producao.Apontamento
{
    public class PDAptTerminoProdRepository : BaseRepository<PDAptTerminoProd>, IPDAptTerminoProdRepository
    {
        private AppDbContext _AppDbContext;
        public PDAptTerminoProdRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _AppDbContext = appDbContext;
        }

        public async Task<bool> WasFinishedAsync(PDAptInicioProd pAptInicio) =>
            (await QueryAsync(r => r.codestab == pAptInicio.codestab &&
            r.codiniapt == pAptInicio.codiniapt)).Any();
    }
}
