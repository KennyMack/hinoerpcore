using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Producao.Apontamento
{
    public class PDAptRetrabalhoRepository : BaseRepository<PDAptRetrabalho>, IPDAptRetrabalhoRepository
    {
        public PDAptRetrabalhoRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
