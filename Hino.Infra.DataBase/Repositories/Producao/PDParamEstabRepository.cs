using Hino.Domain.Producao.Interfaces.Repositories;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Producao
{
    public class PDParamEstabRepository : BaseRepository<PDParamEstab>, IPDParamEstabRepository
    {
        public PDParamEstabRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
