using Hino.Domain.Fiscal.Interfaces.Repositories.Produto;
using Hino.Infra.Cross.Entities.Fiscal.Produto;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Fiscal.Produto
{
    public class FSProdutoPCPRepository : BaseRepository<FSProdutoPCP>, IFSProdutoPCPRepository
    {
        public FSProdutoPCPRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
