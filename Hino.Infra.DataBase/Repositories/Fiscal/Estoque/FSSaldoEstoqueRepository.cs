using Hino.Domain.Fiscal.Interfaces.Repositories.Estoque;
using Hino.Infra.Cross.Entities.Fiscal.Estoque;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Fiscal.Estoque
{
    public class FSSaldoEstoqueRepository : BaseRepository<FSSaldoEstoque>, IFSSaldoEstoqueRepository
    {
        public FSSaldoEstoqueRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
