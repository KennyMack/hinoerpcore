using Hino.Domain.Engenharia.Interfaces.Repositories.Estrutura;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Engenharia.Estrutura
{
    public class ENEstRotinasRepository : BaseRepository<ENEstRotinas>, IENEstRotinasRepository
    {
        public ENEstRotinasRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
