using Hino.Domain.Engenharia.Interfaces.Repositories.Estrutura;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Infra.DataBase.Context;

namespace Hino.Infra.DataBase.Repositories.Engenharia.Estrutura
{
    public class ENEstComponentesRepository : BaseRepository<ENEstComponentes>, IENEstComponentesRepository
    {
        public ENEstComponentesRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
