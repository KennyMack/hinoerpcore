using Hino.Infra.Cross.Entities.Fiscal.Produto;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Fiscal.Interfaces.Repositories.Produto
{
    public interface IFSProdutoParamEstabRepository : IBaseRepository<FSProdutoParamEstab>
    {
    }
}
