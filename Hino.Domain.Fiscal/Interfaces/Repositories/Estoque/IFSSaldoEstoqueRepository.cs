using Hino.Infra.Cross.Entities.Fiscal.Estoque;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Fiscal.Interfaces.Repositories.Estoque
{
    public interface IFSSaldoEstoqueRepository : IBaseRepository<FSSaldoEstoque>
    {
    }
}
