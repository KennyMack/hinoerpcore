using Hino.Infra.Cross.Entities.Fiscal.Estoque;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Fiscal.Interfaces.Services.Estoque
{
    public interface IFSSaldoEstoqueService : IBaseService<FSSaldoEstoque>
    {
    }
}
