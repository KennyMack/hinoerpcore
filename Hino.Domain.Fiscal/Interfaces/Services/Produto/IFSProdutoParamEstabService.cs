using Hino.Infra.Cross.Entities.Fiscal.Produto;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Fiscal.Interfaces.Services.Produto
{
    public interface IFSProdutoParamEstabService : IBaseService<FSProdutoParamEstab>
    {
    }
}
