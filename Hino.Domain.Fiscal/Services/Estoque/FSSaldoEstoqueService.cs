using Hino.Domain.Fiscal.Interfaces.Repositories.Estoque;
using Hino.Domain.Fiscal.Interfaces.Services.Estoque;
using Hino.Infra.Cross.Entities.Fiscal.Estoque;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Fiscal.Services.Estoque
{
    public class FSSaldoEstoqueService : BaseService<FSSaldoEstoque>, IFSSaldoEstoqueService
    {
        private readonly IFSSaldoEstoqueRepository _IFSSaldoEstoqueRepository;

        public FSSaldoEstoqueService(IFSSaldoEstoqueRepository pIFSSaldoEstoqueRepository) : 
             base(pIFSSaldoEstoqueRepository)
        {
            _IFSSaldoEstoqueRepository = pIFSSaldoEstoqueRepository;
        }
    }
}
