using Hino.Domain.Fiscal.Interfaces.Repositories.Produto;
using Hino.Domain.Fiscal.Interfaces.Services.Produto;
using Hino.Infra.Cross.Entities.Fiscal.Produto;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Fiscal.Services.Produto
{
    public class FSProdutoParamEstabService : BaseService<FSProdutoParamEstab>, IFSProdutoParamEstabService
    {
        private readonly IFSProdutoParamEstabRepository _IFSProdutoParamEstabRepository;

        public FSProdutoParamEstabService(IFSProdutoParamEstabRepository pIFSProdutoParamEstabRepository) : 
             base(pIFSProdutoParamEstabRepository)
        {
            _IFSProdutoParamEstabRepository = pIFSProdutoParamEstabRepository;
        }
    }
}
