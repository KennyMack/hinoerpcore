using Hino.Domain.Fiscal.Interfaces.Repositories.Produto;
using Hino.Domain.Fiscal.Interfaces.Services.Produto;
using Hino.Infra.Cross.Entities.Fiscal.Produto;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Fiscal.Services.Produto
{
    public class FSProdutoPCPService : BaseService<FSProdutoPCP>, IFSProdutoPCPService
    {
        private readonly IFSProdutoPCPRepository _IFSProdutoPCPRepository;

        public FSProdutoPCPService(IFSProdutoPCPRepository pIFSProdutoPCPRepository) : 
             base(pIFSProdutoPCPRepository)
        {
            _IFSProdutoPCPRepository = pIFSProdutoPCPRepository;
        }
    }
}
