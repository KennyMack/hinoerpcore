using Hino.Domain.Fiscal.Interfaces.Repositories.Produto;
using Hino.Domain.Fiscal.Interfaces.Services.Produto;
using Hino.Infra.Cross.Entities.Fiscal.Produto;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Fiscal.Services.Produto
{
    public class FSProdutoService : BaseService<FSProduto>, IFSProdutoService
    {
        private readonly IFSProdutoRepository _IFSProdutoRepository;

        public FSProdutoService(IFSProdutoRepository pIFSProdutoRepository) : 
             base(pIFSProdutoRepository)
        {
            _IFSProdutoRepository = pIFSProdutoRepository;
        }
    }
}
