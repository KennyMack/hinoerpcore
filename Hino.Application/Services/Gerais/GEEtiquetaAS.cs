﻿using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;

namespace Hino.Application.Interfaces.Gerais
{
    public class GEEtiquetaAS : BaseAppService<GEEtiqueta>, IGEEtiquetaAS
    {
        private readonly IGEEtiquetaService _IGEEtiquetaService;

        public GEEtiquetaAS(IGEEtiquetaService pIGEEtiquetaService) :
             base(pIGEEtiquetaService)
        {
            _IGEEtiquetaService = pIGEEtiquetaService;
        }
    }
}
