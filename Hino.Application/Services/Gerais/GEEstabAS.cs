﻿using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;

namespace Hino.Application.Interfaces.Gerais
{
    public class GEEstabAS : BaseAppService<GEEstab>, IGEEstabAS
    {
        private readonly IGEEstabService _IGEEstabService;

        public GEEstabAS(IGEEstabService pIGEEstabService) :
             base(pIGEEstabService)
        {
            _IGEEstabService = pIGEEstabService;
        }
    }
}
