﻿using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;

namespace Hino.Application.Interfaces.Gerais
{
    public class GEEmpresaAS : BaseAppService<GEEmpresa>, IGEEmpresaAS
    {
        private readonly IGEEmpresaService _IGEEmpresaService;

        public GEEmpresaAS(IGEEmpresaService pIGEEmpresaService) :
             base(pIGEEmpresaService)
        {
            _IGEEmpresaService = pIGEEmpresaService;
        }
    }
}
