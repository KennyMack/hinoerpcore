﻿using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;

namespace Hino.Application.Interfaces.Gerais
{
    public class GEUsuariosAS : BaseAppService<GEUsuarios>, IGEUsuariosAS
    {
        private readonly IGEUsuariosService _IGEUsuariosService;

        public GEUsuariosAS(IGEUsuariosService pIGEUsuariosService) :
             base(pIGEUsuariosService)
        {
            _IGEUsuariosService = pIGEUsuariosService;
        }
    }
}
