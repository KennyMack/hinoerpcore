﻿using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;

namespace Hino.Application.Interfaces.Gerais
{
    public class GEFuncionariosAS : BaseAppService<GEFuncionarios>, IGEFuncionariosAS
    {
        private readonly IGEFuncionariosService _IGEFuncionariosService;

        public GEFuncionariosAS(IGEFuncionariosService pIGEFuncionariosService) :
             base(pIGEFuncionariosService)
        {
            _IGEFuncionariosService = pIGEFuncionariosService;
        }

        public GEFuncionarios GetByIdentficador(int pCodEstab, string pIdentificacao) =>
            _IGEFuncionariosService.GetByIdentficador(pCodEstab, pIdentificacao);
    }
}
