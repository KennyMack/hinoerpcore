﻿using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Infra.Cross.Entities.Gerais;

namespace Hino.Application.Interfaces.Gerais
{
    public class GEPessoaAS : BaseAppService<GEPessoa>, IGEPessoaAS
    {
        private readonly IGEPessoaService _IGEPessoaService;

        public GEPessoaAS(IGEPessoaService pIGEPessoaService) :
             base(pIGEPessoaService)
        {
            _IGEPessoaService = pIGEPessoaService;
        }
    }
}
