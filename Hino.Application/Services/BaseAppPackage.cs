﻿using Hino.Application.Interfaces;
using Hino.Domain.Base.Interfaces.Services;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application.Services
{
    public class BaseAppPackage : IBaseAppPackage, IDisposable
    {
        private readonly IBasePackage _BasePackage;
        public List<ModelException> Errors
        {
            get
            {
                return _BasePackage.Errors;
            }
            set
            {
                _BasePackage.Errors = value;
            }
        }

        public BaseAppPackage(IBasePackage pBasePackage)
        {
            _BasePackage = pBasePackage;
        }

        public void Dispose()
        {
            _BasePackage.Dispose();
        }
    }
}
