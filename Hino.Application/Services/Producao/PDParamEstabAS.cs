﻿using Hino.Domain.Producao.Interfaces.Services;
using Hino.Infra.Cross.Entities.Producao;

namespace Hino.Application.Interfaces.Producao
{
    public class PDParamEstabAS : BaseAppService<PDParamEstab>, IPDParamEstabAS
    {
        private readonly IPDParamEstabService _IPDParamEstabService;

        public PDParamEstabAS(IPDParamEstabService pIPDParamEstabService) :
             base(pIPDParamEstabService)
        {
            _IPDParamEstabService = pIPDParamEstabService;
        }
    }
}
