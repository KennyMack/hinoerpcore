﻿using Hino.Application.ViewModels.Producao.Ordem;
using Hino.Domain.Producao.Interfaces.Services.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Ordem
{
    public class PDOrdemProdCompAS : BaseAppService<PDOrdemProdComp>, IPDOrdemProdCompAS
    {
        private readonly IPDOrdemProdCompService _IPDOrdemProdCompService;

        public PDOrdemProdCompAS(IPDOrdemProdCompService pIPDOrdemProdCompService) :
             base(pIPDOrdemProdCompService)
        {
            _IPDOrdemProdCompService = pIPDOrdemProdCompService;
        }

        public async Task<List<PDOrdemProdCompNecVM>> SelectSaldoEstoqueNec(int pCodEstab, decimal pQtdLanc, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura)
        {
            var Componentes = await _IPDOrdemProdCompService.SelectSaldoEstoqueNec(pCodEstab, pQtdLanc, pCodOrdProd, pNivelOrdProd, pCodEstrutura);
            var ComponentesNec = new List<PDOrdemProdCompNecVM>();
            foreach (DataRow item in Componentes.Rows)
            {
                ComponentesNec.Add(new PDOrdemProdCompNecVM
                {
                    codcomponente = item["CODCOMPONENTE"].ToString(),
                    produtodescricao = item["FSPRODUTODESCRICAO"].ToString(),
                    codestoque = item["CODESTOQUE"].ToString(),
                    quantidade = Convert.ToDecimal(item["QUANTIDADE"]),
                    qtdconsumida = Convert.ToDecimal(item["QTDCONSUMIDA"]),
                    saldoestoque = Convert.ToDecimal(item["SLDEST"]),
                    programadoop = Convert.ToDecimal(item["PROGRAMADOOP"]),
                    qtdnecessaria = Convert.ToDecimal(item["SLDNEC"]),
                    codunidade = item["CODUNIDADE"].ToString(),
                    codestrutura = Convert.ToInt64(item["CODESTRUTURA"]),
                    codroteiro = Convert.ToInt16(item["CODROTEIRO"]),
                    operacao = Convert.ToInt16(item["OPERACAO"]),
                    fator = Convert.ToDecimal(item["FATOR"]),
                    statussaldo = item["STATUSSALDO"].ToString()
                });
            }
            return ComponentesNec;
        }


    }
}
