﻿using Hino.Domain.Producao.Interfaces.Services.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Ordem
{
    public class PDOrdemProdRotinasAS : BaseAppService<PDOrdemProdRotinas>, IPDOrdemProdRotinasAS
    {
        private readonly IPDOrdemProdRotinasService _IPDOrdemProdRotinasService;

        public PDOrdemProdRotinasAS(IPDOrdemProdRotinasService pIPDOrdemProdRotinasService) :
             base(pIPDOrdemProdRotinasService)
        {
            _IPDOrdemProdRotinasService = pIPDOrdemProdRotinasService;
        }

        public async Task<PDOrdemProdRotinas> BuscaMaqOperacaoOpAsync(PDOrdemProdRotinas pOPRotina) =>
            (await _IPDOrdemProdRotinasService.QueryAsync(r =>
                r.codestab == pOPRotina.codestab &&
                r.codordprod == pOPRotina.codordprod &&
                r.nivelordprod == pOPRotina.nivelordprod &&
                r.codestrutura == pOPRotina.codestrutura &&
                r.codroteiro == pOPRotina.codroteiro &&
                r.operacao == pOPRotina.operacao
                )
            ).FirstOrDefault();

        public async Task<List<PDOrdemProdRotinas>> BuscaOperacoesAptOpAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd) =>
            (await _IPDOrdemProdRotinasService.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codordprod == pCodOrdProd &&
                r.nivelordprod == pNivelOrdProd &&
                ((r.aptoprocesso) || (r.aptoacabado))
            ))
            .OrderBy(r => r.operacao)
            .ToList();
    }
}
