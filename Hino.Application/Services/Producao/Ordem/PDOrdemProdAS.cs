﻿using Hino.Domain.Producao.Interfaces.Services.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;

namespace Hino.Application.Interfaces.Producao.Ordem
{
    public class PDOrdemProdAS : BaseAppService<PDOrdemProd>, IPDOrdemProdAS
    {
        private readonly IPDOrdemProdService _IPDOrdemProdService;

        public PDOrdemProdAS(IPDOrdemProdService pIPDOrdemProdService) :
             base(pIPDOrdemProdService)
        {
            _IPDOrdemProdService = pIPDOrdemProdService;
        }

        public PDOrdemProd GetByBarras(long pSeqBarras) =>
            _IPDOrdemProdService.GetByBarras(pSeqBarras);
    }
}
