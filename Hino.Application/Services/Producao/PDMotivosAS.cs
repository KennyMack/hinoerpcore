﻿using Hino.Domain.Producao.Interfaces.Services;
using Hino.Infra.Cross.Entities.Producao;

namespace Hino.Application.Interfaces.Producao
{
    public class PDMotivosAS : BaseAppService<PDMotivos>, IPDMotivosAS
    {
        private readonly IPDMotivosService _IPDMotivosService;

        public PDMotivosAS(IPDMotivosService pIPDMotivosService) :
             base(pIPDMotivosService)
        {
            _IPDMotivosService = pIPDMotivosService;
        }
    }
}
