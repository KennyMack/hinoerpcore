﻿using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public class PDRetrabalhoAS : BaseAppService<PDRetrabalho>, IPDRetrabalhoAS
    {
        private readonly IPDRetrabalhoService _IPDRetrabalhoService;

        public PDRetrabalhoAS(IPDRetrabalhoService pIPDRetrabalhoService) :
             base(pIPDRetrabalhoService)
        {
            _IPDRetrabalhoService = pIPDRetrabalhoService;
        }
    }
}
