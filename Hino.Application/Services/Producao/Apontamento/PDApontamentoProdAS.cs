﻿using Hino.Application.AutoMapper;
using Hino.Application.Interfaces.Producao.Apontamento;
using Hino.Application.Interfaces.Producao.Ordem;
using Hino.Application.Validations.Producao.Apontamento;
using Hino.Application.ViewModels.Producao.Apontamento;
using Hino.Domain.Estoque.Interfaces.Services;
using Hino.Domain.Producao.Interfaces.Services;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.Cross.Utils.Enums;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Application.Services.Producao.Apontamento
{
    public class PDApontamentoProdAS : IPDApontamentoProdAS
    {
        public DbTransaction Transaction { get; set; }
        public List<ModelException> Errors { get; set; }
        private readonly IPCKG_Producao _IPCKG_Producao;
        private readonly IPCKG_Estoque _IPCKG_Estoque;
        private readonly IPDAptInicioProdAS _IPDAptInicioProdAS;
        private readonly IPDOrdemProdRotinasAS _IPDOrdemProdRotinasAS;
        private readonly IPDLancamentosAS _IPDLancamentosAS;
        private readonly IPDOrdemProdCompAS _IPDOrdemProdCompAS;

        public PDParamEstab ParamEstab { get; set; }
        public PDApontamentoProdVM PDApontamentoProd { get; set; }
        public PDLancamentosVM Lancamento { get; set; }

        public PDApontamentoProdAS(IPCKG_Producao pIPCKG_Producao,
            IPCKG_Estoque pIPCKG_Estoque,
            IPDAptInicioProdAS pIPDAptInicioProdAS,
            IPDOrdemProdRotinasAS pIPDOrdemProdRotinasAS,
            IPDOrdemProdCompAS pIPDOrdemProdCompAS,
            IPDLancamentosAS pIPDLancamentosAS)
        {
            _IPDOrdemProdCompAS = pIPDOrdemProdCompAS;
            _IPDLancamentosAS = pIPDLancamentosAS;
            _IPDOrdemProdRotinasAS = pIPDOrdemProdRotinasAS;
            _IPCKG_Estoque = pIPCKG_Estoque;
            _IPCKG_Producao = pIPCKG_Producao;
            _IPDAptInicioProdAS = pIPDAptInicioProdAS;
            Errors = new List<ModelException>();
        }

        #region Verifica Operações
        public async Task<bool> VerificaOperacoesAsync()
        {
            Errors.Clear();
            if (!LancamentoExists())
                return false;

            var retorno = await _IPCKG_Producao.VerificaOperacoesAsync(
                Lancamento.codestab,
                Lancamento.codordprod,
                Lancamento.nivelordprod,
                Lancamento.operacao,
                Lancamento.quantidade
            );

            Errors = _IPCKG_Producao.Errors;

            return retorno;
        }
        #endregion

        #region Check Operation And Save
        public async Task<bool> CheckOperationAndSaveAsync(PDAptInicioProd pdAptInicioProd)
        {
            if (await VerificaOperacoesAsync())
            {
                _IPDAptInicioProdAS.Add(pdAptInicioProd);

                Errors = _IPDAptInicioProdAS.Errors;
            }

            if (!Errors.Any())
            {
                await _IPDAptInicioProdAS.SaveChanges();
                Errors = _IPDAptInicioProdAS.Errors;
            }

            return !Errors.Any();
        }
        #endregion

        public async Task<DateTime> BuscaDataFechamentoAsync(int pCodEstab) =>
            await _IPCKG_Estoque.BuscaDataFechamentoAsync(pCodEstab);

        bool LancamentoExists()
        {
            if (Lancamento == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "",
                    Value = "",
                    Messages = new string[] { "LANCAMENTO NÃO DEFINIDO, ATRIBUIÇÃO DA CLASSE 'Lancamento' É OBRIGATÓRIA" }
                });

                return false;
            }
            return true;
        }

        public async Task<bool> ValidateApontamentoAsync()
        {
            Errors.Clear();
            if (!LancamentoExists())
                return false;
            var minutosApont = Convert.ToDecimal(Lancamento.dttermino
                .Subtract(Lancamento.dtinicio).TotalMinutes);

            var datafechamento = await BuscaDataFechamentoAsync(Lancamento.codestab);
            if ((datafechamento >= Lancamento.dttermino) ||
                (datafechamento >= Lancamento.dtinicio))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.BloquedByStockClosed }
                });
                return false;
            }

            if (ParamEstab.validadtapont &&
                minutosApont < ParamEstab.minutosapont)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "",
                    Value = "",
                    Messages = new string[] { string.Format(
                        Infra.Cross.Resources.ValidationMessagesResource.MinMinutesAppointment, ParamEstab.minutosapont) }
                });
                return false;
            }

            if (!PDApontamentoProd.apontaProcesso || PDApontamentoProd.aptoAcabado)
            {
                if (!await _IPCKG_Producao.GetValidaOpAptoAsync(
                    Lancamento.codestab, Lancamento.codordprod,
                    Lancamento.nivelordprod, Lancamento.codestrutura,
                    Lancamento.operacao, Lancamento.quantidade))
                {
                    Errors = _IPCKG_Producao.Errors;
                    return false;
                }

                if (!await _IPCKG_Producao.CheckUserPerLocEstAsync(
                    Lancamento.codestab, Lancamento.codordprod,
                    Lancamento.nivelordprod, Lancamento.codestrutura,
                    Lancamento.codfuncionario))
                {
                    Errors = _IPCKG_Producao.Errors;
                    return false;
                }

                if (!await _IPCKG_Producao.GetValidaOpAptoSldDtlancAsync(
                    Lancamento.codestab, Lancamento.codordprod,
                    Lancamento.nivelordprod, Lancamento.codestrutura,
                    Lancamento.dttermino, Lancamento.quantidade))
                {
                    Errors = _IPCKG_Producao.Errors;
                    return false;
                }
            }

            if (PDApontamentoProd.aptoAcabado && PDApontamentoProd.validaProcesso)
            {
                var Rotinas = (await _IPDOrdemProdRotinasAS.QueryAsync(r =>
                    r.codestab == Lancamento.codestab &&
                    r.codordprod == Lancamento.codordprod &&
                    r.nivelordprod == Lancamento.nivelordprod
                    ));

                if (Rotinas.Where(r => r.aptoprocesso && !r.aptoacabado).Count() > 1)
                {
                    var QtdSaldo = Rotinas.Where(r => r.aptoprocesso && !r.aptoacabado)
                        .Min(r => r.qtdeproducao - r.qtderefugo);

                    var QtdApt = Rotinas.Where(r => r.aptoacabado)
                        .Sum(r => r.qtdeproducao - r.qtderefugo);

                    PDApontamentoProd.saldoProcessoFinal = QtdSaldo - QtdApt;

                    if (PDApontamentoProd.saldoProcessoFinal <= 0)
                    {
                        Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                            Field = "",
                            Value = "",
                            Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.PreviousProcessHasNoBalance }
                        });
                        return false;
                    }
                }
                else
                    PDApontamentoProd.saldoProcessoFinal = Lancamento.quantidade;

                if (PDApontamentoProd.saldoProcessoFinal < Lancamento.quantidade)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.PreviousProcessHasNoBalance }
                    });
                    return false;
                }
            }

            if (PDApontamentoProd.validaProcesso)
            {
                if (!await VerificaOperacoesAsync())
                    return false;
            }

            return true;
        }

        public bool Validate(PDOrdemProdRotinas pOPRotinas)
        {
            Errors.Clear();
            if (!LancamentoExists())
                return false;

            var AptVal = new NovoApontamento(ParamEstab);
            Errors = AptVal.ValidateRules(Lancamento);

            return !Errors.Any();
        }

        public async Task<bool> CompComRastreabilidade()
        {
           var Componentes = await _IPDOrdemProdCompAS.QueryAsync(r =>
                r.codestab == Lancamento.codestab &&
                r.codordprod == Lancamento.codordprod &&
                r.nivelordprod == Lancamento.nivelordprod &&
                r.codestrutura == Lancamento.codestrutura &&
                r.baixaapto && r.FSProdutoPCP.rastreabilidade,
                s => s.FSProdutoParamEstab,
                s => s.FSProdutoParamEstab.FSProduto,
                t => t.FSProdutoPCP
            );

            return Componentes.Any();
        }

        #region Salvar Lançamento
        public async Task<EEstadoAtualApontamento> SalvarLancamentoAsync()
        {
            var retorno = EEstadoAtualApontamento.ERRO;

            Lancamento.codlancamento = _IPDLancamentosAS.NextSequence();

            _IPDLancamentosAS.Add(Mapper.Map<PDLancamentos>(Lancamento));

            if (!_IPDLancamentosAS.Errors.Any())
            {
                await _IPDLancamentosAS.SaveChanges();
                if (await CompComRastreabilidade())
                    retorno = EEstadoAtualApontamento.SELECIONALOTE;
                else
                    retorno = EEstadoAtualApontamento.SEMLOTE;
            }

            return retorno;
        }
        #endregion

        #region faz o fifo dos lotes disponiveis
        public async Task<bool> SelecionaLotesAppAsync()
        {
            var retorno = true;

            if (await this.CompComRastreabilidade())
            {
                if (!PDApontamentoProd.apontaProcesso || PDApontamentoProd.aptoAcabado)
                {
                    if (ParamEstab.fifolotesconsumo)
                    {
                        if (!await _IPCKG_Producao.ApontLotesFifoAsync(Mapper.Map<PDLancamentos>(Lancamento), PDApontamentoProd.codusuario, true))
                        {
                            Errors = _IPCKG_Producao.Errors;

                            return false;
                        }
                    }
                }
                else if (PDApontamentoProd.apontaProcesso)
                {
                    retorno = false;
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "",
                        Value = "",
                        Messages = new string[] { "PROCESSO" }
                    });
                }
            }

            return retorno;
        }
        #endregion

        #region Realiza os lancamentos e movimentos no kardex do lancamento 
        public async Task<bool> LancMovKardexAsync()
        {
            _IPCKG_Producao.Errors.Clear();
            Errors.Clear();

            var QtdRefugada = PDApontamentoProd.qtdTotRefugada ?? 0;
            var retorno = true;
            if (!PDApontamentoProd.apontaProcesso || PDApontamentoProd.aptoAcabado)
                retorno = await _IPCKG_Producao.AptoProducaoAsync(Mapper.Map<PDLancamentos>(Lancamento), PDApontamentoProd.codusuario, QtdRefugada);
            else if (PDApontamentoProd.apontaProcesso)
                retorno = await _IPCKG_Producao.LancConsumoProcessoAsync(Mapper.Map<PDLancamentos>(Lancamento), QtdRefugada, PDApontamentoProd.codusuario); // chamada de apt por operacao

            Errors = _IPCKG_Producao.Errors;

            return retorno;
        }
        #endregion
    }
}
