﻿using Hino.Aplication.Interfaces.Engenharia.Estrutura;
using Hino.Application.AutoMapper;
using Hino.Application.ConsoleUtils;
using Hino.Application.Interfaces.Fiscal.Produto;
using Hino.Application.Interfaces.Gerais;
using Hino.Application.Interfaces.Producao;
using Hino.Application.Interfaces.Producao.Apontamento;
using Hino.Application.Interfaces.Producao.Ordem;
using Hino.Application.Interfaces.Producao.Refugos;
using Hino.Application.ViewModels.Producao;
using Hino.Application.ViewModels.Producao.Apontamento;
using Hino.Application.ViewModels.Producao.Refugos;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.Cross.Entities.Producao.Refugos;
using Hino.Infra.Cross.Utils;
using Hino.Infra.Cross.Utils.Enums;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Application.Services.Producao.Apontamento
{
    public class PDApontProducaoAS: IPDApontProducaoAS
    {
        #region Propriedades
        private GEFuncionarios _geFuncionarios;
        private PDOrdemProdRotinas _pdOrdemProdRotinas;
        private PDOrdemProd _pdOrdemProd;
        private PDRefugoMotVM _pdRefugoMotAtual;
        private PDAptInicioProd _pdAptInicioProd;
        private PDAptTerminoProd _pdAptTerminoProd;
        // private PDEtiqAptProducao _pdEtiqAptProducao;
        private PDParamEstab _pdParamEstab;
        private PDMotivosVM _pdMotivos;

        private IFSProdutoParamEstabAS _IFSProdutoParamEstabAS;
        private IFSProdutoPCPAS _IFSProdutoPCPAS;
        private IPCKG_ProducaoAS _IPCKG_ProducaoAS;
        private IPDApontamentoProdAS _IPDApontamentoProdAS;
        private IGEFuncionariosAS _IGEFuncionariosAS;
        private IPDOrdemProdRotinasAS _IPDOrdemProdRotinasAS;
        private IPDOrdemProdCompAS _IPDOrdemProdCompAS;
        private IPDOrdemProdAS _IPDOrdemProdAS;
        private IPDRefugoMotAS _IPDRefugoMotAS;
        private IPDAptInicioProdAS _IPDAptInicioProdAS;
        private IPDAptTerminoProdAS _IPDAptTerminoProdAS;
        // private IPDEtiqAptProducaoAS _IPDEtiqAptProducaoAS;
        private IPDParamEstabAS _IPDParamEstabAS;
        private IPDMotivosAS _IPDMotivosAS;
        private IPDRefugoReapAS _IPDRefugoReapAS;
        private IENEstComponentesAS _IENEstComponentesAS;


        private List<PDRefugoMotVM> _lstpdRefugoMot;
        private ETiposApontamento.INICIOPREPARACAO InicioPreparacao;
        private ETiposApontamento.TERMINOPREPARACAO TerminoPreparacao;
        private ETiposApontamento.INICIOPRODUCAO InicioProducao;
        private ETiposApontamento.TERMINOPRODUCAO TerminoProducao;
        private ETiposApontamento.INICIOTROCASERVICO InicioTrocaServico;
        private ETiposApontamento.TERMINOTROCASERVICO TerminoTrocaServico;
        private ETiposApontamento.INICIOMANUTENCAO InicioManutencao;
        private ETiposApontamento.TERMINOMANUTENCAO TerminoManutencao;
        private ETiposApontamento.INICIOCONSULTADESENHO InicioConsultaDesenho;
        private ETiposApontamento.TERMINOCONSULTADESENHO TerminoConsultaDesenho;
        private ETiposApontamento.INICIORETRABALHO InicioRetrabalho;
        private ETiposApontamento.TERMINORETRABALHO TerminoRetrabalho;
        private ETiposApontamento.TERMINOTURNO TerminoTurno;
        #endregion

        public PDApontProducaoAS(IGEFuncionariosAS pIGEFuncionariosAS,
            IPDOrdemProdRotinasAS pIPDOrdemProdRotinasAS,
            IPDOrdemProdAS pIPDOrdemProdAS,
            IPDRefugoMotAS pIPDRefugoMotAS,
            IPDAptInicioProdAS pIPDAptInicioProdAS,
            IPDAptTerminoProdAS pIPDAptTerminoProdAS,
            // IPDEtiqAptProducaoAS pIPDEtiqAptProducaoAS,
            IPDParamEstabAS pIPDParamEstabAS,
            IPDMotivosAS pIPDMotivosAS,
            IPDRefugoReapAS pIPDRefugoReapAS,
            IPDOrdemProdCompAS pIPDOrdemProdCompAS,
            IENEstComponentesAS pIENEstComponentesAS,
            IPDApontamentoProdAS pIPDApontamentoProdAS,
            IPCKG_ProducaoAS pIPCKG_ProducaoAS,
            IFSProdutoParamEstabAS pIFSProdutoParamEstabAS,
            IFSProdutoPCPAS pIFSProdutoPCPAS
            )
        {
            _IFSProdutoParamEstabAS = pIFSProdutoParamEstabAS;
            _IFSProdutoPCPAS = pIFSProdutoPCPAS;
            _IPCKG_ProducaoAS = pIPCKG_ProducaoAS;
            _IPDApontamentoProdAS = pIPDApontamentoProdAS;
            _IENEstComponentesAS = pIENEstComponentesAS;
            _IPDOrdemProdCompAS = pIPDOrdemProdCompAS;
            _IPDRefugoReapAS = pIPDRefugoReapAS;
            _IGEFuncionariosAS = pIGEFuncionariosAS;
            _IPDOrdemProdRotinasAS = pIPDOrdemProdRotinasAS;
            _IPDOrdemProdAS = pIPDOrdemProdAS;
            _IPDRefugoMotAS = pIPDRefugoMotAS;
            _IPDAptInicioProdAS = pIPDAptInicioProdAS;
            _IPDAptTerminoProdAS = pIPDAptTerminoProdAS;
            // _IPDEtiqAptProducaoAS = pIPDEtiqAptProducaoAS;
            _IPDParamEstabAS = pIPDParamEstabAS;
            _IPDMotivosAS = pIPDMotivosAS;

            _lstpdRefugoMot = new List<PDRefugoMotVM>();
            _pdAptInicioProd = new PDAptInicioProd();
            _pdAptTerminoProd = new PDAptTerminoProd();
        }

        #region Set funcionario atual
        public void SetFuncionarioAtual(GEFuncionarios pFuncionario) =>
            _geFuncionarios = pFuncionario;
        #endregion

        #region Set Param Produção atual
        public void SetParamEstabAtual(PDParamEstab ppdParamEstab) =>
            _pdParamEstab = ppdParamEstab;
        #endregion

        #region Carrega OP
        private ETiposApontamento.INICIOPRODUCAO CarregaOP(string pOp)
        {
            var retorno = ETiposApontamento.INICIOPRODUCAO.OPERACAO;
            if (int.TryParse(pOp, out int barras))
            {
                _pdOrdemProd = _IPDOrdemProdAS.GetByBarras(barras);
                if (_pdOrdemProd != null)
                {
                    if (_pdOrdemProd.status == "E")
                        retorno = TermConsole.ExibeMensagemLine("Ordem ja encerrada", ETiposApontamento.INICIOPRODUCAO.ORDEMPROD);
                }
                else
                    retorno = TermConsole.ExibeMensagemLine("Ordem nao encontrada", ETiposApontamento.INICIOPRODUCAO.ORDEMPROD);
            }
            else
                retorno = TermConsole.ExibeMensagemLine("Barras invalido", ETiposApontamento.INICIOPRODUCAO.ORDEMPROD);

            return retorno;
        }
        #endregion

        #region Produção
        #region Apt. Inicio Produção
        #region Inicio de produção
        public async Task<ETiposApontamento.INICIO> InicioProducaoTerminal()
        {
            var limpaConsole = true;
            var teclado = "";
            var retorno = ETiposApontamento.INICIO.APONTAMENTO;
            do
            {
                switch (InicioProducao)
                {
                    case ETiposApontamento.INICIOPRODUCAO.ORDEMPROD:
                        if (limpaConsole)
                        {
                            limpaConsole = false;
                            TermConsole.LimparConsole();
                            TermConsole.ImprimirCabecalho();
                            TermConsole.ImprimirUsuario(_geFuncionarios);
                        }
                        InicioProducao = TermConsole.ExibeMensagemLine("Inicio Producao", ETiposApontamento.INICIOPRODUCAO.DIGORDEMPROD);
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.DIGORDEMPROD:
                        TermConsole.ExibeMensagem("OP: ");
                        teclado = "";
                        teclado = Console.ReadLine();

                        if (!string.IsNullOrEmpty(teclado))
                        {
                            InicioProducao = CarregaOP(teclado);
                        }
                        else
                        {
                            InicioProducao = ETiposApontamento.INICIOPRODUCAO.CANCELADO;
                        }
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.OPERACAO:
                        TermConsole.ExibeMensagemLine(string.Format("OP: {0} - {1}", _pdOrdemProd.codordprod, _pdOrdemProd.nivelordprod));
                        InicioProducao = TermConsole.ExibeMensagem("Operacao: ", ETiposApontamento.INICIOPRODUCAO.DIGOPERACAO);
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.DIGOPERACAO:

                        var dtOper = (await _IPDOrdemProdRotinasAS.QueryAsync(r =>
                            r.codestab == Globals.CodEstab &&
                            r.codordprod == _pdOrdemProd.codordprod &&
                            r.nivelordprod == _pdOrdemProd.nivelordprod &&
                            ((r.aptoprocesso) || (r.aptoacabado))
                        )).ToArray();

                        if (dtOper.Count() > 1)
                        {
                            teclado = "";
                            teclado = Console.ReadLine();

                            if (!string.IsNullOrEmpty(teclado))
                                InicioProducao = CarregaInicioOperacao(teclado);
                            else
                                InicioProducao = ETiposApontamento.INICIOPRODUCAO.ORDEMPROD;
                        }
                        else if (dtOper.Count() == 1)
                        {
                            TermConsole.ExibeMensagemLine(dtOper[0].operacao.ToString());

                            InicioProducao = CarregaInicioOperacao(dtOper[0].operacao.ToString());

                            if (InicioProducao == ETiposApontamento.INICIOPRODUCAO.OPERACAO)
                                InicioProducao = ETiposApontamento.INICIOPRODUCAO.ORDEMPROD;
                        }
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.VERIFICAINICIOOPERACAO:
                        InicioProducao = await VerificaInicio();
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.SALVAR:
                        TermConsole.ExibeMensagemLine("Confirma os dados?");
                        TermConsole.ExibeMensagemLine("Inicio Producao");
                        TermConsole.ImprimirUsuario(_geFuncionarios);
                        TermConsole.ExibeMensagemLine(string.Format("OP: {0} - {1}", _pdOrdemProd.codordprod, _pdOrdemProd.nivelordprod));
                        TermConsole.ExibeMensagemLine(string.Format("Operacao: {0}", _pdAptInicioProd.operacao));

                        InicioProducao = TermConsole.ExibeMensagem("0 - nao 1 - sim: ", ETiposApontamento.INICIOPRODUCAO.DIGSALVAR);
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.DIGSALVAR:
                        teclado = "";
                        teclado = Console.ReadLine();

                        if (!string.IsNullOrEmpty(teclado))
                        {
                            InicioProducao = await SalvarInicio(teclado);
                        }
                        else
                        {
                            InicioProducao = ETiposApontamento.INICIOPRODUCAO.SALVAR;
                        }
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.CONCLUIDO:
                        var MaqOper = _IPDOrdemProdRotinasAS.GetFirstByWhere(r =>
                            r.codestab == Globals.CodEstab &&
                            r.codordprod == _pdOrdemProd.codordprod &&
                            r.nivelordprod == _pdOrdemProd.nivelordprod &&
                            r.codestrutura == _pdOrdemProd.codestrutura &&
                            r.codroteiro == _pdOrdemProd.codroteiro &&
                            r.operacao == 10
                        );

                        GerarAptInput(1, 1, 5, MaqOper.codmaquina);


                        retorno = TermConsole.ExibeMensagemLine("Salvo com sucesso.", ETiposApontamento.INICIO.INICIO);
                        InicioProducao = ETiposApontamento.INICIOPRODUCAO.SAIR;
                        Console.ReadKey();
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.CANCELADO:
                        InicioProducao = ETiposApontamento.INICIOPRODUCAO.SAIR;
                        retorno = ETiposApontamento.INICIO.INICIO;
                        break;
                    case ETiposApontamento.INICIOPRODUCAO.SAIR:
                        break;
                }

            } while (InicioProducao != ETiposApontamento.INICIOPRODUCAO.SAIR);


            return retorno;
        }
        #endregion

        #region Carrega Inicio Operação
        private ETiposApontamento.INICIOPRODUCAO CarregaInicioOperacao(string pOperacao)
        {
            var retorno = ETiposApontamento.INICIOPRODUCAO.VERIFICAINICIOOPERACAO;
            var operacao = (short)0;
            if (short.TryParse(pOperacao, out operacao))
            {
                _pdOrdemProdRotinas = _IPDOrdemProdRotinasAS.GetFirstByWhere(r =>
                    r.codestab == Globals.CodEstab &&
                    r.codordprod == _pdOrdemProd.codordprod &&
                    r.nivelordprod == _pdOrdemProd.nivelordprod &&
                    r.codestrutura == _pdOrdemProd.codestrutura &&
                    r.codroteiro == _pdOrdemProd.codroteiro &&
                    r.operacao == operacao
                );

                _pdOrdemProdRotinas.codestab = Globals.CodEstab;
                _pdOrdemProdRotinas.codordprod = _pdOrdemProd.codordprod;
                _pdOrdemProdRotinas.nivelordprod = _pdOrdemProd.nivelordprod;
                _pdOrdemProdRotinas.codestrutura = _pdOrdemProd.codestrutura;
                _pdOrdemProdRotinas.codroteiro = _pdOrdemProd.codroteiro;
                _pdOrdemProdRotinas.operacao = operacao;

                if (_pdOrdemProdRotinas != null)
                {
                    _pdAptInicioProd.operacao = operacao;
                    _pdAptInicioProd.codestab = Globals.CodEstab;
                    _pdAptInicioProd.codordprod = _pdOrdemProd.codordprod;
                    _pdAptInicioProd.nivelordprod = _pdOrdemProd.nivelordprod;
                    _pdAptInicioProd.codestrutura = _pdOrdemProd.codestrutura;
                    _pdAptInicioProd.codroteiro = _pdOrdemProd.codroteiro;
                    _pdAptInicioProd.dtinicio = DateTime.Now;
                    _pdAptInicioProd.codiniapt = _IPDAptInicioProdAS.NextSequence();
                    _pdAptInicioProd.codusuario = _geFuncionarios.codusuario;
                    _pdAptInicioProd.codfuncionario = _geFuncionarios.codfuncionario;
                    _pdAptInicioProd.tipo = 3;
                }
                else
                    retorno = TermConsole.ExibeMensagemLine("operacao nao encontrada", ETiposApontamento.INICIOPRODUCAO.OPERACAO);
            }
            else
                retorno = TermConsole.ExibeMensagemLine("operacao invalida", ETiposApontamento.INICIOPRODUCAO.OPERACAO);

            return retorno;
        }
        #endregion        

        #region Verifica inicio de operação
        private async Task<ETiposApontamento.INICIOPRODUCAO> VerificaInicio()
        {
            var retorno = ETiposApontamento.INICIOPRODUCAO.SALVAR;

            if (await _IPDAptInicioProdAS.WasStartedAsync(_pdAptInicioProd))
                return TermConsole.ExibeMensagemLine("operacao ja iniciada", ETiposApontamento.INICIOPRODUCAO.ORDEMPROD);

            return retorno;
        }
        #endregion

        #region Salvar Inicio
        private async Task<ETiposApontamento.INICIOPRODUCAO> SalvarInicio(string pSalvar)
        {

            var retorno = ETiposApontamento.INICIOPRODUCAO.CONCLUIDO;
            var opcao = (short)0;

            if (short.TryParse(pSalvar, out opcao))
            {
                if (opcao.In<short>(0, 1))
                {
                    if (opcao == 1)
                    {
                        var _pdLancamentos = new PDLancamentosVM
                        {
                            codestab = Globals.CodEstab,
                            codlancamento = 0,
                            codordprod = _pdOrdemProd.codordprod,
                            nivelordprod = _pdOrdemProd.nivelordprod,
                            operacao = _pdAptInicioProd.operacao,
                            quantidade = 0,
                            codestrutura = _pdOrdemProd.codestrutura,
                            codroteiro = _pdOrdemProd.codroteiro
                        };

                        _IPDApontamentoProdAS.Lancamento = _pdLancamentos;

                        TermConsole.ExibeMensagemLine("Aguarde...");

                        if (!await _IPDApontamentoProdAS.CheckOperationAndSaveAsync(_pdAptInicioProd))
                            retorno = TermConsole.ExibeMensagemLine(_IPDApontamentoProdAS.Errors.ToMessage(), ETiposApontamento.INICIOPRODUCAO.ORDEMPROD);


                        /*
                        if (string.IsNullOrEmpty(msErro) &&
                            _pdParamEstab.imprimelanc)
                        {

                        }
                        */
                    }
                    else
                        retorno = ETiposApontamento.INICIOPRODUCAO.ORDEMPROD;
                }
                else
                    retorno = TermConsole.ExibeMensagemLine("opcao invalida", ETiposApontamento.INICIOPRODUCAO.ORDEMPROD);
            }
            else
                retorno = TermConsole.ExibeMensagemLine("opcao invalida", ETiposApontamento.INICIOPRODUCAO.SALVAR);

            return retorno;
        }
        #endregion
        #endregion

        #region Apt. Término Produção

        #region Termino de produção
        public async Task<ETiposApontamento.INICIO> TerminoProducaoTerminal()
        {
            var limpaConsole = true;
            var teclado = "";
            var retorno = ETiposApontamento.INICIO.APONTAMENTO;
            do
            {
                switch (TerminoProducao)
                {
                    case ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD:
                        if (limpaConsole)
                        {
                            limpaConsole = false;
                            TermConsole.LimparConsole();
                            TermConsole.ImprimirCabecalho();
                            TermConsole.ImprimirUsuario(_geFuncionarios);
                        }
                        TerminoProducao = TermConsole.ExibeMensagemLine("Termino Producao", ETiposApontamento.TERMINOPRODUCAO.DIGORDEMPROD);
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.DIGORDEMPROD:
                        TermConsole.ExibeMensagem("OP: ");
                        teclado = "";
                        teclado = Console.ReadLine();

                        if (!string.IsNullOrEmpty(teclado))
                        {
                            TerminoProducao = CarregaOP(teclado) == ETiposApontamento.INICIOPRODUCAO.OPERACAO ?
                                     ETiposApontamento.TERMINOPRODUCAO.OPERACAO :
                                     ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                        }
                        else
                        {
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.CANCELADO;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.OPERACAO:
                        TermConsole.ExibeMensagemLine(string.Format("OP: {0} - {1}", _pdOrdemProd.codordprod, _pdOrdemProd.nivelordprod));
                        TerminoProducao = TermConsole.ExibeMensagem("Operacao: ", ETiposApontamento.TERMINOPRODUCAO.DIGOPERACAO);
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.DIGOPERACAO:
                        var OperacoesOP = await BuscaOperacoesAptOp(_pdOrdemProd.codordprod, _pdOrdemProd.nivelordprod, Globals.CodEstab);

                        if (OperacoesOP.Count > 1)
                        {
                            teclado = "";
                            teclado = Console.ReadLine();

                            if (!string.IsNullOrEmpty(teclado))
                            {
                                TerminoProducao = await CarregaTerminoOperacao(teclado);
                            }
                            else
                            {
                                TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                            }
                        }
                        else if (OperacoesOP.Count == 1)
                        {
                            TermConsole.ExibeMensagemLine(OperacoesOP[0].operacao.ToString());

                            TerminoProducao = await CarregaTerminoOperacao(OperacoesOP[0].operacao.ToString());

                            if (TerminoProducao == ETiposApontamento.TERMINOPRODUCAO.OPERACAO)
                                TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.VERIFICATERMINOOPERACAO:
                        TerminoProducao = await VerificaTermino();
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.PECASBOAS:
                        TerminoProducao = TermConsole.ExibeMensagem("BOAS: ", ETiposApontamento.TERMINOPRODUCAO.DIGPECASBOAS);
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.DIGPECASBOAS:
                        teclado = "";
                        teclado = Console.ReadLine();

                        if (!string.IsNullOrEmpty(teclado))
                        {
                            TerminoProducao = QuantidadeBoas(teclado);
                        }
                        else
                        {
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.PECASREFUGO:
                        _lstpdRefugoMot.Clear();
                        if (!string.IsNullOrEmpty(_pdParamEstab.codmotrefugo.ToString()))
                            TerminoProducao = TermConsole.ExibeMensagem("REFUGO: ", ETiposApontamento.TERMINOPRODUCAO.DIGPECASREFUGO);
                        else
                            TerminoProducao = TermConsole.ExibeMensagem("TOTAL REFUGO: ", ETiposApontamento.TERMINOPRODUCAO.DIGPECASREFUGO);
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.DIGPECASREFUGO:
                        teclado = "";
                        teclado = Console.ReadLine();

                        if (!string.IsNullOrEmpty(teclado))
                        {
                            TerminoProducao = QuantidadeRefugo(teclado);
                        }
                        else
                        {
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.MOTREFUGO:
                        if (!string.IsNullOrEmpty(_pdParamEstab.codmotrefugo.ToString()))
                        {
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.PECASRETRABALHO;
                        }
                        else
                        {
                            TerminoProducao = TermConsole.ExibeMensagem("MOTIVO REFUGO: ", ETiposApontamento.TERMINOPRODUCAO.DIGMOTREFUGO);
                        }

                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.DIGMOTREFUGO:
                        teclado = "";
                        teclado = Console.ReadLine();

                        if (!string.IsNullOrEmpty(teclado))
                        {
                            TerminoProducao = CodMotivoRefugo(teclado);
                        }
                        else
                        {
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.MOTREFUGOQTD:
                        TerminoProducao = TermConsole.ExibeMensagem(
                            string.Format("QTD REFUGO '{0} - {1}': ",
                            _pdRefugoMotAtual.codmotivo, _pdRefugoMotAtual.PDMotivos.descricao), ETiposApontamento.TERMINOPRODUCAO.DIGMOTIVOREFUGOQTD);
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.DIGMOTIVOREFUGOQTD:
                        teclado = "";
                        teclado = Console.ReadLine();

                        if (!string.IsNullOrEmpty(teclado))
                        {
                            TerminoProducao = AddQuantidadeRefugoMotivo(teclado);
                        }
                        else
                        {
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.PECASRETRABALHO:
                        if (_pdParamEstab.aptretrab)
                            TerminoProducao = TermConsole.ExibeMensagem("RETRABALHO: ", ETiposApontamento.TERMINOPRODUCAO.DIGPECASRETRABALHO);
                        else
                        {
                            _pdAptTerminoProd.qtdretrabalho = 0;
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.SALVAR;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.DIGPECASRETRABALHO:
                        teclado = "";
                        teclado = Console.ReadLine();

                        if (!string.IsNullOrEmpty(teclado))
                        {
                            TerminoProducao = QuantidadeRetrabalho(teclado);
                        }
                        else
                        {
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.SALVAR:
                        TermConsole.ExibeMensagemLine("Confirma os dados?");
                        TermConsole.ExibeMensagemLine("Termino Producao");
                        TermConsole.ImprimirUsuario(_geFuncionarios);
                        TermConsole.ExibeMensagemLine(string.Format("OP: {0} - {1}", _pdOrdemProd.codordprod, _pdOrdemProd.nivelordprod));
                        TermConsole.ExibeMensagemLine(string.Format("Operacao: {0}", _pdAptInicioProd.operacao));
                        TermConsole.ExibeMensagemLine(string.Format("Boas: {0}", _pdAptTerminoProd.quantidade));
                        TermConsole.ExibeMensagemLine(string.Format("Refugo: {0}", _pdAptTerminoProd.qtdrefugo));
                        if (_pdParamEstab.aptretrab)
                            TermConsole.ExibeMensagemLine(string.Format("Retrabalho: {0}", _pdAptTerminoProd.qtdretrabalho));

                        TerminoProducao = TermConsole.ExibeMensagem("0 - nao 1 - sim: ", ETiposApontamento.TERMINOPRODUCAO.DIGSALVAR);
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.DIGSALVAR:
                        teclado = "";
                        teclado = Console.ReadLine();
                        if (!string.IsNullOrEmpty(teclado))
                        {
                            var opt = 0;

                            if (int.TryParse(teclado, out opt) &&
                                opt.In(0, 1))
                            {
                                if (opt == 1)
                                {
                                    TerminoProducao = await SalvarAptFimApontamento();
                                }
                                else
                                    TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD;
                            }
                            else
                                TerminoProducao = TermConsole.ExibeMensagemLine("Opcao invalida", ETiposApontamento.TERMINOPRODUCAO.SALVAR);
                        }
                        else
                        {
                            TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.SALVAR;
                        }
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.CONCLUIDO:
                        GerarAptInput(5, 1, 5, (await BuscaMaqPrimeiraOper()).codmaquina);

                        retorno = TermConsole.ExibeMensagemLine("Salvo com sucesso.", ETiposApontamento.INICIO.INICIO);
                        TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.SAIR;
                        Console.ReadKey();
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.CANCELADO:
                        TerminoProducao = ETiposApontamento.TERMINOPRODUCAO.SAIR;
                        retorno = ETiposApontamento.INICIO.INICIO;
                        break;
                    case ETiposApontamento.TERMINOPRODUCAO.SAIR:
                        break;
                }

            } while (TerminoProducao != ETiposApontamento.TERMINOPRODUCAO.SAIR);

            return retorno;
        }
        #endregion

        #region Verifica termino de operação
        private async Task<ETiposApontamento.TERMINOPRODUCAO> VerificaTermino()
        {
            var retorno = ETiposApontamento.TERMINOPRODUCAO.PECASBOAS;

            if (await _IPDAptTerminoProdAS.WasFinishedAsync(_pdAptInicioProd))
                return TermConsole.ExibeMensagemLine("E18:Operacao ja encerrada", ETiposApontamento.TERMINOPRODUCAO.ORDEMPROD);

            return retorno;
        }
        #endregion

        #region Carrega Termino Operação
        private async Task<ETiposApontamento.TERMINOPRODUCAO> CarregaTerminoOperacao(string pOperacao)
        {
            var retorno = ETiposApontamento.TERMINOPRODUCAO.VERIFICATERMINOOPERACAO;
            var operacao = (short)0;
            if (short.TryParse(pOperacao, out operacao))
            {
                _pdOrdemProdRotinas = await _IPDOrdemProdRotinasAS.BuscaMaqOperacaoOpAsync(new PDOrdemProdRotinas
                {
                    codestab = Globals.CodEstab,
                    codordprod = _pdOrdemProd.codordprod,
                    nivelordprod = _pdOrdemProd.nivelordprod,
                    codestrutura = _pdOrdemProd.codestrutura,
                    codroteiro = _pdOrdemProd.codroteiro,
                    operacao = operacao
                });

                if (_pdOrdemProdRotinas != null)
                {
                    _pdAptInicioProd = new PDAptInicioProd
                    {
                        operacao = operacao,
                        codestab = Globals.CodEstab,
                        codordprod = _pdOrdemProd.codordprod,
                        nivelordprod = _pdOrdemProd.nivelordprod,
                        codestrutura = _pdOrdemProd.codestrutura,
                        codroteiro = _pdOrdemProd.codroteiro,
                        codusuario = _geFuncionarios.codusuario,
                        codfuncionario = _geFuncionarios.codfuncionario,
                        tipo = 3
                    };

                    _pdAptInicioProd = await _IPDAptInicioProdAS.GetLastAptAsync(_pdAptInicioProd);

                    if (_pdAptInicioProd != null)
                    {
                        _pdAptTerminoProd.codfimapt = _IPDAptTerminoProdAS.NextSequence();
                        _pdAptTerminoProd.codestab = Globals.CodEstab;
                        _pdAptTerminoProd.codiniapt = _pdAptInicioProd.codiniapt;
                        _pdAptTerminoProd.codusuario = _geFuncionarios.codusuario;
                        _pdAptTerminoProd.codfuncionario = _geFuncionarios.codfuncionario;
                        _pdAptTerminoProd.dttermino = DateTime.Now;
                        _pdAptTerminoProd.tipo = 4;
                    }
                    else
                        retorno = TermConsole.ExibeMensagemLine("E19:operacao nao iniciada", ETiposApontamento.TERMINOPRODUCAO.OPERACAO);
                }
                else
                    retorno = TermConsole.ExibeMensagemLine("E20:operacao nao encontrada", ETiposApontamento.TERMINOPRODUCAO.OPERACAO);
            }
            else
                retorno = TermConsole.ExibeMensagemLine("E21:operacao invalida", ETiposApontamento.TERMINOPRODUCAO.OPERACAO);

            return retorno;
        }
        #endregion

        #region Quantidade Boas
        private ETiposApontamento.TERMINOPRODUCAO QuantidadeBoas(string pValor)
        {
            var retorno = ETiposApontamento.TERMINOPRODUCAO.PECASREFUGO;
            _pdAptTerminoProd.quantidade = 0;
            var qtd = 0M;

            if (decimal.TryParse(pValor, out qtd))
                _pdAptTerminoProd.quantidade = qtd;
            else
                retorno = TermConsole.ExibeMensagemLine("Qtd. invalida", ETiposApontamento.TERMINOPRODUCAO.DIGPECASBOAS);
            return retorno;
        }
        #endregion

        #region Cód. Motivo Refugo
        private ETiposApontamento.TERMINOPRODUCAO CodMotivoRefugo(string pValor)
        {
            var retorno = ETiposApontamento.TERMINOPRODUCAO.MOTREFUGOQTD;
            var codmotivo = 0;
            if (int.TryParse(pValor, out codmotivo))
            {
                _pdMotivos = Mapper.Map<PDMotivosVM>(_IPDMotivosAS.GetFirstByWhere(r => r.codmotivo == codmotivo));

                if (_pdMotivos != null)
                {
                    if (_pdMotivos.tipo != 1 || !_pdMotivos.status)
                        retorno = TermConsole.ExibeMensagemLine("Motivo invalido", ETiposApontamento.TERMINOPRODUCAO.MOTREFUGO);
                    else
                    {
                        _pdRefugoMotAtual = new PDRefugoMotVM
                        {
                            codestab = Globals.CodEstab,
                            codmotivo = _pdMotivos.codmotivo,
                            PDMotivos = _pdMotivos,
                            qtdrefugada = 0
                        };
                    }
                }
                else
                    retorno = TermConsole.ExibeMensagemLine("Motivo nao encontrado", ETiposApontamento.TERMINOPRODUCAO.MOTREFUGO);
            }
            else
                retorno = TermConsole.ExibeMensagemLine("Motivo invalido", ETiposApontamento.TERMINOPRODUCAO.MOTREFUGO);

            return retorno;
        }
        #endregion

        #region Quantidade Refugo
        private ETiposApontamento.TERMINOPRODUCAO QuantidadeRefugo(string pValor)
        {
            var retorno = ETiposApontamento.TERMINOPRODUCAO.PECASRETRABALHO;
            _pdAptTerminoProd.qtdrefugo = 0;
            var qtd = 0M;

            if (decimal.TryParse(pValor, out qtd))
            {
                _pdAptTerminoProd.qtdrefugo = qtd;

                if (_pdAptTerminoProd.qtdrefugo > 0)
                    retorno = ETiposApontamento.TERMINOPRODUCAO.MOTREFUGO;
            }
            else
            {
                retorno = TermConsole.ExibeMensagemLine("Qtd. invalida", ETiposApontamento.TERMINOPRODUCAO.DIGPECASREFUGO);
            }

            return retorno;
        }
        #endregion

        #region Add Quantidade Refugo Motivo
        private ETiposApontamento.TERMINOPRODUCAO AddQuantidadeRefugoMotivo(string pValor)
        {
            var retorno = ETiposApontamento.TERMINOPRODUCAO.MOTREFUGO;
            var qtd = 0M;

            if (decimal.TryParse(pValor, out qtd))
            {
                _pdRefugoMotAtual.qtdrefugada = qtd;
                if (_lstpdRefugoMot.Exists(r => r.codmotivo == _pdRefugoMotAtual.codmotivo))
                {
                    _lstpdRefugoMot.Where(r => r.codmotivo == _pdRefugoMotAtual.codmotivo)
                        .First()
                        .qtdrefugada += qtd;
                }
                else
                    _lstpdRefugoMot.Add(_pdRefugoMotAtual);

                if (_lstpdRefugoMot.Sum(r => (decimal)r.qtdrefugada) > _pdAptTerminoProd.qtdrefugo)
                    retorno = TermConsole.ExibeMensagemLine("Qtd. total de refugo nos motivos superior ao total do refugo informada.", ETiposApontamento.TERMINOPRODUCAO.PECASREFUGO);
                if (_lstpdRefugoMot.Sum(r => (decimal)r.qtdrefugada) == _pdAptTerminoProd.qtdrefugo)
                    retorno = ETiposApontamento.TERMINOPRODUCAO.PECASRETRABALHO;
            }
            else
            {
                retorno = TermConsole.ExibeMensagemLine("Qtd. invalida", ETiposApontamento.TERMINOPRODUCAO.DIGPECASREFUGO);
            }

            return retorno;
        }
        #endregion

        #region Quantidade Retrabalho
        private ETiposApontamento.TERMINOPRODUCAO QuantidadeRetrabalho(string pValor)
        {
            var retorno = ETiposApontamento.TERMINOPRODUCAO.SALVAR;
            _pdAptTerminoProd.qtdretrabalho = 0;
            var qtd = 0M;

            if (decimal.TryParse(pValor, out qtd))
            {
                if (qtd > 0 &&
                    _pdAptTerminoProd.quantidade > qtd)
                {
                    _pdAptTerminoProd.qtdretrabalho = qtd;
                }
                else if (qtd > 0)
                {
                    retorno = TermConsole.ExibeMensagemLine("Qtd. invalida", ETiposApontamento.TERMINOPRODUCAO.DIGPECASRETRABALHO);
                }
            }
            else
            {
                retorno = TermConsole.ExibeMensagemLine("Qtd. invalida", ETiposApontamento.TERMINOPRODUCAO.DIGPECASRETRABALHO);
            }
            return retorno;
        }
        #endregion

        #region Salvar Apt Fim Apontamento
        private async Task<ETiposApontamento.TERMINOPRODUCAO> SalvarAptFimApontamento()
        {
            TermConsole.ExibeMensagemLine("Aguarde...");
            var retorno = ETiposApontamento.TERMINOPRODUCAO.CONCLUIDO;

            var _fsProdutoParamEstab = _IFSProdutoParamEstabAS.GetFirstByWhere(r =>
                r.codestab == Globals.CodEstab &&
                r.codproduto == _pdOrdemProd.codproduto
            );

            var _fsProdutoPcp = _IFSProdutoPCPAS.GetFirstByWhere(r =>
                r.codestab == Globals.CodEstab &&
                r.codproduto == _pdOrdemProd.codproduto
            );

            var _pdLancamentos = new PDLancamentosVM
            {
                codestab = Globals.CodEstab,
                codlancamento = 0,
                codordprod = _pdOrdemProd.codordprod,
                nivelordprod = _pdOrdemProd.nivelordprod,
                operacao = _pdAptInicioProd.operacao,
                codfuncionario = _pdAptTerminoProd.codfuncionario,
                quantidade = _pdAptTerminoProd.quantidade + _pdAptTerminoProd.qtdrefugo,
                dtinicio = _pdAptInicioProd.dtinicio,
                dttermino = _pdAptTerminoProd.dttermino,
                turno = null,
                codestrutura = _pdOrdemProd.codestrutura,
                codroteiro = _pdOrdemProd.codroteiro,
                codmaquina = null,
                reginsp = true,
                codmotivo = null
            };
            if (_pdParamEstab.exigeturnoapto)
                _pdLancamentos.turno = string.IsNullOrEmpty(_geFuncionarios.turno) ? _pdParamEstab.turnopadrao : _geFuncionarios.turno;

            if (_pdParamEstab.exigemaqapto)
            {
                var OperacoesOP = await BuscaOperacoesAptOp(_pdOrdemProd.codordprod, _pdOrdemProd.nivelordprod, Globals.CodEstab);

                if (OperacoesOP.Count > 1)
                    _pdLancamentos.codmaquina = _pdParamEstab.codmaqpadrao;
                else
                    _pdLancamentos.codmaquina = (await BuscaMaqPrimeiraOper()).codmaquina;
            }

            _IPDApontamentoProdAS.Lancamento = _pdLancamentos;

            var _pdApontamentoProd = new PDApontamentoProdVM
            {
                codestab = Globals.CodEstab,
                datafechamento = await _IPDApontamentoProdAS.BuscaDataFechamentoAsync(Globals.CodEstab),
                codproduto = _fsProdutoParamEstab.codproduto,
                codsucata = _fsProdutoPcp.codsucata,
                codunidade = _fsProdutoParamEstab.codunidade,
                rastreabilidade = _fsProdutoPcp.rastreabilidade,
                apontaProcesso = _pdOrdemProdRotinas.aptoprocesso,
                aptoAcabado = _pdOrdemProdRotinas.aptoacabado,
                codusuario = _geFuncionarios.codusuario
            };

            _IPDApontamentoProdAS.PDApontamentoProd = _pdApontamentoProd;
            _IPDApontamentoProdAS.ParamEstab = _pdParamEstab;

            if (!_IPDApontamentoProdAS.Validate(_pdOrdemProdRotinas))
            {
                retorno = TermConsole.ExibeMensagemLine(_IPDApontamentoProdAS.Errors.ToMessage(), ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
                return retorno;
            }

            var _pdApontLancRefugo = new PDApontLancRefugoVM();
            _pdApontLancRefugo.codestab = Globals.CodEstab;
            _pdApontLancRefugo.codordprod = _pdLancamentos.codordprod;
            _pdApontLancRefugo.nivelordprod = _pdLancamentos.nivelordprod;
            _pdApontLancRefugo.codestrutura = _pdLancamentos.codestrutura;
            _pdApontLancRefugo.codproduto = _pdOrdemProd.codproduto;
            _pdApontLancRefugo.qtdApontamento = (decimal)_pdLancamentos.quantidade;
            _pdApontLancRefugo.operacao = _pdLancamentos.operacao;
            _pdApontLancRefugo.codroteiro = _pdLancamentos.codroteiro;

            _pdApontLancRefugo.qtdTotReaproveitada = 0;
            _pdApontLancRefugo.qtdTotSucata = ((_pdAptTerminoProd.qtdrefugo) * (decimal)_fsProdutoPcp.pesoliquido);
            _pdApontLancRefugo.qtdTotRefugada = (_pdAptTerminoProd.qtdrefugo);

            var dt = new DataTable();
            dt.Columns.Add("CODREFUGO", typeof(Guid));
            dt.Columns.Add("CODMOTIVO", typeof(int));
            dt.Columns.Add("DESCRICAO", typeof(string));
            dt.Columns.Add("QTDESUCATA", typeof(decimal));
            dt.Columns.Add("QTDEREFUGO", typeof(decimal));
            dt.Columns.Add("QTDEREAPROVEITADA", typeof(decimal));
            dt.Columns.Add("QTDECONSUMIDA", typeof(decimal));
            dt.Columns.Add("CODCOMPONENTE", typeof(string));
            dt.Columns.Add("DESCRICAOCOMPONENTE", typeof(string));

            if (_pdAptTerminoProd.qtdrefugo > 0)
            {
                if (!string.IsNullOrEmpty(_pdParamEstab.codmotrefugo.ToString()))
                {
                    _pdAptTerminoProd.codmotivo = _pdParamEstab.codmotrefugo;
                    var drItem = dt.NewRow();

                    drItem["CODREFUGO"] = Guid.NewGuid();
                    drItem["CODMOTIVO"] = _pdParamEstab.codmotrefugo ?? _pdMotivos.codmotivo;
                    drItem["DESCRICAO"] = " ";
                    drItem["QTDESUCATA"] = _pdApontLancRefugo.qtdTotSucata;
                    drItem["QTDEREFUGO"] = _pdApontLancRefugo.qtdTotRefugada;
                    drItem["QTDEREAPROVEITADA"] = 0;
                    dt.Rows.Add(drItem);
                }
                else
                {
                    foreach (var item in _lstpdRefugoMot)
                    {
                        var drItem = dt.NewRow();

                        drItem["CODREFUGO"] = Guid.NewGuid();
                        drItem["CODMOTIVO"] = item.codmotivo;
                        drItem["DESCRICAO"] = " ";
                        drItem["QTDESUCATA"] = item.qtdrefugada * (decimal)_fsProdutoPcp.pesoliquido;
                        drItem["QTDEREFUGO"] = item.qtdrefugada;
                        drItem["QTDEREAPROVEITADA"] = 0;
                        dt.Rows.Add(drItem);
                    }
                }

                await CarregaItensReap(dt, _pdApontLancRefugo, _lstpdRefugoMot);
            }

            _pdApontamentoProd.dtRefugos = dt;
            _pdApontamentoProd.qtdTotReaproveitada = _pdApontLancRefugo.qtdTotReaproveitada;
            _pdApontamentoProd.qtdTotRefugada = _pdApontLancRefugo.qtdTotRefugada;
            _pdApontamentoProd.qtdTotSucata = _pdApontLancRefugo.qtdTotSucata;
            _pdApontamentoProd.qtdApontamento = _pdLancamentos.quantidade;


            _IPDApontamentoProdAS.PDApontamentoProd = _pdApontamentoProd;
            
            //Executa Validação do apontamento de produção
            if (!await _IPDApontamentoProdAS.ValidateApontamentoAsync())
            {
                var msErro = _IPDApontamentoProdAS.Errors.ToMessage();
                if (!(msErro.IndexOf("Componente(s) com saldo insuficiente") > -1))
                {
                    retorno = TermConsole.ExibeMensagemLine(msErro, ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
                    return retorno;
                }

                var ComponentesNec = await _IPDOrdemProdCompAS.SelectSaldoEstoqueNec(
                    _pdLancamentos.codestab, _pdLancamentos.quantidade, _pdLancamentos.codordprod,
                    _pdLancamentos.nivelordprod, _pdLancamentos.codestrutura);
                var CompHasNoBalance = ComponentesNec.Where(r => r.statussaldo == "SALDO INSUFICIENTE");
                TermConsole.ExibeMensagemLine(Infra.Cross.Resources.ValidationMessagesResource.ComponentsHasNoBalance, ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);

                foreach (var item in CompHasNoBalance)
                {
                    TermConsole.ExibeMensagem(item.codcomponente + ", ");
                }

                TermConsole.ExibeMensagemLine("");

                retorno = ETiposApontamento.TERMINOPRODUCAO.PECASBOAS;

                return retorno;
            }

            var Transaction = await _IPDAptTerminoProdAS.BeginTransactionAsync();

            var ret = await _IPDApontamentoProdAS.SalvarLancamentoAsync();

            if (ret == EEstadoAtualApontamento.ERRO)
            {
                await Transaction.RollbackAsync();
                retorno = TermConsole.ExibeMensagem(_IPDApontamentoProdAS.Errors.ToMessage(), ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
                return retorno;
            }

            if (ret == EEstadoAtualApontamento.SELECIONALOTE)
                await _IPDApontamentoProdAS.SelecionaLotesAppAsync();

            if (_IPDApontamentoProdAS.Errors.Any())
            {
                await Transaction.RollbackAsync();
                retorno = TermConsole.ExibeMensagemLine(_IPDApontamentoProdAS.Errors.ToMessage(), ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
                return retorno;
            }

            await _IPDApontamentoProdAS.LancMovKardexAsync();

            if (_IPDApontamentoProdAS.Errors.Any())
            {
                await Transaction.RollbackAsync();
                retorno = TermConsole.ExibeMensagemLine(_IPDApontamentoProdAS.Errors.ToMessage(), ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
                return retorno;
            }

            if (ret != EEstadoAtualApontamento.ERRO)
            {
                _pdOrdemProdRotinas.qtderefugo = _pdApontamentoProd.qtdTotRefugada ?? 0;
                _pdOrdemProdRotinas.qtdeproducao = _pdApontamentoProd.qtdApontamento ?? 0;

                _IPDOrdemProdRotinasAS.Update(_pdOrdemProdRotinas);

                // if ()


                //_pdOrdemProdRotinas.qtderefugo += _pdApontamentoProd.qtdTotRefugada;
                //_pdOrdemProdRotinas.qtdeproducao += _pdApontamentoProd.qtdApontamento;
                //_pdOrdemProdRotinas.UpdateProdProcesso(out msgErro);

                //if (string.IsNullOrEmpty(msgErro))
                //{
                //    _pdApontamentoProd.SalvarMotReap(out msgErro);
                //}

                //if (string.IsNullOrEmpty(msgErro))
                //{
                //    //if (!_pdApontamentoProd.apontaProcesso || _pdApontamentoProd.aptoAcabado)
                //    _pdApontamentoProd.MotReapMovKardex(out msgErro, 0);
                //}

                //if (string.IsNullOrEmpty(msgErro))
                //{
                //    if (_pdAptTerminoProd.qtdretrabalho > 0)
                //    {
                //        _pdRetrabalho.codestab = Globals.CodEstab;
                //        _pdRetrabalho.codordprod = _pdLancamentos.codordprod;
                //        _pdRetrabalho.nivelordprod = _pdLancamentos.nivelordprod;
                //        _pdRetrabalho.codestrutura = _pdLancamentos.codestrutura;
                //        _pdRetrabalho.codretrab = _pdRetrabalho.NextCodigo();
                //        _pdRetrabalho.operacao = _pdLancamentos.operacao;
                //        _pdRetrabalho.dtinicio = _pdAptInicioProd.dtinicio;
                //        _pdRetrabalho.dttermino = _pdAptTerminoProd.dttermino;
                //        _pdRetrabalho.codfuncionario = _pdAptTerminoProd.codfuncionario;
                //        _pdRetrabalho.codestoque = _fsProdutoParamEstab.codestoque;
                //        _pdRetrabalho.lote = null;
                //        _pdRetrabalho.quantidade = _pdAptTerminoProd.qtdretrabalho;
                //        _pdRetrabalho.codmotivo = _pdParamEstab.codmotretrab;

                //        _pdRetrabalho.Insert(out msgErro);
                //    }

                //}

                //if (!string.IsNullOrEmpty(msgErro))
                //{
                //    bdCont.RollbackTransacao();
                //    retorno = TermConsole.ExibeMensagem(msgErro, ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
                //}
                //else
                //{
                //    _pdAptTerminoProd.Insert(out msgErro);
                //    _pdAptLancamento.codestab = Globals.CodEstab;
                //    _pdAptLancamento.codfimapt = _pdAptTerminoProd.codfimapt;
                //    _pdAptLancamento.codlancamento = _pdApontamentoProd._pdLancamentos.codlancamento;
                //    _pdAptLancamento.Insert(out msgErro);
                //    if (_pdAptTerminoProd.qtdretrabalho > 0)
                //    {
                //        _pdAptRetrabalho.codfimapt = _pdAptTerminoProd.codfimapt;
                //        _pdAptRetrabalho.codestab = Globals.CodEstab;
                //        _pdAptRetrabalho.codordprod = _pdLancamentos.codordprod;
                //        _pdAptRetrabalho.nivelordprod = _pdLancamentos.nivelordprod;
                //        _pdAptRetrabalho.codestrutura = _pdLancamentos.codestrutura;
                //        _pdAptRetrabalho.codretrab = _pdRetrabalho.codretrab;

                //        _pdAptRetrabalho.Insert(out msgErro);
                //    }
                //}

                //if (!string.IsNullOrEmpty(msgErro))
                //{
                //    bdCont.RollbackTransacao();
                //    retorno = TermConsole.ExibeMensagemLine(msgErro, ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
                //}
                //else
                //{
                //    _pdOrdemProd.refugado = _pdOrdemProd.BuscaTotRefugado(
                //        (decimal)_pdLancamentos.codordprod,
                //        _pdLancamentos.nivelordprod,
                //        Globals.CodEstab);
                //    if (_pdApontamentoProd.aptoAcabado || !_pdApontamentoProd.apontaProcesso)
                //    {
                //        _pdOrdemProd.realizado = _pdOrdemProd.BuscaTotProduzido(
                //            (decimal)_pdLancamentos.codordprod,
                //            _pdLancamentos.nivelordprod,
                //            Globals.CodEstab);
                //    }
                //    _pdOrdemProd.UpdateRealizadoRefugado(out msgErro);
                //}

                //if (!string.IsNullOrEmpty(msgErro))
                //{
                //    bdCont.RollbackTransacao();
                //    retorno = TermConsole.ExibeMensagemLine(msgErro, ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
                //}
                //else
                //{
                //    // Recalcula todos os totais da Ordem
                //    // para evitar que fiquem com diferenças
                //    pckg_producao _pckg_producao = new pckg_producao(Globals.ConnStr, _pdAptInicioProd.codusuario, "", Globals.CodEstab);
                //    _pckg_producao.DefineConexao(bdCont.AbreConn());
                //    _pckg_producao.codestab = _pdOrdemProd.codestab;
                //    _pckg_producao.codordprod = Convert.ToDecimal(_pdOrdemProd.codordprod);
                //    _pckg_producao.nivelordprod = _pdOrdemProd.nivelordprod;
                //    var Erro = "";
                //    _pckg_producao.RecalculaTotaisOrdem(out Erro);

                //    _lstpdRefugoMot.Clear();

                //    bdCont.CommitTransacao();
                //}

                 
                await _IPDOrdemProdRotinasAS.SaveChanges();
                await Transaction.CommitAsync();
            }
            else
            {
                await Transaction.RollbackAsync();
                retorno = TermConsole.ExibeMensagemLine(_IPDApontamentoProdAS.Errors.ToMessage(), ETiposApontamento.TERMINOPRODUCAO.PECASBOAS);
            }
            return retorno;
        }
        #endregion

        #region Carrega itens reaproveitamento
        private async Task CarregaItensReap(DataTable pdt, PDApontLancRefugoVM pPDApontLancRefugo, List<PDRefugoMotVM> pLstpdRefugoMot)
        {
            if (_pdParamEstab.reapautomatico &&
                pPDApontLancRefugo.qtdTotRefugada > 0)
            {
                
                var ComponentesReaproveitaveis = await _IPDOrdemProdCompAS.QueryAsync(r => 
                    r.reaproveita && 
                    r.codestab == Globals.CodEstab &&
                    r.codordprod == pPDApontLancRefugo.codordprod &&
                    r.codestrutura == pPDApontLancRefugo.codestrutura &&
                    r.codroteiro == pPDApontLancRefugo.codroteiro &&
                    r.operacao == pPDApontLancRefugo.operacao
                );

                foreach(var componente in ComponentesReaproveitaveis)
                {
                    var compEstrutura = (await _IENEstComponentesAS.QueryAsync(r =>
                            r.codestab == Globals.CodEstab &&
                            r.codestrutura == componente.codestrutura &&
                            r.codroteiro == componente.codroteiro &&
                            r.operacao == componente.operacao &&
                            r.componente == componente.codcomponente
                        )).FirstOrDefault();
                    if (compEstrutura != null)
                    {
                        var linha = pdt.NewRow();
                        foreach (var item in pLstpdRefugoMot)
                        {
                            linha["CODREFUGO"] = Guid.NewGuid();
                            linha["CODMOTIVO"] = item.codmotivo;
                            linha["CODCOMPONENTE"] = componente.codcomponente;
                            linha["DESCRICAO"] = " ";
                            linha["QTDESUCATA"] = 0;
                            linha["QTDEREFUGO"] = 0;
                            linha["QTDECONSUMIDA"] = 0;
                            linha["QTDEREAPROVEITADA"] = item.qtdrefugada * compEstrutura.quantidade;
                        }

                        pdt.Rows.Add(linha);
                    }
                }

            }
        }
        #endregion

        #endregion
        #endregion

        #region Busca Operações Apt. Op.
        public async Task<List<PDOrdemProdRotinas>> BuscaOperacoesAptOp(long pCodOrdProd, string pNivelOrdProd, int pCodEstab) =>
            await _IPDOrdemProdRotinasAS.BuscaOperacoesAptOpAsync(pCodEstab, pCodOrdProd, pNivelOrdProd);
        #endregion

        #region Busca Maq. Primeira Operação
        public async Task<PDOrdemProdRotinas> BuscaMaqPrimeiraOper() =>
            await _IPDOrdemProdRotinasAS.BuscaMaqOperacaoOpAsync(new PDOrdemProdRotinas
            {
                codestab = Globals.CodEstab,
                codordprod = _pdOrdemProd.codordprod,
                nivelordprod = _pdOrdemProd.nivelordprod,
                codestrutura = _pdOrdemProd.codestrutura,
                codroteiro = _pdOrdemProd.codroteiro,
                operacao = 10
            });
        #endregion

        #region Gerar Apt Input
        /// <summary>
        /// 0, 'INICIO SETUP', 
        /// 1, 'INICIO PRODUÇÃO', 
        /// 2, 'INICIO PARADA', 
        /// 3, 'INICIO MANUTENÇÃO', 
        /// 4, 'TÉRMINO SETUP', 
        /// 5, 'TÉRMINO PRODUÇÃO', 
        /// 6, 'TÉRMINO PARADA', 
        /// 7, 'TÉRMINO MANUTENÇÃO'
        /// </summary>
        /// <param name="pClassificacaoInput">Classificação Input</param>
        private void GerarAptInput(int pClassificacaoInput, int pClassIni, int pClassTerm, string pCodMaquina)
        {
            /*
            pdModulos _pdModulos = new pdModulos(Globals.ConnStr, "", "");
            bancoDados bd = new bancoDados(_OrclConn);

            var maquinasTerminal = _pdModulos.MaquinasModulo(Globals.CodEstab);

            var maq = maquinasTerminal.Select(String.Format("[CODMAQUINA] = '{0}'", pCodMaquina));

            if (maq.Length > 0)
            {

                var reader = bd.ExecConsRetDataReader(@"SELECT PDEVENTOS.CODEVENTO                           
                                                      FROM PDEVENTOS
                                                     WHERE NVL(PDEVENTOS.EVENTOPADRAO, -1) IN (:pEVTINI, :pEVTTERM)",
                          bd.CriaParam("pEVTINI", OracleDbType.Int32, pClassIni),
                          bd.CriaParam("pEVTTERM", OracleDbType.Int32, pClassTerm));
                if (reader.HasRows)
                {
                    reader.Read();
                    var _pdEventos = new pdEventos(Globals.ConnStr, "", "");
                    var _pdInput = new pdInput(Globals.ConnStr, "", "");
                    _pdInput.codestab = Globals.CodEstab;
                    _pdInput.codinput = 0;
                    _pdInput.codmaquina = pCodMaquina;
                    _pdInput.codordprod = _pdOrdemProd.codordprod;
                    _pdInput.nivelordprod = _pdOrdemProd.nivelordprod;
                    _pdInput.codestrutura = _pdOrdemProd.codestrutura;

                    _pdEventos.codevento = Convert.ToDecimal(reader["CODEVENTO"].ToString());
                    _pdEventos.Carrega();


                    _pdInput.codevento = _pdEventos.codevento;
                    _pdInput.data = DateTime.Now;
                    _pdInput.codfuncionario = _geFuncionarios.codfuncionario;
                    _pdInput.datainput = DateTime.Now;
                    _pdInput.qtdpulsos = 0;

                    _pdInput.classificacao = pClassificacaoInput;
                    _pdInput.geestabRazaosocial = "";
                    _pdInput.pdeventosDescricao = "";
                    _pdInput.enmaquinasDescricao = "";

                    string msgErro = "";
                    string cmpErro = "";

                    if (_pdInput.Valida(out msgErro, out cmpErro, false))
                    {
                        pdSensores _pdSensores = new pdSensores(Globals.ConnStr, "", "", Globals.CodEstab);
                        pckg_producao _pckg_producao = new pckg_producao(Globals.ConnStr, "", "", Globals.CodEstab);

                        if (pClassificacaoInput.In(0, 1, 2, 3))
                        {
                            _pckg_producao.codestab = Globals.CodEstab;
                            _pckg_producao.codmaquina = _pdInput.codmaquina;
                            _pckg_producao.classificacao = _pdInput.classificacao;
                            _pckg_producao.codfuncionario = _pdInput.codfuncionario;
                            _pckg_producao.datainput = _pdInput.data;
                            _pckg_producao.qtdpulsos = _pdInput.qtdpulsos;
                            _pckg_producao.aptoInput(out msgErro);
                        }

                        _pdInput.codinput = _pdInput.NextCodInput();
                        _pdInput.Insert(out msgErro);


                        if (_pdSensores.CarregaSensorMaq(_pdInput.codmaquina, Globals.CodEstab))
                        {
                            pdLogSensores _pdLogSensores = new pdLogSensores(Globals.ConnStr, "", "");
                            pdLogSensores _pdLogSensoresAnt = new pdLogSensores(Globals.ConnStr, "", "");
                            _pdLogSensores.codestab = Globals.CodEstab;
                            _pdLogSensores.codmodulo = _pdSensores.codmodulo;
                            _pdLogSensores.codsensor = _pdSensores.codsensor;

                            _pdLogSensoresAnt.codestab = Globals.CodEstab;
                            _pdLogSensoresAnt.codmodulo = _pdSensores.codmodulo;
                            _pdLogSensoresAnt.codsensor = _pdSensores.codsensor;
                            _pdLogSensoresAnt.carregaUltLog();

                            _pdLogSensores.statusrele = "False";
                            _pdLogSensores.codevento = _pdInput.codevento;

                            _pdLogSensores.estadoinput = _pdEventos.classificacao == 0 ? "TRABALHANDO" : "MAQUINA PARADA";

                            _pdLogSensores.statuscon = "CONECTADO";
                            _pdLogSensores.cicloreal = 0;
                            _pdLogSensores.datainiciociclo = DateTime.Today;
                            _pdLogSensores.dataultciclo = DateTime.Today;
                            _pdLogSensores.qtdbatidas = _pdInput.qtdpulsos;
                            _pckg_producao.InsereLogSensor(_pdLogSensores, _pdLogSensoresAnt.estadoinput);
                        }

                    }
                }
                reader.Close();
                reader.Dispose();
            }

            maquinasTerminal.Dispose();
            */
        }
        #endregion

    }
}

