﻿using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public class PDLancamentosAS : BaseAppService<PDLancamentos>, IPDLancamentosAS
    {
        private readonly IPDLancamentosService _IPDLancamentosService;

        public PDLancamentosAS(IPDLancamentosService pIPDLancamentosService) :
             base(pIPDLancamentosService)
        {
            _IPDLancamentosService = pIPDLancamentosService;
        }
    }
}
