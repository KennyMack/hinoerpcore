﻿using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public class PDAptRetrabalhoAS : BaseAppService<PDAptRetrabalho>, IPDAptRetrabalhoAS
    {
        private readonly IPDAptRetrabalhoService _IPDAptRetrabalhoService;

        public PDAptRetrabalhoAS(IPDAptRetrabalhoService pIPDAptRetrabalhoService) :
             base(pIPDAptRetrabalhoService)
        {
            _IPDAptRetrabalhoService = pIPDAptRetrabalhoService;
        }
    }
}
