﻿using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public class PDAptTerminoProdAS : BaseAppService<PDAptTerminoProd>, IPDAptTerminoProdAS
    {
        private readonly IPDAptTerminoProdService _IPDAptTerminoProdService;

        public PDAptTerminoProdAS(IPDAptTerminoProdService pIPDAptTerminoProdService) :
             base(pIPDAptTerminoProdService)
        {
            _IPDAptTerminoProdService = pIPDAptTerminoProdService;
        }

        public async Task<bool> WasFinishedAsync(PDAptInicioProd pAptInicio) =>
            await _IPDAptTerminoProdService.WasFinishedAsync(pAptInicio);
    }
}
