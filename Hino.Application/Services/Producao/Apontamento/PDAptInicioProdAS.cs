﻿using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public class PDAptInicioProdAS : BaseAppService<PDAptInicioProd>, IPDAptInicioProdAS
    {
        private readonly IPDAptInicioProdService _IPDAptInicioProdService;
        private readonly IPDAptTerminoProdService _IPDAptTerminoProdService;

        public PDAptInicioProdAS(IPDAptInicioProdService pIPDAptInicioProdService,
            IPDAptTerminoProdService pIPDAptTerminoProdService) :
             base(pIPDAptInicioProdService)
        {
            _IPDAptInicioProdService = pIPDAptInicioProdService;
            _IPDAptTerminoProdService = pIPDAptTerminoProdService;
        }

        public async Task<PDAptInicioProd> GetLastAptAsync(PDAptInicioProd pAptInicio) =>
            await _IPDAptInicioProdService.GetLastAptAsync(pAptInicio);

        public async Task<bool> WasStartedAsync(PDAptInicioProd pAptInicio) =>
            await _IPDAptInicioProdService.WasStartedAsync(pAptInicio);
    }
}
