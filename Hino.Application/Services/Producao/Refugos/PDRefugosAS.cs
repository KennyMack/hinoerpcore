﻿using Hino.Domain.Producao.Interfaces.Services.Refugos;
using Hino.Infra.Cross.Entities.Producao.Refugos;

namespace Hino.Application.Interfaces.Producao.Refugos
{
    public class PDRefugosAS : BaseAppService<PDRefugos>, IPDRefugosAS
    {
        private readonly IPDRefugosService _IPDRefugosService;

        public PDRefugosAS(IPDRefugosService pIPDRefugosService) :
             base(pIPDRefugosService)
        {
            _IPDRefugosService = pIPDRefugosService;
        }
    }
}
