﻿using Hino.Domain.Producao.Interfaces.Services.Refugos;
using Hino.Infra.Cross.Entities.Producao.Refugos;

namespace Hino.Application.Interfaces.Producao.Refugos
{
    public class PDRefugoReapAS : BaseAppService<PDRefugoReap>, IPDRefugoReapAS
    {
        private readonly IPDRefugoReapService _IPDRefugoReapService;

        public PDRefugoReapAS(IPDRefugoReapService pIPDRefugoReapService) :
             base(pIPDRefugoReapService)
        {
            _IPDRefugoReapService = pIPDRefugoReapService;
        }
    }
}
