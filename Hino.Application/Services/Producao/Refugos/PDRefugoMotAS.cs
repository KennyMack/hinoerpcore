﻿using Hino.Domain.Producao.Interfaces.Services.Refugos;
using Hino.Infra.Cross.Entities.Producao.Refugos;

namespace Hino.Application.Interfaces.Producao.Refugos
{
    public class PDRefugoMotAS : BaseAppService<PDRefugoMot>, IPDRefugoMotAS
    {
        private readonly IPDRefugoMotService _IPDRefugoMotService;

        public PDRefugoMotAS(IPDRefugoMotService pIPDRefugoMotService) :
             base(pIPDRefugoMotService)
        {
            _IPDRefugoMotService = pIPDRefugoMotService;
        }
    }
}
