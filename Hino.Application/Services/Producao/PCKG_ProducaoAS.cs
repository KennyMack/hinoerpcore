﻿using Hino.Application.Interfaces.Producao;
using Hino.Domain.Producao.Interfaces.Services;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Application.Services.Producao
{
    public class PCKG_ProducaoAS : BaseAppPackage, IPCKG_ProducaoAS
    {
        private readonly IPCKG_Producao _IPCKG_Producao;

        public PCKG_ProducaoAS(IPCKG_Producao pIPCKG_Producao) :
             base(pIPCKG_Producao)
        {
            _IPCKG_Producao = pIPCKG_Producao;
        }

        public async Task<bool> ApontLotesFifoAsync(PDLancamentos pLancamento, string pCodUsuario, bool pMovimentaEstoque) =>
            await _IPCKG_Producao.ApontLotesFifoAsync(pLancamento, pCodUsuario, pMovimentaEstoque);

        public async Task<bool> AptoProducaoAsync(PDLancamentos pLancamento, string pCodUsuario, decimal pQtdrefugo) =>
            await _IPCKG_Producao.AptoProducaoAsync(pLancamento, pCodUsuario, pQtdrefugo);

        public async Task<bool> CheckUserPerLocEstAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, long pCodFuncionario) =>
            await _IPCKG_Producao.CheckUserPerLocEstAsync(pCodEstab, pCodOrdProd, pNivelOrdProd, pCodEstrutura, pCodFuncionario);

        public async Task<bool> GetValidaOpAptoAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, short pOperacao, decimal pQuantidade) =>
            await _IPCKG_Producao.GetValidaOpAptoAsync(pCodEstab, pCodOrdProd, pNivelOrdProd, pCodEstrutura, pOperacao, pQuantidade);

        public async Task<bool> GetValidaOpAptoSldDtlancAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, DateTime pDtLanc, decimal pQuantidade) =>
            await _IPCKG_Producao.GetValidaOpAptoSldDtlancAsync(pCodEstab, pCodOrdProd, pNivelOrdProd, pCodEstrutura, pDtLanc, pQuantidade);

        public async Task<bool> LancConsumoProcessoAsync(PDLancamentos pLancamento, decimal pQtdRefugo, string pCodUsuario) =>
            await _IPCKG_Producao.LancConsumoProcessoAsync(pLancamento, pQtdRefugo, pCodUsuario);

        public async Task<bool> MovRefugoOperacaoAsync(int pCodEstab, long pCodLancamento, long pCodRefugo, decimal pQtdRefugo, string pCodUsuario) =>
            await _IPCKG_Producao.MovRefugoOperacaoAsync(pCodEstab, pCodLancamento, pCodRefugo, pQtdRefugo, pCodUsuario);

        public async Task<bool> RecalculaTotaisOrdemAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd) =>
            await _IPCKG_Producao.RecalculaTotaisOrdemAsync(pCodEstab, pCodOrdProd, pNivelOrdProd);

        public async Task<bool> VerificaOperacoesAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, short pOperacao, decimal pQuantidade) =>
            await _IPCKG_Producao.VerificaOperacoesAsync(pCodEstab, pCodOrdProd, pNivelOrdProd, pOperacao, pQuantidade);
    }
}
