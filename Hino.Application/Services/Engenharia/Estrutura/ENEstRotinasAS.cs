using Hino.Aplication.Interfaces.Engenharia.Estrutura;
using Hino.Application.Interfaces;
using Hino.Domain.Engenharia.Interfaces.Services.Estrutura;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;

namespace Hino.Aplication.Services.Engenharia.Estrutura
{
    public class ENEstRotinasAS : BaseAppService<ENEstRotinas>, IENEstRotinasAS
    {
        private readonly IENEstRotinasService _IENEstRotinasService;

        public ENEstRotinasAS(IENEstRotinasService pIENEstRotinasService) : 
             base(pIENEstRotinasService)
        {
            _IENEstRotinasService = pIENEstRotinasService;
        }
    }
}
