using Hino.Aplication.Interfaces.Engenharia.Estrutura;
using Hino.Application.Interfaces;
using Hino.Domain.Engenharia.Interfaces.Services.Estrutura;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;

namespace Hino.Aplication.Services.Engenharia.Estrutura
{
    public class ENEstComponentesAS : BaseAppService<ENEstComponentes>, IENEstComponentesAS
    {
        private readonly IENEstComponentesService _IENEstComponentesService;

        public ENEstComponentesAS(IENEstComponentesService pIENEstComponentesService) : 
             base(pIENEstComponentesService)
        {
            _IENEstComponentesService = pIENEstComponentesService;
        }
    }
}
