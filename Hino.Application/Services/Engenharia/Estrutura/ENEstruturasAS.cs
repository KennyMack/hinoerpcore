using Hino.Aplication.Interfaces.Engenharia.Estrutura;
using Hino.Application.Interfaces;
using Hino.Domain.Engenharia.Interfaces.Services.Estrutura;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;

namespace Hino.Aplication.Services.Engenharia.Estrutura
{
    public class ENEstruturasAS : BaseAppService<ENEstruturas>, IENEstruturasAS
    {
        private readonly IENEstruturasService _IENEstruturasService;

        public ENEstruturasAS(IENEstruturasService pIENEstruturasService) : 
             base(pIENEstruturasService)
        {
            _IENEstruturasService = pIENEstruturasService;
        }
    }
}
