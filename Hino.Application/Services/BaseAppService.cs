﻿using Hino.Domain.Base.Interfaces.Services;
using Hino.Infra.Cross.Entities.Interfaces;
using Hino.Infra.Cross.Utils.Exceptions;
using Hino.Infra.Cross.Utils.Paging;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces
{
    public class BaseAppService<T> : IBaseAppService<T>, IDisposable where T : class
    {
        private readonly IBaseService<T> _BaseService;
        public List<ModelException> Errors
        {
            get
            {
                return _BaseService.Errors;
            }
            set
            {
                _BaseService.Errors = value;
            }
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync() =>
            await _BaseService.BeginTransactionAsync();

        public BaseAppService(IBaseService<T> baseService)
        {
            _BaseService = baseService;
        }

        public virtual T Add(T model) =>
            _BaseService.Add(model);

        public void Dispose()
        {
            _BaseService.Dispose();
            // GC.SuppressFinalize(this);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.GetAllAsync(includeProperties);

        public virtual async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.GetAllPagedAsync(page, pageSize, includeProperties);

        public virtual T GetByWhereForUpdate(Expression<Func<T, bool>> predicate) =>
            _BaseService.GetByWhereForUpdate(predicate);

        public virtual T GetFirstByWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            _BaseService.GetFirstByWhere(predicate, includeProperties);

        public virtual long NextSequence() =>
            _BaseService.NextSequence();

        public virtual async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.QueryAsync(predicate, includeProperties);

        public virtual async Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.QueryPagedAsync(page, pageSize, predicate, includeProperties);

        public virtual T Remove(T model) =>
            _BaseService.Remove(model);

        public virtual T RemoveByWhere(Expression<Func<T, bool>> predicate) =>
            _BaseService.RemoveByWhere(predicate);

        public virtual async Task<int> SaveChanges() =>
            await _BaseService.SaveChanges();

        public virtual T Update(T model) =>
            _BaseService.Remove(model);
    }
}
