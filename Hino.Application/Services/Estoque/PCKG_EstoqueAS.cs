﻿using Hino.Application.Interfaces.Estoque;
using Hino.Application.Interfaces.Fiscal.Estoque;
using Hino.Domain.Estoque.Interfaces.Services;
using Hino.Infra.Cross.Entities.Estoque;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Application.Services.Estoque
{
    public class PCKG_EstoqueAS : BaseAppPackage, IPCKG_EstoqueAS
    {
        private readonly IPCKG_Estoque _IPCKG_Estoque;

        public PCKG_EstoqueAS(IPCKG_Estoque pIPCKG_Estoque) :
             base(pIPCKG_Estoque)
        {
            _IPCKG_Estoque = pIPCKG_Estoque;
        }

        public async Task<DateTime> BuscaDataFechamentoAsync(int pCodEstab) =>
            await _IPCKG_Estoque.BuscaDataFechamentoAsync(pCodEstab);

        public async Task<bool> GeraKardexAsync(ESKardex pKardex) =>
            await _IPCKG_Estoque.GeraKardexAsync(pKardex);

        public async Task<bool> MovimentaSaldoLoteAsync(ESLoteSaldo pLoteSaldo, decimal pQuantidade, string pMovimento) =>
            await _IPCKG_Estoque.MovimentaSaldoLoteAsync(pLoteSaldo, pQuantidade, pMovimento);
    }
}
