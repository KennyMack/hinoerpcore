﻿using Hino.Domain.Estoque.Interfaces.Services;
using Hino.Infra.Cross.Entities.Estoque;

namespace Hino.Application.Interfaces.Estoque
{
    public class ESKardexAS : BaseAppService<ESKardex>, IESKardexAS
    {
        private readonly IESKardexService _IESKardexService;

        public ESKardexAS(IESKardexService pIESKardexService) :
             base(pIESKardexService)
        {
            _IESKardexService = pIESKardexService;
        }
    }
}
