﻿using Hino.Domain.Estoque.Interfaces.Services.Lote;
using Hino.Infra.Cross.Entities.Estoque.Lote;

namespace Hino.Application.Interfaces.Estoque.Lote
{
    public class ESLoteSaldoAS : BaseAppService<ESLoteSaldo>, IESLoteSaldoAS
    {
        private readonly IESLoteSaldoService _IESLoteSaldoService;

        public ESLoteSaldoAS(IESLoteSaldoService pIESLoteSaldoService) :
             base(pIESLoteSaldoService)
        {
            _IESLoteSaldoService = pIESLoteSaldoService;
        }
    }
}
