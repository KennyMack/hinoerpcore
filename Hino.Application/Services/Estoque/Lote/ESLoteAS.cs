﻿using Hino.Domain.Estoque.Interfaces.Services.Lote;
using Hino.Infra.Cross.Entities.Estoque.Lote;

namespace Hino.Application.Interfaces.Estoque.Lote
{
    public class ESLoteAS : BaseAppService<ESLote>, IESLoteAS
    {
        private readonly IESLoteService _IESLoteService;

        public ESLoteAS(IESLoteService pIESLoteService) : 
             base(pIESLoteService)
        {
            _IESLoteService = pIESLoteService;
        }
    }
}
