﻿using Hino.Domain.Fiscal.Interfaces.Services.Estoque;
using Hino.Infra.Cross.Entities.Fiscal.Estoque;

namespace Hino.Application.Interfaces.Fiscal.Estoque
{
    public class FSSaldoEstoqueAS : BaseAppService<FSSaldoEstoque>, IFSSaldoEstoqueAS
    {
        private readonly IFSSaldoEstoqueService _IFSSaldoEstoqueService;

        public FSSaldoEstoqueAS(IFSSaldoEstoqueService pIFSSaldoEstoqueService) :
             base(pIFSSaldoEstoqueService)
        {
            _IFSSaldoEstoqueService = pIFSSaldoEstoqueService;
        }
    }
}
