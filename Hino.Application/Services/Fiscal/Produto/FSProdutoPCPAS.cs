﻿using Hino.Domain.Fiscal.Interfaces.Services.Produto;
using Hino.Infra.Cross.Entities.Fiscal.Produto;

namespace Hino.Application.Interfaces.Fiscal.Produto
{
    public class FSProdutoPCPAS : BaseAppService<FSProdutoPCP>, IFSProdutoPCPAS
    {
        private readonly IFSProdutoPCPService _IFSProdutoPCPService;

        public FSProdutoPCPAS(IFSProdutoPCPService pIFSProdutoPCPService) :
             base(pIFSProdutoPCPService)
        {
            _IFSProdutoPCPService = pIFSProdutoPCPService;
        }
    }
}
