﻿using Hino.Domain.Fiscal.Interfaces.Services.Produto;
using Hino.Infra.Cross.Entities.Fiscal.Produto;

namespace Hino.Application.Interfaces.Fiscal.Produto
{
    public class FSProdutoParamEstabAS : BaseAppService<FSProdutoParamEstab>, IFSProdutoParamEstabAS
    {
        private readonly IFSProdutoParamEstabService _IFSProdutoParamEstabService;

        public FSProdutoParamEstabAS(IFSProdutoParamEstabService pIFSProdutoParamEstabService) :
             base(pIFSProdutoParamEstabService)
        {
            _IFSProdutoParamEstabService = pIFSProdutoParamEstabService;
        }
    }
}
