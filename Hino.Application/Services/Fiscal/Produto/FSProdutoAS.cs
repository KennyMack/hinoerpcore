﻿using Hino.Domain.Fiscal.Interfaces.Services.Produto;
using Hino.Infra.Cross.Entities.Fiscal.Produto;

namespace Hino.Application.Interfaces.Fiscal.Produto
{
    public class FSProdutoAS : BaseAppService<FSProduto>, IFSProdutoAS
    {
        private readonly IFSProdutoService _IFSProdutoService;

        public FSProdutoAS(IFSProdutoService pIFSProdutoService) :
             base(pIFSProdutoService)
        {
            _IFSProdutoService = pIFSProdutoService;
        }
    }
}
