﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application.ConsoleUtils
{
    public static class ETiposApontamento
    {
        public enum TECLADORESULT
        {
            INVALIDO = 0,
            VALIDO = 1,
            ENTER = 2,
            BACKSPACE = 3,
            DELETE = 4,
            COMMA = 5
        }

        public enum OPERACAO
        {
            INICIO = 0,
            INICIOPREPARACAO = 1,
            TERMINOPREPARACAO = 2,
            INICIOPRODUCAO = 3,
            TERMINOPRODUCAO = 4,
            INICIOTROCASERVICO = 5,
            TERMINOTROCASERVICO = 6,
            INICIOMANUTENCAO = 7,
            TERMINOMANUTENCAO = 8,
            INICIOCONSULTADESENHO = 9,
            TERMINOCONSULTADESENHO = 10,
            INICIORETRABALHO = 11,
            TERMINORETRABALHO = 12,
            APONTSUCATA = 13,
            TERMINOTURNO = 14,


            PROCESSANDO = 998,
            CONCLUIDO = 999,
        }

        public enum INICIO
        {
            INICIO = 0,
            USUARIO = 1,
            DIGUSUARIO = 2,
            TIPO = 3,
            DIGTIPO = 4,
            APONTAMENTO = 5,
            SAIR = 6,
            OPERACOES = 7,
            DIGOPERACOES = 8
        }

        public enum INICIOPRODUCAO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICAINICIOOPERACAO = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9

        }

        public enum TERMINOPRODUCAO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICATERMINOOPERACAO = 4,
            PECASBOAS = 5,
            DIGPECASBOAS = 6,

            PECASREFUGO = 7,
            DIGPECASREFUGO = 8,

            MOTREFUGO = 9,
            DIGMOTREFUGO = 10,

            PECASRETRABALHO = 11,
            DIGPECASRETRABALHO = 12,

            SALVAR = 13,
            DIGSALVAR = 14,
            SAIR = 15,
            CONCLUIDO = 16,
            CANCELADO = 17,

            MOTREFUGOQTD = 18,
            DIGMOTIVOREFUGOQTD = 19,

            CODMOTREFUGOQTD = 20,
            DIGCODMOTIVOREFUGOQTD = 21
        }

        public enum INICIOPREPARACAO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICAINICIOPREP = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        public enum TERMINOPREPARACAO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICATERMINOPREP = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        public enum INICIORETRABALHO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICAINICIORETRABALHO = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        public enum TERMINORETRABALHO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICATERMINORETRABALHO = 4,
            QUANTIDADE = 5,
            DIGQUANTIDADE = 6,

            SALVAR = 7,
            DIGSALVAR = 8,
            SAIR = 9,
            CONCLUIDO = 10,
            CANCELADO = 11
        }

        public enum INICIOTROCASERVICO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICAINICIOSERV = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        public enum TERMINOTROCASERVICO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICATERMINOSERV = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        public enum INICIOMANUTENCAO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICAINICIOMANUT = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        public enum TERMINOMANUTENCAO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICATERMINOMANUT = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        public enum INICIOCONSULTADESENHO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICAINICIODESENHO = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        public enum TERMINOCONSULTADESENHO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICATERMINODESENHO = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }

        #region Convert Operacao
        public static OPERACAO ToOperacao(this int Operacao)
        {
            switch (Operacao)
            {
                case 1:
                    return OPERACAO.INICIOPREPARACAO;
                case 2:
                    return OPERACAO.TERMINOPREPARACAO;
                case 3:
                    return OPERACAO.INICIOPRODUCAO;
                case 4:
                    return OPERACAO.TERMINOPRODUCAO;
                case 5:
                    return OPERACAO.INICIOTROCASERVICO;
                case 6:
                    return OPERACAO.TERMINOTROCASERVICO;
                case 7:
                    return OPERACAO.INICIOMANUTENCAO;
                case 8:
                    return OPERACAO.TERMINOMANUTENCAO;
                case 9:
                    return OPERACAO.INICIOCONSULTADESENHO;
                case 10:
                    return OPERACAO.TERMINOCONSULTADESENHO;
                case 11:
                    return OPERACAO.INICIORETRABALHO;
                case 12:
                    return OPERACAO.TERMINORETRABALHO;
                case 13:
                    return OPERACAO.APONTSUCATA;
                case 14:
                    return OPERACAO.TERMINOTURNO;
                case 998:
                    return OPERACAO.PROCESSANDO;
                case 999:
                    return OPERACAO.CONCLUIDO;
                default:
                    return OPERACAO.INICIO;
            }
        }
        #endregion


        public enum TERMINOTURNO
        {
            ORDEMPROD = 0,
            DIGORDEMPROD = 1,
            OPERACAO = 2,
            DIGOPERACAO = 3,
            VERIFICATERMINOOPERACAO = 4,
            SALVAR = 5,
            DIGSALVAR = 6,
            SAIR = 7,
            CONCLUIDO = 8,
            CANCELADO = 9
        }
    }
}
