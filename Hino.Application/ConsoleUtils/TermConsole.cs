﻿using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.Cross.Entities.Producao;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application.ConsoleUtils
{
    public static class TermConsole
    {
        public static void ExibeMensagemLine(string pMessage)
        {
            Console.WriteLine(pMessage);
        }
        public static T ExibeMensagemLine<T>(string pMessage, T pProximoEstado) where T : struct, IConvertible
        {
            Console.WriteLine(pMessage);

            return pProximoEstado;
        }

        public static T ExibeMensagem<T>(string pMessage, T pProximoEstado) where T : struct, IConvertible
        {
            Console.Write(pMessage);
            return pProximoEstado;
        }

        public static void ExibeMensagem(string pMessage)
        {
            Console.Write(pMessage);
        }

        #region Imprimir Cabeçalho
        public static void ImprimirCabecalho()
        {
            var versao = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            var numCharTot = 59;
            var numChar = (numCharTot - versao.Length) - 1;

            Console.WriteLine("#################################################################");
            Console.WriteLine("###                         HINO SISTEMAS                     ###");
            Console.Write("###");

            for (int i = 0; i < numChar; i++)
            {
                if (i == Math.Abs(numCharTot / 2) - Math.Abs(versao.Length / 2))
                {
                    Console.Write($" v{versao}");
                }
                else
                    Console.Write(" ");
            }

            Console.WriteLine("###");
            Console.WriteLine("#################################################################");
            Console.WriteLine("");
        }
        #endregion

        #region Imprimir Operações
        public static void ImprimirOperacoes(PDParamEstab _pdParamEstab, GEFuncionarios pgeFuncionarios)
        {
            Console.WriteLine("SELECIONE A OPERACAO");

            if (_pdParamEstab.aptsetup || _pdParamEstab.aptproducao)
            {
                Console.WriteLine("{0}{1}{2}",
                    _pdParamEstab.aptsetup && (pgeFuncionarios.aptsetup) ? "1  - INICIO SETUP          2  - TERMINO SETUP" : "",
                    _pdParamEstab.aptsetup && (pgeFuncionarios.aptsetup) && _pdParamEstab.aptproducao && (pgeFuncionarios.aptproducao) ? "             " : "",
                    _pdParamEstab.aptproducao && (pgeFuncionarios.aptproducao) ? "3  - INICIO PRODUCAO     4  - TERMINO PRODUCAO" : ""
                    );
            }

            if (_pdParamEstab.aptproducao && (bool)pgeFuncionarios.aptproducao)
                Console.WriteLine("{0}",
                    "14 - TERMINO DE TURNO"
                    );

            if (_pdParamEstab.aptmanut || _pdParamEstab.apttrocaserv)
            {
                Console.WriteLine("{0}{1}{2}",
                    _pdParamEstab.apttrocaserv && (pgeFuncionarios.apttrocaserv) ? "5  - INICIO TROCA SERV.    6  - TERMINO  TROCA SERV." : "",
                    _pdParamEstab.aptmanut && (pgeFuncionarios.aptmanut) && _pdParamEstab.apttrocaserv && (pgeFuncionarios.apttrocaserv) ? "      " : "",
                    _pdParamEstab.aptmanut && (pgeFuncionarios.aptmanut) ? "7  - INICIO MANUTENCAO   8  - TERMINO MANUTENCAO" : ""
                    );
            }

            if (_pdParamEstab.aptconsdesenho || _pdParamEstab.aptretrab)
            {
                Console.WriteLine("{0}{1}{2}",
                _pdParamEstab.aptconsdesenho && (pgeFuncionarios.aptconsdesenho) ? "9  - INICIO CONS.DESENHO  10 - TERMINO  CONS.DESENHO." : "",
                _pdParamEstab.aptconsdesenho && (pgeFuncionarios.aptconsdesenho) && _pdParamEstab.aptretrab && (pgeFuncionarios.aptretrab) ? "  " : "",
                _pdParamEstab.aptretrab && (pgeFuncionarios.aptretrab) ? "11 - INICIO RETRABALHO  12 - TERMINO RETRABALHO" : ""
                );
            }

        }
        #endregion

        #region Imprimir Operações
        public static void ImprimirUsuario(GEFuncionarios pgeFuncionarios)
        {
            Console.WriteLine(string.Format("Usuário: {0} - {1}", pgeFuncionarios.codfuncionario, pgeFuncionarios.GEPessoa.nome));
        }
        #endregion

        #region Limpar Console
        public static void LimparConsole()
        {
            Console.Clear();
        }
        #endregion


    }
}
