﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application
{
    public static class Globals
    {
        public static int CodEstab { get; set; }
        public static int NTerminais { get; set; }
        public static string ConnStr { get; set; }
        public static string ConnName { get; set; }
        public static bool Iniciado { get; set; }
        public static int NLinhas { get; set; }
    }
}
