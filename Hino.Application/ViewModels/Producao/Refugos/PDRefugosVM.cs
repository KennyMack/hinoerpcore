﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Refugos
{
    public class PDRefugosVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codrefugo { get; set; }
        public int codestab { get; set; }
        public long codordprod { get; set; }
        public string nivelordprod { get; set; }
        public DateTime dataref { get; set; }
        public string codestoque { get; set; }
        public long codlancamento { get; set; }
        public int codfuncionario { get; set; }
        public string observacao { get; set; }
        public long codestrutura { get; set; }
        public string turno { get; set; }
        public string codmaquina { get; set; }
    }
}
