﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Refugos
{
    public class PDRefugoMotVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codrefugo { get; set; }
        public virtual PDMotivosVM PDMotivos { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codmotivo { get; set; }
        public int codestab { get; set; }
        public decimal qtdrefugada { get; set; }
        public decimal qtdsucata { get; set; }
    }
}
