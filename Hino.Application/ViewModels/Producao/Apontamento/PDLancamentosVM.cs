﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Apontamento
{
    public class PDLancamentosVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        [DisplayField]
        [RequiredField]
        public long codlancamento { get; set; }
        [Key]
        [Column(Order = 2)]
        [DisplayField]
        [GreatherThanZero]
        [RequiredField]
        public int codestab { get; set; }
        [DisplayField]
        [GreatherThanZero]
        [RequiredField]
        public long codordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public string nivelordprod { get; set; }
        [DisplayField]
        [GreatherThanZero]
        public short operacao { get; set; }
        [DisplayField]
        [GreatherThanZero]
        [RequiredField]
        public int codfuncionario { get; set; }
        [DisplayField]
        [QuantityGreatherThanZero]
        [RequiredField]
        public decimal quantidade { get; set; }
        [DisplayField]
        public DateTime dtinicio { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime dttermino { get; set; }
        [DisplayField]
        [GreatherThanZero]
        [RequiredField]
        public long codestrutura { get; set; }
        [DisplayField]
        [GreatherThanZero]
        public short codroteiro { get; set; }
        [DisplayField]
        public long? codlancpai { get; set; }
        [DisplayField]
        public string turno { get; set; }
        [DisplayField]
        public string codmaquina { get; set; }
        [DisplayField]
        public long? codcor { get; set; }
        [DisplayField]
        public string lotemanual { get; set; }
        [DisplayField]
        [RequiredField]
        public bool reginsp { get; set; }
        [DisplayField]
        public int? codmotivo { get; set; }
        [DisplayField]
        public string codccusto { get; set; }
    }
}
