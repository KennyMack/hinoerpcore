﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Apontamento
{
    public class PDAptTerminoProdVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codfimapt { get; set; }
        [Key]
        [Column(Order = 1)]
        public int codestab { get; set; }
        public long codiniapt { get; set; }
        public string codusuario { get; set; }
        public int codfuncionario { get; set; }
        public DateTime dttermino { get; set; }
        public short tipo { get; set; }
        public decimal quantidade { get; set; }
        public int? codmotivo { get; set; }
        public decimal qtdrefugo { get; set; }
        public decimal qtdretrabalho { get; set; }
        public string observacao { get; set; }
    }
}
