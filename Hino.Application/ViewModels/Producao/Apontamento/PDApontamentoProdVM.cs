﻿using Hino.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Apontamento
{
    public class PDApontamentoProdVM: BaseVM
    {
        public int codestab { private get; set; }
        public string codproduto { private get; set; }
        public string codsucata { private get; set; }
        public string codunidade { private get; set; }
        public bool rastreabilidade { private get; set; }
        public DateTime datafechamento { private get; set; }
        public DataTable dtRefugos { private get; set; }
        public Decimal? qtdApontamento { get; set; }
        public Decimal? qtdTotRefugada { get; set; }
        public Decimal? qtdTotReaproveitada { get; set; }
        public Decimal? qtdTotSucata { get; set; }
        public Int64? lotegerado { get; set; }
        public bool apontaProcesso { get; set; }
        public bool aptoAcabado { get; set; }
        public decimal saldoProcessoFinal { get; set; }
        public Boolean validaProcesso { get; set; }
        public string codusuario { get; set; }
        
    }
}
