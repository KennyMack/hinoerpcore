﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Apontamento
{
    public class PDApontLancRefugoVM: BaseVM
    {
        #region Propriedades públicas
        public int codestab { get; set; }
        public long codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public string codproduto { get; set; }
        public decimal qtdApontamento { get; set; }
        public decimal qtdTotRefugada { get; set; }
        public decimal qtdTotSucata { get; set; }
        public decimal qtdTotReaproveitada { get; set; }
        public short operacao { get; set; }
        public short codroteiro { get; set; }
        #endregion
    }
}
