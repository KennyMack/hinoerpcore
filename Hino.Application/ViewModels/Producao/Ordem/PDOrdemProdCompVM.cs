﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Ordem
{
    public class PDOrdemProdCompVM : BaseVM
    {
        [Key]
        [Column(Order = 1)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public long codordprod { get; set; }
        [Key]
        [Column(Order = 3)]
        public string nivelordprod { get; set; }
        [Key]
        [Column(Order = 4)]
        public long codestrutura { get; set; }
        [Key]
        [Column(Order = 5)]
        public short codroteiro { get; set; }
        [Key]
        [Column(Order = 6)]
        public short operacao { get; set; }
        [Key]
        [Column(Order = 7)]
        public string codcomponente { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }
        public bool baixaapto { get; set; }
        public decimal variacao { get; set; }
        public decimal perda { get; set; }
        public bool reaproveita { get; set; }
        public decimal qtdconsumida { get; set; }
        public bool altmanual { get; set; }
        public decimal fator { get; set; }
        public bool geraconsudif { get; set; }
        public long sequencia { get; set; }
        public long camadas { get; set; }
        public long passadas { get; set; }
        public short nivel { get; set; }
        public int? codtecido { get; set; }
        public decimal percagrup { get; set; }
        public string codunidade { get; set; }
        public decimal qtdeeng { get; set; }
        public bool separacao { get; set; }
    }
}
