﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Ordem
{
    public class PDOrdemProdVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long codordprod { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 3)]
        public string nivelordprod { get; set; }
        [Key]
        [Column(Order = 4)]
        public long codestrutura { get; set; }
        public long codprograma { get; set; }
        public long codordprodpai { get; set; }
        public string nivelordprodpai { get; set; }
        public short codroteiro { get; set; }
        public string codproduto { get; set; }
        public DateTime dtinicio { get; set; }
        public DateTime dttermino { get; set; }
        public DateTime? dtencerramento { get; set; }
        public decimal programado { get; set; }
        public decimal realizado { get; set; }
        public decimal refugado { get; set; }
        public string status { get; set; }
        public long ultimasequencia { get; set; }
        public decimal fator { get; set; }
        public decimal qtdinicial { get; set; }
        public decimal custoinicial { get; set; }
        public decimal custoreproc { get; set; }
        public long seqbarras { get; set; }
        public long? codagrupfoto { get; set; }
        public bool liberado { get; set; }
        public DateTime dtemissao { get; set; }
        public short usaopestoque { get; set; }
        public short usasaldoestoque { get; set; }
    }
}
