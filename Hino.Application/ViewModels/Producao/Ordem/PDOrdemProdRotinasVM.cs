﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Ordem
{
    public class PDOrdemProdRotinasVM : BaseVM
    {
        [Key]
        [Column(Order = 1)]
        public long codordprod { get; set; }
        [Key]
        [Column(Order = 2)]
        public string nivelordprod { get; set; }
        [Key]
        [Column(Order = 3)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 4)]
        public long codestrutura { get; set; }
        [Key]
        [Column(Order = 5)]
        public short codroteiro { get; set; }
        [Key]
        [Column(Order = 6)]
        public short operacao { get; set; }
        public short codrotina { get; set; }
        public string descricao { get; set; }
        public string codmaquina { get; set; }
        public decimal prodhoraria { get; set; }
        public decimal operador { get; set; }
        public bool aptoprocesso { get; set; }
        public bool aptoacabado { get; set; }
        public decimal qtdeproducao { get; set; }
        public decimal qtderefugo { get; set; }
        public string codmaqopc { get; set; }
        public decimal ciclopeca { get; set; }
        public decimal pecaciclo { get; set; }
        public DateTime dtinicio { get; set; }
        public DateTime dttermino { get; set; }
    }
}
