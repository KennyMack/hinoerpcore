﻿using Hino.Infra.Cross.Entities.Fiscal.Produto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application.ViewModels.Producao.Ordem
{
    public class PDOrdemProdCompNecVM: BaseVM
    {
        public string codcomponente { get; set; }
        public string produtodescricao { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }
        public decimal qtdconsumida { get; set; }
        public decimal qtdnecessaria { get; set; }
        public decimal saldoestoque { get; set; }
        public decimal programadoop { get; set; }
        public string codunidade { get; set; }
        public long codestrutura { get; set; }
        public short codroteiro { get; set; }
        public short operacao { get; set; }
        public decimal fator { get; set; }
        public string statussaldo { get; set; }
    }
}
