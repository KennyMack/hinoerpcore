using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Infra.Cross.Utils.Attributes;

namespace Hino.Application.ViewModels.Engenharia.Estrutura
{
    public class ENEstruturasVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public int codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public long codestrutura { get; set; }
        [DisplayField]
        [RequiredField]
        public string codproduto { get; set; }
        [DisplayField]
        [RequiredField]
        public short codprocesso { get; set; }
        [DisplayField]
        [RequiredField]
        public string descricao { get; set; }
        [DisplayField]
        [RequiredField]
        public string status { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal versao { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime cadastro { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime vigenciainicio { get; set; }
        [DisplayField]
        [RequiredField]
        public string cadusuario { get; set; }
        [DisplayField]
        [RequiredField]
        public string aprovusuario { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoque { get; set; }
        [DisplayField]
        [RequiredField]
        public string fantasma { get; set; }
        [DisplayField]
        [RequiredField]
        public string cronoanalise { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal kgporbatida { get; set; }
        [DisplayField]
        public long? codcor { get; set; }
        [DisplayField]
        [RequiredField]
        public string numdesenho { get; set; }
        [DisplayField]
        public long? codorcitem { get; set; }
        [DisplayField]
        [RequiredField]
        public bool exigeduzentos { get; set; }
    }
}
