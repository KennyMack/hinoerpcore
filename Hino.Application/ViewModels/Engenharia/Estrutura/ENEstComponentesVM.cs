using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Infra.Cross.Utils.Attributes;

namespace Hino.Application.ViewModels.Engenharia.Estrutura
{
    public class ENEstComponentesVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public int codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public long codestrutura { get; set; }
        [DisplayField]
        [RequiredField]
        public short codroteiro { get; set; }
        [DisplayField]
        [RequiredField]
        public short operacao { get; set; }
        [DisplayField]
        [RequiredField]
        public string componente { get; set; }
        [DisplayField]
        [RequiredField]
        public string estoquebaixa { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal quantidade { get; set; }
        [DisplayField]
        [RequiredField]
        public bool baixaapto { get; set; }
        [DisplayField]
        [RequiredField]
        public int variacao { get; set; }
        [DisplayField]
        [RequiredField]
        public int perda { get; set; }
        [DisplayField]
        [RequiredField]
        public bool reaproveita { get; set; }
        [DisplayField]
        [RequiredField]
        public string codagrup { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal geraconsudif { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal sequencia { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal camadas { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal passadas { get; set; }
        [DisplayField]
        [RequiredField]
        public short nivel { get; set; }
        [DisplayField]
        public int? codtecido { get; set; }
        [DisplayField]
        [RequiredField]
        public string codunidade { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal qtdeeng { get; set; }
        [DisplayField]
        [RequiredField]
        public string espectec { get; set; }
        [DisplayField]
        [RequiredField]
        public bool separacao { get; set; }
    }
}
