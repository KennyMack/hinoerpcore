using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Infra.Cross.Utils.Attributes;

namespace Hino.Application.ViewModels.Engenharia.Estrutura
{
    public class ENEstRotinasVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public int codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public long codestrutura { get; set; }
        [DisplayField]
        [RequiredField]
        public short codroteiro { get; set; }
        [DisplayField]
        [RequiredField]
        public short codrotina { get; set; }
        [DisplayField]
        [RequiredField]
        public string descricao { get; set; }
        [DisplayField]
        [RequiredField]
        public short operacao { get; set; }
        [DisplayField]
        [RequiredField]
        public string codmaquina { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal prodhoraria { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal operador { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal setup { get; set; }
        [DisplayField]
        [RequiredField]
        public bool aptoprocesso { get; set; }
        [DisplayField]
        [RequiredField]
        public bool aptoacabado { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal segundosporpeca { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal minutossporpeca { get; set; }
        [DisplayField]
        [RequiredField]
        public string codmaqopc { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal ciclopeca { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal pecaciclo { get; set; }
        [DisplayField]
        [RequiredField]
        public string areaproduto { get; set; }
    }
}
