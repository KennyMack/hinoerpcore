﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Fiscal.Estoque
{
    public class FSSaldoEstoqueVM : BaseVM
    {
        [Key]
        [Column(Order = 1)]
        public string codproduto { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 3)]
        public string codestoque { get; set; }
        public short status { get; set; }
        public decimal anterior { get; set; }
        public decimal entrada { get; set; }
        public decimal saida { get; set; }
    }
}
