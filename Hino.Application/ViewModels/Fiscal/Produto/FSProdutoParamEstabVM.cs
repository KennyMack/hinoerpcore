﻿using System;
using System.Collections.Generic;
using System.Text;
using Hino.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Application.ViewModels.Fiscal.Produto
{
    public class FSProdutoParamEstabVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public string codproduto { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        public string codfamilia { get; set; }
        public string codtipo { get; set; }
        public string codestoque { get; set; }
        public string codunidade { get; set; }
        public string ncm { get; set; }
        public short status { get; set; }
        public short contestoque { get; set; }
        public int? codaplicentrada { get; set; }
        public int? codaplicsaida { get; set; }
        public string codserv { get; set; }
        public short? codorigmerc { get; set; }
        public string codcest { get; set; }
        public decimal perclucro { get; set; }
        public short atualizapreco { get; set; }
        public short? tipoatualizacao { get; set; }
        public short? mrpplanjcomp { get; set; }
        public decimal perperdasped { get; set; }
        public string codanp { get; set; }
        public short enviagtinxml { get; set; }
        public decimal fatconvunidex { get; set; }
        public DateTime? dtultcompra { get; set; }
        public decimal vlrultcompra { get; set; }
        public short imobilizado { get; set; }
        public int? codsubfamilia { get; set; }
        public DateTime datamodificacao { get; set; }
        public string uniquekey { get; set; }
        public long? idapi { get; set; }
        public decimal vlrultcompraimp { get; set; }
        public short permsinc { get; set; }
        public string codgrupo { get; set; }
        public int diasvalidade { get; set; }
        public string numfci { get; set; }
        public long? codcadanvisa { get; set; }
        public int? codmarca { get; set; }
    }
}
