﻿using System;
using System.Collections.Generic;
using System.Text;
using Hino.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Application.ViewModels.Fiscal.Produto
{
    public class FSProdutoPCPVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public string codproduto { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        public decimal estoqueminimo { get; set; }
        public decimal estoquemaximo { get; set; }
        public decimal pesoliquido { get; set; }
        public decimal pesobruto { get; set; }
        public decimal qtdeporemb { get; set; }
        public decimal loteminimo { get; set; }
        public decimal lotemaximo { get; set; }
        public decimal lotemultiplo { get; set; }
        public decimal lotedia { get; set; }
        public bool rastreabilidade { get; set; }
        public decimal leadtime { get; set; }
        public short? rotpadrao { get; set; }
        public string codsucata { get; set; }
        public bool encomenda { get; set; }
        public decimal ltentrega { get; set; }
        public decimal tamanhocubico { get; set; }
        public decimal qtddeembalagem { get; set; }
        public decimal calcestminauto { get; set; }
        public string codgtin { get; set; }
        public decimal perctolcompra { get; set; }
        public bool compagregado { get; set; }
        public string codgtinemb { get; set; }
        public bool arqoriginal { get; set; }
        public decimal fatorimpressao { get; set; }
        public bool importado { get; set; }
        public int? codempresa { get; set; }
        public decimal comprimento { get; set; }
        public decimal largura { get; set; }
        public decimal espessura { get; set; }
        public string umcompra { get; set; }
        public decimal fatorumcompra { get; set; }
        public string umvenda { get; set; }
        public decimal fatorumvenda { get; set; }
    }
}
