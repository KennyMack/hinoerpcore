﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Hino.Application.ViewModels.Fiscal.Produto
{
    public class FSProdutoVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        public string codproduto { get; set; }
        public string descricao { get; set; }
        public string detalhamento { get; set; }
        public string infadicfiscal { get; set; }
    }
}
