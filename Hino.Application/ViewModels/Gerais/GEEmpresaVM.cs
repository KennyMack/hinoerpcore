﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Gerais
{
    public class GEEmpresaVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        public int codempresa { get; set; }
        [PaginateBy]
        public string razaosocial { get; set; }
        public string nomefantasia { get; set; }
    }
}
