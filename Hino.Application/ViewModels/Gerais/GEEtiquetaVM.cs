﻿using Hino.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Application.ViewModels.Gerais
{
    public class GEEtiquetaVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        public long codetiqueta { get; set; }
        public int? codempresa { get; set; }
        public string descricao { get; set; }
        public short tipo { get; set; }
        public string conteudo { get; set; }
        public bool quebraporvol { get; set; }
    }
}
