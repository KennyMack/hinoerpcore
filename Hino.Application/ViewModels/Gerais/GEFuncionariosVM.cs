﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Gerais
{
    public class GEFuncionariosVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public int codfuncionario { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        public string identificacao { get; set; }
        public bool status { get; set; }
        public short? codprocesso { get; set; }
        public int codpessoa { get; set; }
        public string codccusto { get; set; }
        public long? contacorrente { get; set; }
        public string digitocc { get; set; }
        public string agencia { get; set; }
        public string digitoag { get; set; }
        public int? codbanco { get; set; }
        public string pis { get; set; }
        public string ctsp { get; set; }
        public string seriectsp { get; set; }
        public string ufctsp { get; set; }
        public string cargo { get; set; }
        public DateTime? admissao { get; set; }
        public DateTime? demissao { get; set; }
        public string titulo { get; set; }
        public string setor { get; set; }
        public string categoriahab { get; set; }
        public DateTime? validadehab { get; set; }
        public string codusuario { get; set; }
        public string numcnh { get; set; }
        public string tituloeleitor { get; set; }
        public string zona { get; set; }
        public string secao { get; set; }
        public string nomepai { get; set; }
        public string nomemae { get; set; }
        public string turno { get; set; }
        public bool? aptproducao { get; set; }
        public bool? aptsetup { get; set; }
        public bool? apttrocaserv { get; set; }
        public bool? aptmanut { get; set; }
        public bool? aptconsdesenho { get; set; }
        public bool? aptretrab { get; set; }
        public short? tipoconta { get; set; }
        public short? grauinsturcao { get; set; }
        public DateTime? dataopcao { get; set; }
        public short? numdependentes { get; set; }
        public decimal? salario { get; set; }
        public sbyte? tiposalario { get; set; }
        public int? codpais { get; set; }
        public string cbo { get; set; }
    }
}
