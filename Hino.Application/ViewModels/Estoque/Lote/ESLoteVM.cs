﻿using Hino.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Application.ViewModels.Estoque.Lote
{
    public class ESLoteVM : BaseVM
    {
        [Key]
        [PrimaryKey]
        [Column(Order = 1)]
        public long lote { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codestab { get; set; }
        public long? indiceitemnf { get; set; }
        public short? seqitemnf { get; set; }
        public string codproduto { get; set; }
        public short origem { get; set; }
        [NotMapped]
        public string descorigem { get; set; }
        public string status { get; set; }
        [NotMapped]
        public string descstatus { get; set; }
        public decimal? codlancamento { get; set; }
        public DateTime datalote { get; set; }
        public string aprovador { get; set; }
        [NotMapped]
        public string geusuariosNome { get; set; }
        [NotMapped]
        public decimal qtdentrada { get; set; }
        [NotMapped]
        public decimal qtdsaida { get; set; }
        [NotMapped]
        public string codestoque { get; set; }
        public long? loteorigem { get; set; }
    }
}
