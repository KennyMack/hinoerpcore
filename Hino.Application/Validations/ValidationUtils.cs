﻿using Hino.Infra.Cross.Utils;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Hino.Application.Validations
{
    public static class ValidationUtils
    {
        #region Validate Object
        public static List<ModelException> ValidateObject<T>(this T value, ValidationContext pCtx, FluentValidation.AbstractValidator<T> pValidationResult)
        {
            var Errors = new List<ModelException>();
            var ResultAnnotationValidation = new List<ValidationResult>();

            if (!Validator.TryValidateObject(value, pCtx, ResultAnnotationValidation, true))
            {
                foreach (var item in ResultAnnotationValidation)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Messages = new[] { item.ErrorMessage },
                        Field = item.MemberNames.FirstOrDefault(),
                        Value = ""
                    });
                }
            }

            if (!ResultAnnotationValidation.Any())
            {
                var ResultFluentValidation = pValidationResult.Validate(value);
                foreach (var item in ResultFluentValidation.Errors)
                {
                    var properties = item.FormattedMessagePlaceholderValues.Where(r => r.Key == "PropertyName" && r.Value != null)
                            .Select(r => Infra.Cross.Resources.FieldsNameResource.ResourceManager.GetString(r.Value.ToString()) ?? r.Value).ToArray();

                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Messages = new[] { string.Format(item.ErrorMessage, properties) },
                        Field = item.PropertyName,
                        Value = (item.AttemptedValue ?? "").ToString()
                    });
                }
            }

            return Errors;
        }
        #endregion
    }
}
