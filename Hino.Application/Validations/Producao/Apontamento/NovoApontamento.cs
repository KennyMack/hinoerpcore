﻿using FluentValidation;
using Hino.Application.ViewModels.Producao.Apontamento;
using Hino.Domain.Base.Interfaces.ViewModels;
using Hino.Domain.Base.Validations;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Infra.Cross.Utils;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Hino.Application.Validations.Producao.Apontamento
{
    public class NovoApontamento: AbstractValidator<PDLancamentosVM>, IBaseValidator
    {
        public NovoApontamento(PDParamEstab pParamEstab)
        {
            RuleFor(x => x.quantidade)
                .NotEmpty()
                .GreaterThan(0)
                .WithMessage(Infra.Cross.Resources.ValidationMessagesResource.QuantityGreatherThanZero);
            RuleFor(x => x.dttermino)
                .NotEmpty()
                .WithMessage(Infra.Cross.Resources.ValidationMessagesResource.RequiredDefault);
            RuleFor(x => x.dtinicio)
                .NotEmpty()
                .WithMessage(Infra.Cross.Resources.ValidationMessagesResource.RequiredDefault);
            RuleFor(x => x.codestrutura)
                .NotEmpty()
                .WithMessage(Infra.Cross.Resources.ValidationMessagesResource.RequiredDefault);
            RuleFor(x => x.quantidade)
                .NotEmpty()
                .GreaterThan(0)
                .WithMessage(Infra.Cross.Resources.ValidationMessagesResource.QuantityGreatherThanZero);
            RuleFor(x => x.turno)
                .Must(t => (t.IsEmpty() && !pParamEstab.exigeturnoapto) || !t.IsEmpty())
                .WithMessage(Infra.Cross.Resources.ValidationMessagesResource.QuantityGreatherThanZero);
            RuleFor(x => x)
                .Custom((r, context) =>
                {
                    if (r.dtinicio > r.dttermino)
                        context.AddFailure(Infra.Cross.Resources.ValidationMessagesResource.StartDateTimeGreatherThanEndDateTime);
                    else if (r.dttermino > DateTime.Now)
                        context.AddFailure(Infra.Cross.Resources.ValidationMessagesResource.EndDateTimeGreatherThanNow);
                });
            RuleFor(x => x.codmaquina)
                .Must(t => (t.IsEmpty() && !pParamEstab.exigemaqapto) || !t.IsEmpty())
                .WithMessage(Infra.Cross.Resources.ValidationMessagesResource.QuantityGreatherThanZero);
            RuleFor(x => x)
                .Must(t => {
                    if (pParamEstab.validadtapont)
                    {
                        TimeSpan tempoPercorrido;
                        tempoPercorrido = t.dttermino.Subtract(t.dtinicio);
                        return (pParamEstab.minutosapont < Convert.ToDecimal(tempoPercorrido.TotalMinutes));
                    }

                    return true;
                })
                .WithMessage(string.Format(
                    Infra.Cross.Resources.ValidationMessagesResource.MinMinutesAppointment, pParamEstab.minutosapont));

        }

        #region Validate
        public List<ModelException> ValidateRules(IBaseVM pVM)
        {
            var ctx = new ValidationContext((PDLancamentosVM)pVM);
            return ((PDLancamentosVM)pVM).ValidateObject(ctx, this);
        }
        #endregion
    }
}
