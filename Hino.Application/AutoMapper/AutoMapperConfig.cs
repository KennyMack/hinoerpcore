﻿using AutoMapper;
using Hino.Application.ViewModels.Engenharia.Estrutura;
using Hino.Application.ViewModels.Estoque;
using Hino.Application.ViewModels.Estoque.Lote;
using Hino.Application.ViewModels.Fiscal.Estoque;
using Hino.Application.ViewModels.Fiscal.Produto;
using Hino.Application.ViewModels.Gerais;
using Hino.Application.ViewModels.Producao;
using Hino.Application.ViewModels.Producao.Apontamento;
using Hino.Application.ViewModels.Producao.Ordem;
using Hino.Application.ViewModels.Producao.Refugos;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Infra.Cross.Entities.Estoque;
using Hino.Infra.Cross.Entities.Estoque.Lote;
using Hino.Infra.Cross.Entities.Fiscal.Estoque;
using Hino.Infra.Cross.Entities.Fiscal.Produto;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.Cross.Entities.Producao.Refugos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<ENEstComponentes, ENEstComponentesVM>();
            CreateMap<ENEstComponentesVM, ENEstComponentes>();

            CreateMap<ENEstRotinas, ENEstRotinasVM>();
            CreateMap<ENEstRotinasVM, ENEstRotinas>();

            CreateMap<ENEstruturas, ENEstruturasVM>();
            CreateMap<ENEstruturasVM, ENEstruturas>();

            CreateMap<ESLoteSaldoVM, ESLoteSaldo>();
            CreateMap<ESLoteSaldo, ESLoteSaldoVM>();

            CreateMap<ESLoteVM, ESLote>();
            CreateMap<ESLote, ESLoteVM> ();

            CreateMap<ESKardexVM, ESKardex>();
            CreateMap<ESKardex, ESKardexVM>();
            
            CreateMap<FSSaldoEstoque, FSSaldoEstoqueVM>();
            CreateMap<FSSaldoEstoqueVM, FSSaldoEstoque>();

            CreateMap<FSProdutoParamEstab, FSProdutoParamEstabVM>();
            CreateMap<FSProdutoParamEstabVM, FSProdutoParamEstab>();

            CreateMap<FSProdutoPCPVM, FSProdutoPCP>();
            CreateMap<FSProdutoPCP, FSProdutoPCPVM>();

            CreateMap<FSProdutoVM, FSProduto>();
            CreateMap<FSProduto, FSProdutoVM>();

            CreateMap<GEEmpresaVM, GEEmpresa>();
            CreateMap<GEEmpresa, GEEmpresaVM>();

            CreateMap<GEEstabVM, GEEstab>();
            CreateMap<GEEstab, GEEstabVM>();

            CreateMap<GEEtiquetaVM, GEEtiqueta>();
            CreateMap<GEEtiqueta, GEEtiquetaVM>();

            CreateMap<GEFuncionariosVM, GEFuncionarios>();
            CreateMap<GEFuncionarios, GEFuncionariosVM>();

            CreateMap<GEPessoaVM, GEPessoa>();
            CreateMap<GEPessoa, GEPessoaVM>();

            CreateMap<GEUsuariosVM, GEUsuarios>();
            CreateMap<GEUsuarios, GEUsuariosVM>();

            CreateMap<PDRefugoMotVM, PDRefugoMot>();
            CreateMap<PDRefugoMot, PDRefugoMotVM>();

            CreateMap<PDRefugoReapVM, PDRefugoReap>();
            CreateMap<PDRefugoReap, PDRefugoReapVM>();

            CreateMap<PDRefugosVM, PDRefugos>();
            CreateMap<PDRefugos, PDRefugosVM>();

            CreateMap<PDMotivosVM, PDMotivos>();
            CreateMap<PDMotivos, PDMotivosVM>();

            CreateMap<PDParamEstabVM, PDParamEstab>();
            CreateMap<PDParamEstab, PDParamEstabVM>();

            CreateMap<PDAptInicioProdVM, PDAptInicioProd>();
            CreateMap<PDAptInicioProd, PDAptInicioProdVM>();

            CreateMap<PDAptRetrabalhoVM, PDAptRetrabalho>();
            CreateMap<PDAptRetrabalho, PDAptRetrabalhoVM>();

            CreateMap<PDAptTerminoProdVM, PDAptTerminoProd>();
            CreateMap<PDAptTerminoProd, PDAptTerminoProdVM>();

            CreateMap<PDLancamentosVM, PDLancamentos>();
            CreateMap<PDLancamentos, PDLancamentosVM>();

            CreateMap<PDRetrabalhoVM, PDRetrabalho>();
            CreateMap<PDRetrabalho, PDRetrabalhoVM>();

            CreateMap<PDOrdemProdCompVM, PDOrdemProdComp>();
            CreateMap<PDOrdemProdComp, PDOrdemProdCompVM>();

            CreateMap<PDOrdemProdRotinasVM, PDOrdemProdRotinas>();
            CreateMap<PDOrdemProdRotinas, PDOrdemProdRotinasVM>();

            CreateMap<PDOrdemProd, PDOrdemProdVM>();
            CreateMap<PDOrdemProd, PDOrdemProdVM>();

            CreateMap<PDOrdemProd, PDOrdemProdVM>();
            CreateMap<PDOrdemProd, PDOrdemProdVM>();
        }
    }
}
