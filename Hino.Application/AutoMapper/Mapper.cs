﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application.AutoMapper
{
    public static class Mapper
    {
        public static IMapper IMapperCFG;

        public static TDestination Map<TSource, TDestination>(TSource source) =>
            IMapperCFG.Map<TSource, TDestination>(source);

        public static TDestination Map<TDestination>(object source) =>
            IMapperCFG.Map<TDestination>(source);

        public static void Initialize()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperConfig()));

            IMapperCFG = config.CreateMapper();
        }
    }
}
