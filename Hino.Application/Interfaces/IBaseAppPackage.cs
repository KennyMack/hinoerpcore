﻿using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Application.Interfaces
{
    public interface IBaseAppPackage
    {
        List<ModelException> Errors { get; set; }
    }
}
