﻿using Hino.Infra.Cross.Entities.Estoque.Lote;

namespace Hino.Application.Interfaces.Estoque.Lote
{
    public interface IESLoteAS : IBaseAppService<ESLote>
    {
    }
}
