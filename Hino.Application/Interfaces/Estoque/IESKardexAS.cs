﻿using Hino.Infra.Cross.Entities.Estoque;

namespace Hino.Application.Interfaces.Estoque
{
    public interface IESKardexAS : IBaseAppService<ESKardex>
    {
    }
}
