﻿using Hino.Infra.Cross.Entities.Fiscal.Produto;

namespace Hino.Application.Interfaces.Fiscal.Produto
{
    public interface IFSProdutoParamEstabAS : IBaseAppService<FSProdutoParamEstab>
    {
    }
}
