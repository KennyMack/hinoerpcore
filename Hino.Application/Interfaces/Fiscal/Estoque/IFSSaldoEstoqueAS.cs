﻿using Hino.Infra.Cross.Entities.Fiscal.Estoque;

namespace Hino.Application.Interfaces.Fiscal.Estoque
{
    public interface IFSSaldoEstoqueAS : IBaseAppService<FSSaldoEstoque>
    {
    }
}
