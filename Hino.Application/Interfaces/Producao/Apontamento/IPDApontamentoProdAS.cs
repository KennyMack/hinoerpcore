﻿using Hino.Application.ViewModels.Producao.Apontamento;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Infra.Cross.Utils.Enums;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using static Hino.Application.ViewModels.Producao.Apontamento.PDApontamentoProdVM;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public interface IPDApontamentoProdAS
    {
        DbTransaction Transaction { get; set; }
        List<ModelException> Errors { get; set; }
        PDParamEstab ParamEstab { get; set; }
        PDLancamentosVM Lancamento { get; set; }
        PDApontamentoProdVM PDApontamentoProd { get; set; }
        Task<bool> VerificaOperacoesAsync();
        Task<bool> ValidateApontamentoAsync();
        Task<bool> CheckOperationAndSaveAsync(PDAptInicioProd pdAptInicioProd);
        Task<DateTime> BuscaDataFechamentoAsync(int pCodEstab);
        bool Validate(PDOrdemProdRotinas pOPRotinas);
        Task<EEstadoAtualApontamento> SalvarLancamentoAsync();
        Task<bool> SelecionaLotesAppAsync();
        Task<bool> LancMovKardexAsync();
    }
}
