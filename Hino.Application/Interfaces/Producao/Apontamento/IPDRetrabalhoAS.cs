﻿using Hino.Infra.Cross.Entities.Producao.Apontamento;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public interface IPDRetrabalhoAS : IBaseAppService<PDRetrabalho>
    {
    }
}
