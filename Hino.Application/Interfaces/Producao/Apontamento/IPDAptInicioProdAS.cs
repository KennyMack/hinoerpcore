﻿using Hino.Infra.Cross.Entities.Producao.Apontamento;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public interface IPDAptInicioProdAS : IBaseAppService<PDAptInicioProd>
    {
        Task<bool> WasStartedAsync(PDAptInicioProd pAptInicio);
        Task<PDAptInicioProd> GetLastAptAsync(PDAptInicioProd pAptInicio);
    }
}
