﻿using Hino.Application.ConsoleUtils;
using Hino.Infra.Cross.Entities.Gerais;
using Hino.Infra.Cross.Entities.Producao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public interface IPDApontProducaoAS
    {
        Task<ETiposApontamento.INICIO> InicioProducaoTerminal();
        Task<ETiposApontamento.INICIO> TerminoProducaoTerminal();
        void SetFuncionarioAtual(GEFuncionarios pFuncionario);
        void SetParamEstabAtual(PDParamEstab ppdParamEstab);
    }
}
