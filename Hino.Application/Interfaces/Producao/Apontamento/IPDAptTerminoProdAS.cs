﻿using Hino.Infra.Cross.Entities.Producao.Apontamento;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Apontamento
{
    public interface IPDAptTerminoProdAS : IBaseAppService<PDAptTerminoProd>
    {
        Task<bool> WasFinishedAsync(PDAptInicioProd pAptInicio);
    }
}
