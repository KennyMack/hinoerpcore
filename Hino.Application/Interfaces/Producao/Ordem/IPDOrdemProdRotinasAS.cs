﻿using Hino.Infra.Cross.Entities.Producao.Ordem;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Ordem
{
    public interface IPDOrdemProdRotinasAS : IBaseAppService<PDOrdemProdRotinas>
    {
        Task<List<PDOrdemProdRotinas>> BuscaOperacoesAptOpAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd);
        Task<PDOrdemProdRotinas> BuscaMaqOperacaoOpAsync(PDOrdemProdRotinas pOPRotina);
    }
}
