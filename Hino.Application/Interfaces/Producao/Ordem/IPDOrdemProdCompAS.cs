﻿using Hino.Application.ViewModels.Producao.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao.Ordem
{
    public interface IPDOrdemProdCompAS : IBaseAppService<PDOrdemProdComp>
    {
        Task<List<PDOrdemProdCompNecVM>> SelectSaldoEstoqueNec(int pCodEstab, decimal pQtdLanc, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura);
    }
}
