﻿using Hino.Infra.Cross.Entities.Producao.Ordem;

namespace Hino.Application.Interfaces.Producao.Ordem
{
    public interface IPDOrdemProdAS : IBaseAppService<PDOrdemProd>
    {
        PDOrdemProd GetByBarras(long pSeqBarras);
    }
}
