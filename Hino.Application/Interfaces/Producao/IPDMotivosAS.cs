﻿using Hino.Infra.Cross.Entities.Producao;

namespace Hino.Application.Interfaces.Producao
{
    public interface IPDMotivosAS : IBaseAppService<PDMotivos>
    {
    }
}
