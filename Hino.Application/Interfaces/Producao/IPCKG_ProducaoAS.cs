﻿using Hino.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Application.Interfaces.Producao
{
    public interface IPCKG_ProducaoAS: IBaseAppPackage
    {
        Task<bool> VerificaOperacoesAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, short pOperacao, decimal pQuantidade);

        Task<bool> GetValidaOpAptoAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, short pOperacao, decimal pQuantidade);
        Task<bool> CheckUserPerLocEstAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, long pCodFuncionario);
        Task<bool> GetValidaOpAptoSldDtlancAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, DateTime pDtLanc, decimal pQuantidade);

        Task<bool> ApontLotesFifoAsync(PDLancamentos pLancamento, string pCodUsuario, bool pMovimentaEstoque);
        Task<bool> AptoProducaoAsync(PDLancamentos pLancamento, string pCodUsuario, decimal pQtdrefugo);
        Task<bool> LancConsumoProcessoAsync(PDLancamentos pLancamento, decimal pQtdRefugo, string pCodUsuario);
        Task<bool> MovRefugoOperacaoAsync(int pCodEstab, long pCodLancamento, long pCodRefugo, decimal pQtdRefugo, string pCodUsuario);
        Task<bool> RecalculaTotaisOrdemAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd);
    }
}
