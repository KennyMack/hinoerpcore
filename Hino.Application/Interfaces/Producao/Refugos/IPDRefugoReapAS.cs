﻿using Hino.Infra.Cross.Entities.Producao.Refugos;

namespace Hino.Application.Interfaces.Producao.Refugos
{
    public interface IPDRefugoReapAS : IBaseAppService<PDRefugoReap>
    {
    }
}
