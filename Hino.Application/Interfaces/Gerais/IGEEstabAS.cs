﻿using Hino.Infra.Cross.Entities.Gerais;

namespace Hino.Application.Interfaces.Gerais
{
    public interface IGEEstabAS : IBaseAppService<GEEstab>
    {
    }
}
