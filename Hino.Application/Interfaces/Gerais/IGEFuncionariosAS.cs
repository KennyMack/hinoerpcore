﻿using Hino.Infra.Cross.Entities.Gerais;

namespace Hino.Application.Interfaces.Gerais
{
    public interface IGEFuncionariosAS : IBaseAppService<GEFuncionarios>
    {
        GEFuncionarios GetByIdentficador(int pCodEstab, string pIdentificacao);
    }
}
