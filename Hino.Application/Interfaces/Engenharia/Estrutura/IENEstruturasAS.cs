using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Application.Interfaces;
using Hino.Application.ViewModels.Engenharia.Estrutura;

namespace Hino.Aplication.Interfaces.Engenharia.Estrutura
{
    public interface IENEstruturasAS : IBaseAppService<ENEstruturas>
    {
    }
}
