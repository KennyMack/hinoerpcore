using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Application.Interfaces;

namespace Hino.Aplication.Interfaces.Engenharia.Estrutura
{
    public interface IENEstComponentesAS : IBaseAppService<ENEstComponentes>
    {
    }
}
