using Hino.Infra.Cross.Entities.Producao.Refugos;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Producao.Interfaces.Services.Refugos
{
    public interface IPDRefugosService : IBaseService<PDRefugos>
    {
    }
}
