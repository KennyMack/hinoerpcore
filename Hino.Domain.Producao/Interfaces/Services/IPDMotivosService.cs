using Hino.Infra.Cross.Entities.Producao;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Producao.Interfaces.Services
{
    public interface IPDMotivosService : IBaseService<PDMotivos>
    {
    }
}
