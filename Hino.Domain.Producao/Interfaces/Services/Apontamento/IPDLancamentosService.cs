using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Producao.Interfaces.Services.Apontamento
{
    public interface IPDLancamentosService : IBaseService<PDLancamentos>
    {
    }
}
