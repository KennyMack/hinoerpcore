using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;

namespace Hino.Domain.Producao.Interfaces.Services.Apontamento
{
    public interface IPDAptInicioProdService : IBaseService<PDAptInicioProd>
    {
        Task<bool> WasStartedAsync(PDAptInicioProd pAptInicio);
        Task<PDAptInicioProd> GetLastAptAsync(PDAptInicioProd pAptInicio);
    }
}
