using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;

namespace Hino.Domain.Producao.Interfaces.Services.Apontamento
{
    public interface IPDAptTerminoProdService : IBaseService<PDAptTerminoProd>
    {
        Task<bool> WasFinishedAsync(PDAptInicioProd pAptInicio);
    }
}
