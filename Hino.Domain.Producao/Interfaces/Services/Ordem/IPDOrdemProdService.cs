using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Producao.Interfaces.Services.Ordem
{
    public interface IPDOrdemProdService : IBaseService<PDOrdemProd>
    {
        PDOrdemProd GetByBarras(long pSeqBarras);
    }
}
