using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Domain.Base.Interfaces.Services;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Domain.Producao.Interfaces.Services.Ordem
{
    public interface IPDOrdemProdCompService : IBaseService<PDOrdemProdComp>
    {
        Task<DataTable> SelectSaldoEstoqueNec(int pCodEstab, decimal pQtdLanc, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura);
    }
}
