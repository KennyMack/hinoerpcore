using Hino.Infra.Cross.Entities.Producao.Refugos;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Producao.Interfaces.Repositories.Refugos
{
    public interface IPDRefugosRepository : IBaseRepository<PDRefugos>
    {
    }
}
