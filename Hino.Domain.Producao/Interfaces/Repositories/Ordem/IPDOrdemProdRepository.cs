using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Producao.Interfaces.Repositories.Ordem
{
    public interface IPDOrdemProdRepository : IBaseRepository<PDOrdemProd>
    {
        PDOrdemProd GetByBarras(long pSeqBarras);
    }
}
