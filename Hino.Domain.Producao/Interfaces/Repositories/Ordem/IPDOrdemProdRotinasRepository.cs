using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Producao.Interfaces.Repositories.Ordem
{
    public interface IPDOrdemProdRotinasRepository : IBaseRepository<PDOrdemProdRotinas>
    {
    }
}
