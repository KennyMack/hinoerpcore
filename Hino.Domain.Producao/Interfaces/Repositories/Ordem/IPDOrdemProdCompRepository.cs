using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;
using System.Data;

namespace Hino.Domain.Producao.Interfaces.Repositories.Ordem
{
    public interface IPDOrdemProdCompRepository : IBaseRepository<PDOrdemProdComp>
    {
        Task<DataTable> SelectSaldoEstoqueNec(int pCodEstab, decimal pQtdLanc, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura);
    }
}
