using Hino.Infra.Cross.Entities.Producao;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Producao.Interfaces.Repositories
{
    public interface IPDParamEstabRepository : IBaseRepository<PDParamEstab>
    {
    }
}
