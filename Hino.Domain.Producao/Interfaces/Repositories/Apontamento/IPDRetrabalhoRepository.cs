using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Producao.Interfaces.Repositories.Apontamento
{
    public interface IPDRetrabalhoRepository : IBaseRepository<PDRetrabalho>
    {
    }
}
