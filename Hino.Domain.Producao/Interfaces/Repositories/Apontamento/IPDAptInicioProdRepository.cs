using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Hino.Domain.Producao.Interfaces.Repositories.Apontamento
{
    public interface IPDAptInicioProdRepository : IBaseRepository<PDAptInicioProd>
    {
        Task<bool> WasStartedAsync(PDAptInicioProd pAptInicio);
        Task<PDAptInicioProd> GetLastAptAsync(PDAptInicioProd pAptInicio);
    }
}
