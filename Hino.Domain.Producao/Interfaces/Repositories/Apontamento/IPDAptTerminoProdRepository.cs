using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Hino.Domain.Producao.Interfaces.Repositories.Apontamento
{
    public interface IPDAptTerminoProdRepository : IBaseRepository<PDAptTerminoProd>
    {
        Task<bool> WasFinishedAsync(PDAptInicioProd pAptInicio);
    }
}
