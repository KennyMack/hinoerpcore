using Hino.Domain.Producao.Interfaces.Repositories;
using Hino.Domain.Producao.Interfaces.Services;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services
{
    public class PDMotivosService : BaseService<PDMotivos>, IPDMotivosService
    {
        private readonly IPDMotivosRepository _IPDMotivosRepository;

        public PDMotivosService(IPDMotivosRepository pIPDMotivosRepository) : 
             base(pIPDMotivosRepository)
        {
            _IPDMotivosRepository = pIPDMotivosRepository;
        }
    }
}
