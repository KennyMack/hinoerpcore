using Hino.Domain.Producao.Interfaces.Repositories.Refugos;
using Hino.Domain.Producao.Interfaces.Services.Refugos;
using Hino.Infra.Cross.Entities.Producao.Refugos;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services.Refugos
{
    public class PDRefugoMotService : BaseService<PDRefugoMot>, IPDRefugoMotService
    {
        private readonly IPDRefugoMotRepository _IPDRefugoMotRepository;

        public PDRefugoMotService(IPDRefugoMotRepository pIPDRefugoMotRepository) : 
             base(pIPDRefugoMotRepository)
        {
            _IPDRefugoMotRepository = pIPDRefugoMotRepository;
        }
    }
}
