using Hino.Domain.Producao.Interfaces.Repositories.Refugos;
using Hino.Domain.Producao.Interfaces.Services.Refugos;
using Hino.Infra.Cross.Entities.Producao.Refugos;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services.Refugos
{
    public class PDRefugosService : BaseService<PDRefugos>, IPDRefugosService
    {
        private readonly IPDRefugosRepository _IPDRefugosRepository;

        public PDRefugosService(IPDRefugosRepository pIPDRefugosRepository) : 
             base(pIPDRefugosRepository)
        {
            _IPDRefugosRepository = pIPDRefugosRepository;
        }
    }
}
