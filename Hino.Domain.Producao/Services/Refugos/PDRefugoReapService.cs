using Hino.Domain.Producao.Interfaces.Repositories.Refugos;
using Hino.Domain.Producao.Interfaces.Services.Refugos;
using Hino.Infra.Cross.Entities.Producao.Refugos;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services.Refugos
{
    public class PDRefugoReapService : BaseService<PDRefugoReap>, IPDRefugoReapService
    {
        private readonly IPDRefugoReapRepository _IPDRefugoReapRepository;

        public PDRefugoReapService(IPDRefugoReapRepository pIPDRefugoReapRepository) : 
             base(pIPDRefugoReapRepository)
        {
            _IPDRefugoReapRepository = pIPDRefugoReapRepository;
        }
    }
}
