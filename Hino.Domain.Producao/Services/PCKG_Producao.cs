﻿using Hino.Domain.Base.Interfaces.Services;
using Hino.Domain.Base.Services;
using Hino.Domain.Producao.Interfaces.Packages;
using Hino.Domain.Producao.Interfaces.Services;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hino.Domain.Producao.Services
{
    public class PCKG_Producao: BasePackage, IPCKG_Producao
    {
        private readonly IProducaoPackage _IProducaoPackage;
        public PCKG_Producao(IProducaoPackage pIProducaoPackage) :
            base(pIProducaoPackage)
        {
            _IProducaoPackage = pIProducaoPackage;
        }

        public async Task<bool> CheckUserPerLocEstAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, long pCodFuncionario)
        {
            try
            {
                return await _IProducaoPackage.CheckUserPerLocEstAsync(
                    pCodEstab, pCodOrdProd, 
                    pNivelOrdProd, pCodEstrutura, 
                    pCodFuncionario);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }
            return false;
        }

        public async Task<bool> GetValidaOpAptoAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, short pOperacao, decimal pQuantidade)
        {
            try
            {
                return await _IProducaoPackage.GetValidaOpAptoAsync(
                    pCodEstab, pCodOrdProd,
                    pNivelOrdProd, pCodEstrutura,
                    pOperacao, pQuantidade);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }
            return false;
        }

        public async Task<bool> GetValidaOpAptoSldDtlancAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura, DateTime pDtLanc, decimal pQuantidade)
        {
            try
            {
                return await _IProducaoPackage.GetValidaOpAptoSldDtlancAsync(
                    pCodEstab, pCodOrdProd, 
                    pNivelOrdProd, pCodEstrutura,
                    pDtLanc, pQuantidade);
            }
            catch (Exception ex)
            {
                var msErro = ex.Message;

                if (msErro == "**DIVERG-DT-KARDEX**")
                    msErro = Infra.Cross.Resources.ValidationMessagesResource.NegativeKardexInDateAppointment;

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { msErro }
                });
            }
            return false;
        }

        public async Task<bool> VerificaOperacoesAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd, short pOperacao, decimal pQuantidade)
        {
            try
            {
                return await _IProducaoPackage.VerificaOperacoesAsync(
                    pCodEstab, pCodOrdProd, 
                    pNivelOrdProd, pOperacao, 
                    pQuantidade);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }
            return false;
        }

        public async Task<bool> ApontLotesFifoAsync(PDLancamentos pLancamento, string pCodUsuario, bool pMovimentaEstoque)
        {
            try
            {
                return await _IProducaoPackage.ApontLotesFifoAsync(
                    pLancamento, 
                    pCodUsuario, pMovimentaEstoque);
            }
            catch (Exception ex)
            {
                var message = ex.Message.ClearMessage();

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { message }
                });
            }
            return false;
        }

        public async Task<bool> AptoProducaoAsync(PDLancamentos pLancamento, string pCodUsuario, decimal pQtdrefugo)
        {
            try
            {
                return await _IProducaoPackage.AptoProducaoAsync(
                    pLancamento,
                    pCodUsuario, pQtdrefugo);
            }
            catch (Exception ex)
            {
                var message = ex.Message.ClearMessage();

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { message }
                });
            }
            return false;
        }

        public async Task<bool> LancConsumoProcessoAsync(PDLancamentos pLancamento, decimal pQtdRefugo, string pCodUsuario)
        {
            try
            {
                return await _IProducaoPackage.LancConsumoProcessoAsync(
                    pLancamento, 
                    pQtdRefugo, pCodUsuario);
            }
            catch (Exception ex)
            {
                var message = ex.Message.ClearMessage();

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { message }
                });
            }
            return false;
        }

        public async Task<bool> MovRefugoOperacaoAsync(int pCodEstab, long pCodLancamento, long pCodRefugo, decimal pQtdRefugo, string pCodUsuario)
        {
            try
            {
                return await _IProducaoPackage.MovRefugoOperacaoAsync(
                    pCodEstab, 
                    pCodLancamento, pCodRefugo, 
                    pQtdRefugo, pCodUsuario);
            }
            catch (Exception ex)
            {
                var message = ex.Message.ClearMessage();

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { message }
                });
            }
            return false;
        }

        public async Task<bool> RecalculaTotaisOrdemAsync(int pCodEstab, long pCodOrdProd, string pNivelOrdProd)
        {
            try
            {
                return await _IProducaoPackage.RecalculaTotaisOrdemAsync(
                    pCodEstab, 
                    pCodOrdProd, pNivelOrdProd);
            }
            catch (Exception ex)
            {
                var message = ex.Message.ClearMessage();

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ProcedureError,
                    Field = "",
                    Value = "",
                    Messages = new string[] { message }
                });
            }
            return false;
        }
    }
}
