using Hino.Domain.Producao.Interfaces.Repositories.Ordem;
using Hino.Domain.Producao.Interfaces.Services.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Domain.Base.Services;
using System.Threading.Tasks;
using System.Data;

namespace Hino.Domain.Producao.Services.Ordem
{
    public class PDOrdemProdCompService : BaseService<PDOrdemProdComp>, IPDOrdemProdCompService
    {
        private readonly IPDOrdemProdCompRepository _IPDOrdemProdCompRepository;

        public PDOrdemProdCompService(IPDOrdemProdCompRepository pIPDOrdemProdCompRepository) : 
             base(pIPDOrdemProdCompRepository)
        {
            _IPDOrdemProdCompRepository = pIPDOrdemProdCompRepository;
        }

        public async Task<DataTable> SelectSaldoEstoqueNec(int pCodEstab, decimal pQtdLanc, long pCodOrdProd, string pNivelOrdProd, long pCodEstrutura) =>
            await _IPDOrdemProdCompRepository.SelectSaldoEstoqueNec(pCodEstab, pQtdLanc, pCodOrdProd, pNivelOrdProd, pCodEstrutura);
    }
}
