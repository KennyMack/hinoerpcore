using Hino.Domain.Producao.Interfaces.Repositories.Ordem;
using Hino.Domain.Producao.Interfaces.Services.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services.Ordem
{
    public class PDOrdemProdService : BaseService<PDOrdemProd>, IPDOrdemProdService
    {
        private readonly IPDOrdemProdRepository _IPDOrdemProdRepository;

        public PDOrdemProdService(IPDOrdemProdRepository pIPDOrdemProdRepository) : 
             base(pIPDOrdemProdRepository)
        {
            _IPDOrdemProdRepository = pIPDOrdemProdRepository;
        }

        public PDOrdemProd GetByBarras(long pSeqBarras) =>
            _IPDOrdemProdRepository.GetByBarras(pSeqBarras);
    }
}
