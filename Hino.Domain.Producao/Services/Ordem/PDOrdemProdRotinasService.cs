using Hino.Domain.Producao.Interfaces.Repositories.Ordem;
using Hino.Domain.Producao.Interfaces.Services.Ordem;
using Hino.Infra.Cross.Entities.Producao.Ordem;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services.Ordem
{
    public class PDOrdemProdRotinasService : BaseService<PDOrdemProdRotinas>, IPDOrdemProdRotinasService
    {
        private readonly IPDOrdemProdRotinasRepository _IPDOrdemProdRotinasRepository;

        public PDOrdemProdRotinasService(IPDOrdemProdRotinasRepository pIPDOrdemProdRotinasRepository) : 
             base(pIPDOrdemProdRotinasRepository)
        {
            _IPDOrdemProdRotinasRepository = pIPDOrdemProdRotinasRepository;
        }
    }
}
