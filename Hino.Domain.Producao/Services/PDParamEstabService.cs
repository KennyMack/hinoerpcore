using Hino.Domain.Producao.Interfaces.Repositories;
using Hino.Domain.Producao.Interfaces.Services;
using Hino.Infra.Cross.Entities.Producao;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services
{
    public class PDParamEstabService : BaseService<PDParamEstab>, IPDParamEstabService
    {
        private readonly IPDParamEstabRepository _IPDParamEstabRepository;

        public PDParamEstabService(IPDParamEstabRepository pIPDParamEstabRepository) : 
             base(pIPDParamEstabRepository)
        {
            _IPDParamEstabRepository = pIPDParamEstabRepository;
        }
    }
}
