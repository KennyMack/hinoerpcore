using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Services;
using System.Threading.Tasks;

namespace Hino.Domain.Producao.Services.Apontamento
{
    public class PDAptTerminoProdService : BaseService<PDAptTerminoProd>, IPDAptTerminoProdService
    {
        private readonly IPDAptTerminoProdRepository _IPDAptTerminoProdRepository;

        public PDAptTerminoProdService(IPDAptTerminoProdRepository pIPDAptTerminoProdRepository) : 
             base(pIPDAptTerminoProdRepository)
        {
            _IPDAptTerminoProdRepository = pIPDAptTerminoProdRepository;
        }

        public async Task<bool> WasFinishedAsync(PDAptInicioProd pAptInicio) =>
            await _IPDAptTerminoProdRepository.WasFinishedAsync(pAptInicio);
    }
}
