using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services.Apontamento
{
    public class PDAptRetrabalhoService : BaseService<PDAptRetrabalho>, IPDAptRetrabalhoService
    {
        private readonly IPDAptRetrabalhoRepository _IPDAptRetrabalhoRepository;

        public PDAptRetrabalhoService(IPDAptRetrabalhoRepository pIPDAptRetrabalhoRepository) : 
             base(pIPDAptRetrabalhoRepository)
        {
            _IPDAptRetrabalhoRepository = pIPDAptRetrabalhoRepository;
        }
    }
}
