using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services.Apontamento
{
    public class PDRetrabalhoService : BaseService<PDRetrabalho>, IPDRetrabalhoService
    {
        private readonly IPDRetrabalhoRepository _IPDRetrabalhoRepository;

        public PDRetrabalhoService(IPDRetrabalhoRepository pIPDRetrabalhoRepository) : 
             base(pIPDRetrabalhoRepository)
        {
            _IPDRetrabalhoRepository = pIPDRetrabalhoRepository;
        }
    }
}
