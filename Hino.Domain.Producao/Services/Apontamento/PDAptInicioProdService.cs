using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Services;
using System.Threading.Tasks;

namespace Hino.Domain.Producao.Services.Apontamento
{
    public class PDAptInicioProdService : BaseService<PDAptInicioProd>, IPDAptInicioProdService
    {
        private readonly IPDAptInicioProdRepository _IPDAptInicioProdRepository;

        public PDAptInicioProdService(IPDAptInicioProdRepository pIPDAptInicioProdRepository) : 
             base(pIPDAptInicioProdRepository)
        {
            _IPDAptInicioProdRepository = pIPDAptInicioProdRepository;
        }

        public async Task<bool> WasStartedAsync(PDAptInicioProd pAptInicio) =>
            await _IPDAptInicioProdRepository.WasStartedAsync(pAptInicio);

        public async Task<PDAptInicioProd> GetLastAptAsync(PDAptInicioProd pAptInicio) =>
            await _IPDAptInicioProdRepository.GetLastAptAsync(pAptInicio);
    }
}
