using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Producao.Services.Apontamento
{
    public class PDLancamentosService : BaseService<PDLancamentos>, IPDLancamentosService
    {
        private readonly IPDLancamentosRepository _IPDLancamentosRepository;

        public PDLancamentosService(IPDLancamentosRepository pIPDLancamentosRepository) : 
             base(pIPDLancamentosRepository)
        {
            _IPDLancamentosRepository = pIPDLancamentosRepository;
        }
    }
}
