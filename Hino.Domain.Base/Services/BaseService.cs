﻿using Hino.Domain.Base.Exceptions;
using Hino.Domain.Base.Interfaces.Repositories;
using Hino.Domain.Base.Interfaces.Services;
using Hino.Infra.Cross.Utils.Exceptions;
using Hino.Infra.Cross.Utils.Paging;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Domain.Base.Services
{
    public class BaseService<T> : IDisposable, IBaseService<T> where T : class
    {
        public bool DontSendToQueue { get; set; }
        public List<ModelException> Errors { get; set; }
        public IBaseRepository<T> DataRepository { get; }
        public IList<T> AddedOrUpdatedItems { get; }
        public IList<T> RemovedItems { get; }

        public BaseService(IBaseRepository<T> repo)
        {
            DataRepository = repo;
            AddedOrUpdatedItems = new List<T>();
            RemovedItems = new List<T>();
            Errors = new List<ModelException>();
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync() =>
            await DataRepository.BeginTransactionAsync();

        public virtual T Add(T model)
        {
            try
            {
                DataRepository.Add(model);
                AddedOrUpdatedItems.Add(model);
            }
            catch (Exception e)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }
            return model;
        }

        public virtual T Update(T model)
        {
            try
            {
                DataRepository.Update(model);
                AddedOrUpdatedItems.Add(model);
            }
            catch (Exception e)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UpdateSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message }
                });
            }
            return model;
        }

        public virtual T Remove(T model)
        {
            try
            {
                DataRepository.Remove(model);
                RemovedItems.Remove(model);
            }
            catch (Exception e)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.DeleteSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }
            return model;
        }

        public virtual T RemoveByWhere(Expression<Func<T, bool>> predicate)
        {
            try
            {
                var model = DataRepository.RemoveByWhere(predicate);
                RemovedItems.Add(model);
                return model;
            }
            catch (Exception e)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.DeleteSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message }
                });
            }
            return null;
        }

        public virtual async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.GetAllPagedAsync(page, pageSize, includeProperties);        

        public virtual async Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.GetAllAsync(includeProperties);

        public virtual T GetFirstByWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            DataRepository.GetFirstByWhere(predicate, includeProperties);

        public virtual async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.QueryAsync(predicate, includeProperties);        

        public virtual async Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.QueryPagedAsync(page, pageSize, predicate, includeProperties);

        public virtual T GetByWhereForUpdate(Expression<Func<T, bool>> predicate) =>
            DataRepository.GetByWhereForUpdate(predicate);

        public virtual long NextSequence() => DataRepository.NextSequence();

        public async Task<int> SaveChanges()
        {
            try
            {
                var ret = await DataRepository.SaveChanges();

                if (ret > 0)
                    await GenerateEntryQueueAsync();

                return ret;
            }
            catch (Exception e)
            {
                if (e is IModelEntityValidationException exception)
                {
                    Errors = exception.MException;
                }
                else
                {

                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                                e?.InnerException.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
            return -1;
        }

        public void Dispose()
        {
            DataRepository.Dispose();
            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }

        #region Generate Entry Queue
        public virtual async Task GenerateEntryQueueAsync()
        {
            await Task.Delay(1);
            if (!DontSendToQueue)
            {
                // var queue = new MessageService<T>();
                // foreach (var item in AddedOrUpdatedItems)
                // {
                //     // var Entity = (IBaseEntity)item;
                //     // var dbItem = await GetByIdAsync(Entity.Id, Entity.EstablishmentKey, Entity.UniqueKey);
                //     queue.SendCreatedOrUpdatedQueue(item);
                // }
                // 
                // foreach (var item in RemovedItems)
                //     queue.SendRemovedQueue(item);
            }
            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }
        #endregion

    }
}
