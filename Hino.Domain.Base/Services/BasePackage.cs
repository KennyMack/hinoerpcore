﻿using Hino.Domain.Base.Interfaces.Packages;
using Hino.Domain.Base.Interfaces.Services;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Domain.Base.Services
{
    public class BasePackage: IBasePackage, IDisposable
    {
        public IBaseDBPackage BaseDBPackage { get; }
        public List<ModelException> Errors { get; set; }

        public BasePackage(IBaseDBPackage package)
        {
            BaseDBPackage = package;
            Errors = new List<ModelException>();
        }

        public void Dispose()
        {
            Errors.Clear();
        }
    }
}
