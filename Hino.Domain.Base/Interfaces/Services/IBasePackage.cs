﻿using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Domain.Base.Interfaces.Services
{
    public interface IBasePackage
    {
        List<ModelException> Errors { get; set; }
        void Dispose();
    }
}
