﻿using Hino.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hino.Infra.Cross.Utils.Exceptions;
using Hino.Domain.Base.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore.Storage;

namespace Hino.Domain.Base.Interfaces.Services
{
    public interface IBaseService<T> where T : class
    {
        Task<IDbContextTransaction> BeginTransactionAsync();
        bool DontSendToQueue { get; set; }
        IList<T> AddedOrUpdatedItems { get; }
        IList<T> RemovedItems { get; }
        List<ModelException> Errors { get; set; }
        IBaseRepository<T> DataRepository { get; }

        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);

        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);        
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T GetByWhereForUpdate(Expression<Func<T, bool>> predicate);

        T Add(T model);
        T Update(T model);
        T Remove(T model);
        
        long NextSequence();
        Task<int> SaveChanges();
        void Dispose();
        Task GenerateEntryQueueAsync();

        T GetFirstByWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T RemoveByWhere(Expression<Func<T, bool>> predicate);
    }
}
