﻿using System;
using System.Threading.Tasks;

namespace Hino.Domain.Base.Interfaces.Email
{
    public interface IBaseEmail<T> where T : class
    {
        Task<bool> SendCreate(T model, string pFrom, string pTo);
        Task<bool> SendUpdate(T model, string pFrom, string pTo);
        Task<bool> SendRemove(T model, string pFrom, string pTo);
    }
}
