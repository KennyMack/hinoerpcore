﻿using Hino.Infra.Cross.Utils.Paging;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Domain.Base.Interfaces.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        Task<IDbContextTransaction> BeginTransactionAsync();
        T GetByWhereForUpdate(Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);

        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        T Add(T model);
        T Update(T model);
        T Remove(T model);
        
        long NextSequence();
        Task<int> SaveChanges();
        void Dispose();

        T GetFirstByWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T RemoveByWhere(Expression<Func<T, bool>> predicate);
    }
}
