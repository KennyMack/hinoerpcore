﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Domain.Base.Interfaces.Packages
{
    public interface IBaseDBPackage
    {
        void Dispose();
    }
}
