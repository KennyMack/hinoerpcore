﻿using Hino.Domain.Base.Interfaces.ViewModels;
using Hino.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Domain.Base.Validations
{
    public interface IBaseValidator
    {
        List<ModelException> ValidateRules(IBaseVM pVM);
    }
}
