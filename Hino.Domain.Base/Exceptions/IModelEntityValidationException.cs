﻿using Hino.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;

namespace Hino.Domain.Base.Exceptions
{
    public interface IModelEntityValidationException
    {
        string Message { get; }
        List<ModelException> MException { get; }
    }
}
