﻿using System.ComponentModel.DataAnnotations;

namespace Hino.Domain.Base.Enums
{
    public enum EUserType
    {
        [Display(Description = "Ajudante")]
        Helper = 0,

        [Display(Description = "Vendedor")]
        Sallesman = 1,

        [Display(Description = "Representante")]
        Representant = 2,

        [Display(Description = "Diretor")]
        Director = 3,

        [Display(Description = "Administrador")]
        Master = 4
    }
}
