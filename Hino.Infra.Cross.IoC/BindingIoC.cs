﻿using Hino.Aplication.Interfaces.Engenharia.Estrutura;
using Hino.Aplication.Services.Engenharia.Estrutura;
using Hino.Application.Interfaces.Estoque;
using Hino.Application.Interfaces.Estoque.Lote;
using Hino.Application.Interfaces.Fiscal.Estoque;
using Hino.Application.Interfaces.Fiscal.Produto;
using Hino.Application.Interfaces.Gerais;
using Hino.Application.Interfaces.Producao;
using Hino.Application.Interfaces.Producao.Apontamento;
using Hino.Application.Interfaces.Producao.Ordem;
using Hino.Application.Interfaces.Producao.Refugos;
using Hino.Application.Services.Estoque;
using Hino.Application.Services.Producao;
using Hino.Application.Services.Producao.Apontamento;
using Hino.Domain.Engenharia.Interfaces.Repositories.Estrutura;
using Hino.Domain.Engenharia.Interfaces.Services.Estrutura;
using Hino.Domain.Engenharia.Services.Estrutura;
using Hino.Domain.Estoque.Interfaces.Packages;
using Hino.Domain.Estoque.Interfaces.Repositories;
using Hino.Domain.Estoque.Interfaces.Repositories.Lote;
using Hino.Domain.Estoque.Interfaces.Services;
using Hino.Domain.Estoque.Interfaces.Services.Lote;
using Hino.Domain.Estoque.Services;
using Hino.Domain.Estoque.Services.Lote;
using Hino.Domain.Fiscal.Interfaces.Repositories.Estoque;
using Hino.Domain.Fiscal.Interfaces.Repositories.Produto;
using Hino.Domain.Fiscal.Interfaces.Services.Estoque;
using Hino.Domain.Fiscal.Interfaces.Services.Produto;
using Hino.Domain.Fiscal.Services.Estoque;
using Hino.Domain.Fiscal.Services.Produto;
using Hino.Domain.Gerais.Interfaces.Repositories;
using Hino.Domain.Gerais.Interfaces.Services;
using Hino.Domain.Gerais.Services;
using Hino.Domain.Producao.Interfaces.Packages;
using Hino.Domain.Producao.Interfaces.Repositories;
using Hino.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Domain.Producao.Interfaces.Repositories.Ordem;
using Hino.Domain.Producao.Interfaces.Repositories.Refugos;
using Hino.Domain.Producao.Interfaces.Services;
using Hino.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Domain.Producao.Interfaces.Services.Ordem;
using Hino.Domain.Producao.Interfaces.Services.Refugos;
using Hino.Domain.Producao.Services;
using Hino.Domain.Producao.Services.Apontamento;
using Hino.Domain.Producao.Services.Ordem;
using Hino.Domain.Producao.Services.Refugos;
using Hino.Infra.DataBase.Context;
using Hino.Infra.DataBase.Packages.Estoque;
using Hino.Infra.DataBase.Packages.Producao;
using Hino.Infra.DataBase.Repositories.Engenharia.Estrutura;
using Hino.Infra.DataBase.Repositories.Estoque;
using Hino.Infra.DataBase.Repositories.Estoque.Lote;
using Hino.Infra.DataBase.Repositories.Fiscal.Estoque;
using Hino.Infra.DataBase.Repositories.Fiscal.Produto;
using Hino.Infra.DataBase.Repositories.Gerais;
using Hino.Infra.DataBase.Repositories.Producao;
using Hino.Infra.DataBase.Repositories.Producao.Apontamento;
using Hino.Infra.DataBase.Repositories.Producao.Ordem;
using Hino.Infra.DataBase.Repositories.Producao.Refugos;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Infra.Cross.IoC
{
    public static class BindingIoC
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>();

            services.AddTransient<IPDApontProducaoAS, PDApontProducaoAS>();

            services.AddTransient<IPDApontamentoProdAS, PDApontamentoProdAS>();
            services.AddTransient<IESKardexRepository, ESKardexRepository>();
            services.AddTransient<IESLoteRepository, ESLoteRepository>();
            services.AddTransient<IESLoteSaldoRepository, ESLoteSaldoRepository>();
            services.AddTransient<IESLoteSaldoService, ESLoteSaldoService>();
            services.AddTransient<IESLoteService, ESLoteService>();
            services.AddTransient<IFSSaldoEstoqueRepository, FSSaldoEstoqueRepository>();
            services.AddTransient<IFSProdutoParamEstabRepository, FSProdutoParamEstabRepository>();
            services.AddTransient<IFSProdutoPCPRepository, FSProdutoPCPRepository>();
            services.AddTransient<IFSProdutoRepository, FSProdutoRepository>();
            services.AddTransient<IFSSaldoEstoqueService, FSSaldoEstoqueService>();
            services.AddTransient<IFSProdutoParamEstabService, FSProdutoParamEstabService>();
            services.AddTransient<IFSProdutoPCPService, FSProdutoPCPService>();
            services.AddTransient<IFSProdutoService, FSProdutoService>();

            services.AddTransient<IPDOrdemProdCompService, PDOrdemProdCompService>();

            services.AddTransient<IPDOrdemProdRotinasService, PDOrdemProdRotinasService>();
            services.AddTransient<IPDOrdemProdRotinasRepository, PDOrdemProdRotinasRepository>();
            services.AddTransient<IPDOrdemProdRotinasAS, PDOrdemProdRotinasAS>();

            services.AddTransient<IPDParamEstabRepository, PDParamEstabRepository>();
            services.AddTransient<IPDParamEstabAS, PDParamEstabAS>();
            services.AddTransient<IPDParamEstabService, PDParamEstabService>();

            services.AddTransient<IPDOrdemProdRepository, PDOrdemProdRepository>();
            services.AddTransient<IPDOrdemProdService, PDOrdemProdService>();
            services.AddTransient<IPDOrdemProdAS, PDOrdemProdAS>();

            services.AddTransient<IENEstRotinasRepository, ENEstRotinasRepository>();
            services.AddTransient<IENEstRotinasService, ENEstRotinasService>();
            services.AddTransient<IENEstRotinasAS, ENEstRotinasAS>();

            services.AddTransient<IENEstComponentesRepository, ENEstComponentesRepository>();
            services.AddTransient<IENEstComponentesService, ENEstComponentesService>();
            services.AddTransient<IENEstComponentesAS, ENEstComponentesAS>();

            services.AddTransient<IPDRefugoMotService, PDRefugoMotService>();
            services.AddTransient<IPDRefugoMotRepository, PDRefugoMotRepository>();
            services.AddTransient<IPDRefugoMotAS, PDRefugoMotAS>();

            services.AddTransient<IENEstruturasRepository, ENEstruturasRepository>();
            services.AddTransient<IENEstruturasService, ENEstruturasService>();
            services.AddTransient<IENEstruturasAS, ENEstruturasAS>();

            services.AddTransient<IPDAptInicioProdService, PDAptInicioProdService>();

            services.AddTransient<IPDAptTerminoProdAS, PDAptTerminoProdAS>();
            services.AddTransient<IPDAptTerminoProdService, PDAptTerminoProdService>();
            services.AddTransient<IPDAptTerminoProdRepository, PDAptTerminoProdRepository>();

            services.AddTransient<IGEEmpresaRepository, GEEmpresaRepository>();
            services.AddTransient<IGEEstabRepository, GEEstabRepository>();
            services.AddTransient<IGEPessoaRepository, GEPessoaRepository>();
            services.AddTransient<IGEEtiquetaRepository, GEEtiquetaRepository>();
            services.AddTransient<IGEFuncionariosRepository, GEFuncionariosRepository>();
            services.AddTransient<IGEUsuariosRepository, GEUsuariosRepository>();
            services.AddTransient<IGEEmpresaService, GEEmpresaService>();
            services.AddTransient<IGEEstabService, GEEstabService>();
            services.AddTransient<IGEPessoaService, GEPessoaService>();
            services.AddTransient<IGEEtiquetaService, GEEtiquetaService>();
            services.AddTransient<IGEFuncionariosService, GEFuncionariosService>();
            services.AddTransient<IGEUsuariosService, GEUsuariosService>();

            services.AddTransient<IPDRefugoReapService, PDRefugoReapService>();

            services.AddTransient<IPDAptInicioProdRepository, PDAptInicioProdRepository>();
            services.AddTransient<IPDAptRetrabalhoRepository, PDAptRetrabalhoRepository>();
            services.AddTransient<IPDAptTerminoProdRepository, PDAptTerminoProdRepository>();
            services.AddTransient<IPDLancamentosRepository, PDLancamentosRepository>();
            services.AddTransient<IPDLancamentosService, PDLancamentosService>();
            services.AddTransient<IPDRetrabalhoRepository, PDRetrabalhoRepository>();
            services.AddTransient<IPDOrdemProdCompRepository, PDOrdemProdCompRepository>();
            services.AddTransient<IPDOrdemProdRepository, PDOrdemProdRepository>();
            services.AddTransient<IPDOrdemProdRotinasRepository, PDOrdemProdRotinasRepository>();
            services.AddTransient<IPDRefugoMotRepository, PDRefugoMotRepository>();
            services.AddTransient<IPDRefugoReapRepository, PDRefugoReapRepository>();
            services.AddTransient<IPDRefugosRepository, PDRefugosRepository>();
            services.AddTransient<IPDMotivosRepository, PDMotivosRepository>();
            services.AddTransient<IPDParamEstabRepository, PDParamEstabRepository>();

            services.AddTransient<IESKardexAS, ESKardexAS>();
            services.AddTransient<IESLoteAS, ESLoteAS>();
            services.AddTransient<IESLoteSaldoAS, ESLoteSaldoAS>();
            services.AddTransient<IFSProdutoParamEstabAS, FSProdutoParamEstabAS>();
            services.AddTransient<IFSProdutoPCPAS, FSProdutoPCPAS>();
            services.AddTransient<IFSProdutoAS, FSProdutoAS>();
            services.AddTransient<IFSSaldoEstoqueAS, FSSaldoEstoqueAS>();
            services.AddTransient<IGEEmpresaAS, GEEmpresaAS>();
            services.AddTransient<IGEEstabAS, GEEstabAS>();
            services.AddTransient<IGEPessoaAS, GEPessoaAS>();
            services.AddTransient<IGEEtiquetaAS, GEEtiquetaAS>();
            services.AddTransient<IGEFuncionariosAS, GEFuncionariosAS>();
            services.AddTransient<IGEUsuariosAS, GEUsuariosAS>();
            services.AddTransient<IPDAptInicioProdAS, PDAptInicioProdAS>();
            services.AddTransient<IPDAptRetrabalhoAS, PDAptRetrabalhoAS>();
            services.AddTransient<IPDLancamentosAS, PDLancamentosAS>();
            services.AddTransient<IPDRetrabalhoAS, PDRetrabalhoAS>();
            services.AddTransient<IPDOrdemProdCompAS, PDOrdemProdCompAS>();
            services.AddTransient<IPDOrdemProdAS, PDOrdemProdAS>();
            services.AddTransient<IPDOrdemProdRotinasAS, PDOrdemProdRotinasAS>();
            services.AddTransient<IPDRefugoMotAS, PDRefugoMotAS>();
            services.AddTransient<IPDRefugoReapAS, PDRefugoReapAS>();
            services.AddTransient<IPDRefugosAS, PDRefugosAS>();
            services.AddTransient<IPDMotivosAS, PDMotivosAS>();
            services.AddTransient<IPDParamEstabAS, PDParamEstabAS>();

            services.AddTransient<IPCKG_ProducaoAS, PCKG_ProducaoAS>();
            services.AddTransient<IPCKG_Producao, PCKG_Producao>();
            services.AddTransient<IProducaoPackage, ProducaoPackage>();

            services.AddTransient<IPDMotivosService, PDMotivosService > ();
            services.AddTransient<IPDMotivosRepository, PDMotivosRepository>();

            services.AddTransient<IPCKG_EstoqueAS, PCKG_EstoqueAS>();
            services.AddTransient<IPCKG_Estoque, PCKG_Estoque>();
            services.AddTransient<IEstoquePackage, EstoquePackage>();

        }
    }
}
