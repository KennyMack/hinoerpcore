﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Infra.Cross.IoC
{
    public static class DInjection
    {
        public static ServiceProvider _Provider { get; private set; }

        public static void Initialize(IServiceCollection serviceCollection)
        {
            serviceCollection.RegisterServices();
            _Provider = serviceCollection.BuildServiceProvider();
        }

        public static T GetIntance<T>() =>
            _Provider.GetService<T>();
    }
}
