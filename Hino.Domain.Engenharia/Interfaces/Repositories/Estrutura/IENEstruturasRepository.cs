﻿using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Domain.Base.Interfaces.Repositories;

namespace Hino.Domain.Engenharia.Interfaces.Repositories.Estrutura
{
    public interface IENEstruturasRepository : IBaseRepository<ENEstruturas>
    {
    }
}
