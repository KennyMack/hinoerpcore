﻿using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Domain.Base.Interfaces.Services;

namespace Hino.Domain.Engenharia.Interfaces.Services.Estrutura
{
    public interface IENEstruturasService : IBaseService<ENEstruturas>
    {
    }
}
