﻿using Hino.Domain.Engenharia.Interfaces.Repositories.Estrutura;
using Hino.Domain.Engenharia.Interfaces.Services.Estrutura;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Engenharia.Services.Estrutura
{
    public class ENEstruturasService : BaseService<ENEstruturas>, IENEstruturasService
    {
        private readonly IENEstruturasRepository _IENEstruturasRepository;

        public ENEstruturasService(IENEstruturasRepository pIENEstruturasRepository) :
             base(pIENEstruturasRepository)
        {
            _IENEstruturasRepository = pIENEstruturasRepository;
        }
    }
}
