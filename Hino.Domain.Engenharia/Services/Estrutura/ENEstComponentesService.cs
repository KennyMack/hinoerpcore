using Hino.Domain.Engenharia.Interfaces.Repositories.Estrutura;
using Hino.Domain.Engenharia.Interfaces.Services.Estrutura;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Engenharia.Services.Estrutura
{
    public class ENEstComponentesService : BaseService<ENEstComponentes>, IENEstComponentesService
    {
        private readonly IENEstComponentesRepository _IENEstComponentesRepository;

        public ENEstComponentesService(IENEstComponentesRepository pIENEstComponentesRepository) : 
             base(pIENEstComponentesRepository)
        {
            _IENEstComponentesRepository = pIENEstComponentesRepository;
        }
    }
}
