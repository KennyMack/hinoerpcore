using Hino.Domain.Engenharia.Interfaces.Repositories.Estrutura;
using Hino.Domain.Engenharia.Interfaces.Services.Estrutura;
using Hino.Infra.Cross.Entities.Engenharia.Estrutura;
using Hino.Domain.Base.Services;

namespace Hino.Domain.Engenharia.Services.Estrutura
{
    public class ENEstRotinasService : BaseService<ENEstRotinas>, IENEstRotinasService
    {
        private readonly IENEstRotinasRepository _IENEstRotinasRepository;

        public ENEstRotinasService(IENEstRotinasRepository pIENEstRotinasRepository) : 
             base(pIENEstRotinasRepository)
        {
            _IENEstRotinasRepository = pIENEstRotinasRepository;
        }
    }
}
